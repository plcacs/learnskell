from __future__ import print_function
from patsy import dmatrices
import numpy as np
import pandas as pd
from sklearn.linear_model import LogisticRegression
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import AdaBoostClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.svm import SVC
from sklearn import metrics
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import OneHotEncoder, LabelEncoder
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import cross_val_predict
from imblearn.over_sampling import SMOTE
from imblearn.over_sampling import RandomOverSampler
from imblearn.ensemble import EasyEnsemble 
from imblearn.under_sampling import RandomUnderSampler 
from sklearn.feature_selection import RFE
from imblearn.under_sampling import NeighbourhoodCleaningRule 
from imblearn.under_sampling import NearMiss 
from sklearn.model_selection import KFold
from sklearn.model_selection import StratifiedKFold
from scipy.sparse import *
import operator
from sklearn.utils import check_array, check_random_state
from collections import Counter
from math import log
import random
from sklearn.feature_selection import SelectFromModel
from sklearn.linear_model import RandomizedLasso
from sklearn.linear_model import LassoCV
from sklearn.linear_model import SGDClassifier

class_order = {0 : -2, 1 : -1, 2 : 0, 3 : 1, 4 : 2, 5 : 101}
feature_num = [12, 3, 6, 3, 5, 4, 9, 4, 3, 6, 4, 2, 2, 4, 3, 4, 3, 9, 2, 3, 2, 2]
majority_index = 2
skip_feature = [19]

def sperate_class(XX, yy, X, y):
	for i in range(len(class_order)):
		yy.append(np.array([]))
		XX.append(np.array([[]]))

		for j, v in enumerate(y):
			if v == class_order[i]:
				yy[i] = np.append(yy[i], v)
				if XX[i].size == 0:
					XX[i] = np.reshape(X[j],(1,-1))
				else:
					XX[i] = np.concatenate((XX[i], [X[j]]), axis=0)

def resample_data(X, y, r, seed):
	smote_clf = SVC(kernel='poly', degree=2)
	rs = SMOTE(ratio=float(r), random_state=seed, kind='svm', svm_estimator=smote_clf)
	X_resampled, y_resampled = rs.fit_sample(X, y)
	for v in np.nditer(X_resampled, op_flags=['readwrite']):
		v[...] = round(v)

	res = []
	res.append(X_resampled)
	res.append(y_resampled)
	return res

def create_tmp_data(XX, yy, i, j):
	XX_tmp = np.concatenate((XX[i], XX[j]), axis=0)
	yy_tmp = np.append(yy[i], yy[j])
	res = []
	res.append(XX_tmp)
	res.append(yy_tmp)
	return res

def extract_data(XX, yy):
	res_X = np.array([[]])
	res_y = np.array([])
	i=0
	for v in yy:
		if v != 0:
			res_y = np.append(res_y, v)
			if res_X.size == 0:
				res_X = np.reshape(XX[i], (1,-1))
			else:
				res_X = np.concatenate((res_X, [XX[i]]), axis=0)
		i += 1
	res=[]
	res.append(res_X)
	res.append(res_y)
	return res

def print_rank(rank, i, j, index, flag=0):
	print("\n###f%d:" % index)
	for v in rank[i:j]:
		print(v),

	if flag == 1:
		tmp=np.array(rank[i:j])
		print("\nres: %0.5f (%0.5f)" % (tmp.mean(), tmp.std()))

def print_feature_test_wrapper(lst, flag=0):
	s, j=0, 1
	for i in feature_num:
		if j not in skip_feature:
			print_rank(lst, s, s+i, j, flag)
			s += i

		j += 1

def feature_rank_test(model, X, y):
	selector = RFE(model, 5)
	selector = selector.fit(X, y)
	print_feature_test_wrapper(selector.support_)
	print_feature_test_wrapper(selector.ranking_)

def feature_importance_test(model, X, y):
	X = one_hot_encode(X, skip_feature)
	model.fit(X, y)
	print_feature_test_wrapper(model.feature_importances_, 1)


def DVM_vector(v1, v2, X_max, X_min, k=1):
	if len(v1) != len(v2):
		print('vector size should be the same')
		return

	res = 0.0

	if cmp(list(v1), list(v2)) == 0:
		return res
	
	for i in range(len(v1)):
		if v1[i] == v2[i]:
			continue

		c_m = Counter(list(np.reshape(X_max[:,i:i+1], (1,-1)).flatten()))
		c_n = Counter(list(np.reshape(X_min[:,i:i+1], (1,-1)).flatten()))
		t1 = c_m[v1[i]]
		t2 = c_n[v1[i]]
		t3 = c_m[v2[i]]
		t4 = c_n[v2[i]]

		res += pow(abs(t1/(t1+t2) - t3/(t3+t4)), 1)
		res += pow(abs(t2/(t1+t2) - t4/(t3+t4)), 1)

	return res

def OM(v1,v2):
	return len(list(set(v1) & set(v2)))

def info_diff(v1, v2, Cl):
	intersection_index = np.where(v1-v2 == 0)[0]
	cv = v1[intersection_index]

	intersection_matrix = Cl[:, intersection_index]
	match_num = np.count_nonzero((intersection_matrix == cv).all(axis=1))

# approximate value for conditional entropy
	res = log(match_num, 2) / log(Cl.shape[0], 2)

	return res

def getNeighbors(X_max, X_min, i, Stype, k):
    distances = []
    Instance = X_min[i]
    for j in range(len(X_min)):
    	if i == j:
    		continue
    	
    	if Stype == 'DVM':
    		dist = DVM_vector(Instance, X_min[j], X_max, X_min)
    	elif Stype == 'info':
    		dist = info_diff(Instance, X_min[j], X_max)
    	else:
    		dist = OM(Instance, X_min[j])

    	distances.append((j, dist))
    
    distances.sort(key = operator.itemgetter(1))
    neighbors = []
    for j in range(k):
        neighbors.append(distances[j][0])

    return neighbors

def find_kneighbors(X_max, X_min, Stype, k=5):
	res = np.array([[]])
	for i in range(len(X_min)):
		neighbors = getNeighbors(X_max, X_min, i, Stype, k)
		if res.size == 0:
			res = np.reshape(neighbors,(1,-1))
		else:
			res = np.concatenate((res, [neighbors]), axis=0)

	return res

def make_cv_samples(X, y_type, nn_num, n_samples, seed):
	X = check_array(X)
	random_state = check_random_state(seed)
	X_new = np.zeros((n_samples, X.shape[1]))

	samples = random_state.randint(
            low=0, high=len(nn_num.flatten()), size=n_samples)

	for i, n in enumerate(samples):
		row, col = divmod(n, nn_num.shape[1])
		orig = X[row]
		neighbor = X[nn_num[row, col]]

		intersection_index = np.where(orig-neighbor == 0)[0]
		cv = orig[intersection_index]
		X_new[i][intersection_index] = cv

		for j in np.where(orig-neighbor != 0)[0]:
			attribute_set = X[:, j:j+1].flatten()
			attribute_set = filter(lambda a: a != orig[j], attribute_set)
			X_new[i][j] = Counter(attribute_set).most_common()[0][0]

#			attribute_set = list(set(X[:, j:j+1].flatten()))
#			attribute_set.remove(orig[j])
#			X_new[i][j] = random.choice(attribute_set)

	X_new = np.concatenate((X, X_new), axis=0)
	y_new = np.array([y_type] * len(X_new))

	return X_new, y_new

def make_synth_samples(X, y_type, nn_num, n_samples, seed):
	X = check_array(X)
	random_state = check_random_state(seed)
	X_new = np.zeros((n_samples, X.shape[1]))

	samples = random_state.randint(
            low=0, high=len(X), size=n_samples)

	row_len = X.shape[1]
	for i, row in enumerate(samples):
		orig = X[row]
		neighbor_index = nn_num[row]
		for j in range(row_len):
			candidate = []
			candidate.append(orig[j])
			for k in neighbor_index:
				candidate.append(X[k][j])
#			candidate_set = list(set(candidate))
#			r_index = random_state.randint(0, len(candidate_set)/3+1)
			r_index = 0
			X_new[i][j] = Counter(candidate).most_common()[r_index][0]

	X_new = np.concatenate((X, X_new), axis=0)
	y_new = np.array([y_type] * len(X_new))

	return X_new, y_new

def SMOTE_N(X_m, X_n, y_m, y_n, Stype, ratio=1.0, seed=1, k=5):
	nns = find_kneighbors(X_m, X_n, Stype, k)
	
	if Stype == 'DVM':
		n_samples = int(ratio * len(y_m))	
	else:
		n_samples = int(np.count_nonzero(y_m == 0) * ratio) + 1

	if n_samples <= len(y_n):
		print('the ratio is too small!')
		return

	n_samples = n_samples - len(y_n)
	X_new, y_new = make_cv_samples(X_n, y_n[0], nns, n_samples, seed)

	return X_new, y_new

def tune_ratio(i):
	if i in [3]:
		ratio = 0.6
	elif i in [0, 1]:
		ratio = 0.5
	else:
		ratio = 0.45

	return ratio

def SMOTE_wrapper(orig_X, orig_y, Stype, seed=1, ratio=1.0, k=5):
	yy=[]
	XX=[]
	sperate_class(XX, yy, orig_X, orig_y)

	X = XX[majority_index]
	y = np.zeros(len(XX[majority_index]))

	X_resampled = []
	y_resampled = []
	
	for i in range(len(class_order)):
		if i == majority_index:
			continue

#		if float(XX[i].shape[0]) / float(XX[majority_index].shape[0]) >= ratio:
		if i in [5]:
			y = np.append(y, yy[i])
			X = np.concatenate((X, XX[i]), axis=0)
			continue

#Tune the ratio to optimize the performance...
		ratio = tune_ratio(i)

		if Stype == 'DVM':
			X_max = XX[majority_index]
			y_max = yy[majority_index]
		else:
			X_max = orig_X
			y_max = orig_y

		X_resampled, y_resampled = SMOTE_N(X_max, XX[i], 
										y_max, yy[i], Stype, 
										ratio, seed, k)

		y = np.append(y, y_resampled)
		X = np.concatenate((X, X_resampled), axis=0)

#	rs = NeighbourhoodCleaningRule(random_state=1)
#	X, y = rs.fit_sample(X, y)

	return X, y

def labelized_X(X, nc_set=[]):
	lc = LabelEncoder()
	y = X.T
	res = np.array([[]])
	for i, v in enumerate(y):

		if i+1 in nc_set:
			res = np.concatenate((res, [v]), axis=0)
			continue

		tmp = lc.fit_transform(v)
		if res.size == 0:
			res = np.reshape(tmp, (1,-1))
		else:
			res = np.concatenate((res, [tmp]), axis=0)

	return res.T

def binalized_X(X, skip=[], nc_set=[]):
	res = np.array([[]])
	for v in X:
		row = np.array([])
		for i in range(len(v)):
			if i+1 in skip:
				continue

			if i+1 in nc_set:
				row = np.append(row, v[i])
				continue

			tmp = np.zeros(feature_num[i])
			
			if v[i] >= feature_num[i]:
				print("i:%d, vi:%d" % (i, v[i]))

			tmp[int(v[i])] = 1.0
			row = np.append(row, tmp)

		if res.size == 0:
			res = np.reshape(row, (1,-1))
		else:
			res = np.concatenate((res, [row]), axis=0)

	return res

def one_hot_encode(X, skip, noncatetorical_set=[]):
	lx = labelized_X(X, noncatetorical_set)
	res = binalized_X(lx, skip, noncatetorical_set)
	return res

def output_csv(X, y, fname='sampled_data'):
	column = np.array(['f1', 'f2', 'f3', 'f4', 'f5', 'f6', 'f7', \
		'f8', 'f9', 'f10', 'f11', 'f12', 'f13', 'f14', 'f15', 'f16', \
		'f17', 'f18', 'f19', 'f20', 'f21', 'f22'])

	rs = RandomOverSampler(random_state=1)
#	X_resampled, y_resampled = rs.fit_sample(X, y)
	X_resampled, y_resampled = SMOTE_wrapper(X, y, 'info', k=6)
	df = pd.DataFrame(data=X_resampled, columns=column)
	df.insert(0, 'target', y_resampled)

	df.to_csv(fname + '.csv')

def cross_val_test(model, X, y, cv):
#	rs = RandomOverSampler(random_state=1)
#	rs = RandomUnderSampler(random_state=1)
	
#	X_resampled, y_resampled = rs.fit_sample(X, y)
	X_resampled, y_resampled = X, y
#	X_resampled, y_resampled = SMOTE_wrapper(X, y, 'info', k=6)
	X_resampled = one_hot_encode(X_resampled, skip_feature)

	scores = cross_val_score(model, X_resampled, y_resampled, cv=cv)
	print("Accuracy: %0.2f (+/- %0.2f)" % (scores.mean(), scores.std() * 2))

#	predicted = cross_val_predict(model, X_resampled, y_resampled, cv=cv)
#	p, r, f1, s = metrics.precision_recall_fscore_support(y_resampled, predicted)

#	print("class:\tprecision:\trecall:\t\tf1:\t\tsupport:")
#	for i in range(len(class_order)):
#		print("%d\t%lf\t%lf\t%lf\t%d" % (class_order[i], p[i], r[i], f1[i], s[i]))

#	print("Average Accuracy: %0.2f" % metrics.accuracy_score(y_resampled, predicted))
#	print("Average Predict: %0.2f" % metrics.precision_score(y_resampled, predicted, average='macro').mean())
#	print("Average Recall: %0.2f" % metrics.recall_score(y_resampled, predicted, average='macro').mean())
#	print("Average f1: %0.2f" % metrics.f1_score(y_resampled, predicted, average='macro').mean())

def my_val_test(model, X, y):
	test_predict = np.array([])
#	rs = RandomOverSampler(random_state=1)
#	rs = RandomUnderSampler(random_state=1)
#	rs = NeighbourhoodCleaningRule(random_state=1)
	kf = StratifiedKFold(n_splits=10, random_state=1)
	res = np.zeros((len(class_order), 4, 10))
#	kf = KFold(n_splits=10, random_state=1)

	for n, index in enumerate(kf.split(X, y)):
		tr_X, test_X = X[index[0]], X[index[1]]
		tr_y, test_y = y[index[0]], y[index[1]]

#		X_resampled, y_resampled = SMOTE_wrapper(tr_X, tr_y, 'info', ratio = 0.5, k=6)
#		X_resampled, y_resampled = SMOTE_wrapper(tr_X, tr_y, 'info', k=6)
		X_resampled, y_resampled = tr_X, tr_y
#		X_resampled, y_resampled = rs.fit_sample(tr_X, tr_y)

		X_resampled = one_hot_encode(X_resampled, skip_feature)
		test_X = one_hot_encode(test_X, skip_feature)

		clf = model.fit(X_resampled, y_resampled)
		predict_res = clf.predict(test_X)

		p, r, f1, s = metrics.precision_recall_fscore_support(test_y, predict_res)
		for c in range(len(class_order)):
			res[c].T[n] = [p[c], r[c], f1[c], s[c]]
#		print(metrics.accuracy_score(test_y, predict_res))
#		probs_res = clf.predict_proba(test_X)
#		print(metrics.classification_report(test_y, predict_res))
#		test_predict = np.append(test_predict, metrics.accuracy_score(test_y, predict_res))

	print("class:\tprecision:\trecall:\t\tf1:\t\tsupport:")
	for n in range(len(class_order)):
		print("%d\t%lf\t%lf\t%lf\t%0.1f" % 
			(class_order[n], res[n][0].mean(), 
				res[n][1].mean(), res[n][2].mean(), res[n][3].mean()))

#	print("Accuracy: %0.2f (+/- %0.2f)" % (test_predict.mean(), test_predict.std() * 2))

def print_wrong_predict(predict_res, probs_res, test_y):
	n=0
	for i,j in zip(predict_res, np.ravel(test_y)):
		if i != j:
			tmp = np.argsort(probs_res[n])[::-1][[0,1]]

			if int(j) not in [class_order[x] for x in tmp]:
				print("%d" % j)
				print("predicted class: %r, %r" % (class_order[tmp[0]], class_order[tmp[1]]), end=' ; ')
				print("probas: %r, %r" % (probs_res[n][tmp[0]], probs_res[n][tmp[1]]))

		n += 1

def print_predict_res(probs_res):
	for v in probs_res:
		tmp = np.argsort(v)[::-1][[0,1]]
		for i in tmp:
			print("%d,%lf;" % (class_order[i], v[i]), end='')
		print('')

def generate_model_predict(model, X, y):
	tr_X, test_X, tr_y, test_y = train_test_split(X, y, test_size=0.4, stratify=y)

	X_resampled, y_resampled = SMOTE_wrapper(tr_X, tr_y, 'info', k=6)
#	X_resampled, y_resampled = tr_X, tr_y

	X_resampled = one_hot_encode(X_resampled, skip_feature)
	test_X = one_hot_encode(test_X, skip_feature)

	clf = model.fit(X_resampled, y_resampled)
	predict_res = clf.predict(test_X)
	probs_res = clf.predict_proba(test_X)

	print(metrics.accuracy_score(test_y, predict_res))
	print(metrics.classification_report(test_y, predict_res))

#	print_wrong_predict(predict_res, probs_res, test_y)

def generate_predict_2(model):
	data = pd.read_csv("./train.csv")
	tr_y, tr_X = dmatrices('target~f1+f2+f3+f4+f5+f6+f7+f8+f9+f10+f11+f12+f13+f14+f15+f16+f17+f18+f19+f20+f21+f22 - 1',
				data, return_type='dataframe')
	tr_y = np.ravel(tr_y)
	tr_X = tr_X.values
#	tr_X, tr_y = X, y

	data = pd.read_csv("./test.csv")
	test_y, test_X = dmatrices('target~f1+f2+f3+f4+f5+f6+f7+f8+f9+f10+f11+f12+f13+f14+f15+f16+f17+f18+f19+f20+f21+f22 - 1',
				data, return_type='dataframe')
	test_y = np.ravel(test_y)
	test_X = test_X.values

	X_resampled, y_resampled = SMOTE_wrapper(tr_X, tr_y, 'info', k=6)
#	X_resampled, y_resampled = tr_X, tr_y

	X_resampled = one_hot_encode(X_resampled, skip_feature)
	test_X = one_hot_encode(test_X, skip_feature)

	clf = model.fit(X_resampled, y_resampled)
	predict_res = clf.predict(test_X)
	probs_res = clf.predict_proba(test_X)

#	print(metrics.accuracy_score(test_y, predict_res))
	print(metrics.classification_report(test_y, predict_res))

	print_wrong_predict(predict_res, probs_res, test_y)
#	print_predict_res(probs_res)

def feature_select_test(X, y):
	enc = OneHotEncoder()
	X = enc.fit_transform(X)

#	clf = LogisticRegression(max_iter=100000, class_weight='balanced', solver='sag')
	clf = SGDClassifier(penalty='l1')
	sfm = SelectFromModel(clf,'0.75*mean')
	sfm.fit(X, y)
	X_transform = sfm.transform(X)
	n_features = X_transform.shape[1]
	print(sfm.get_support())
	print(n_features)


#########################################
#########################################
data = pd.read_csv("./train.csv")
#data = pd.read_csv("./yr02_test.csv")

y, X = dmatrices('target~f1+f2+f3+f4+f5+f6+f7+f8+f9+f10+f11+f12+f13+f14+f15+f16+f17+f18+f19+f20+f21+f22 - 1',
				data, return_type='dataframe')
y = np.ravel(y)
X = X.values

#tr_X, test_X, tr_y, test_y = train_test_split(X, y, test_size=0.15, random_state=1, stratify=y)

#enc = OneHotEncoder(categorical_features=[1,2,9,10,11,12,17,18,19,20])
#enc = OneHotEncoder()
#tr_X_resampled = enc.fit_transform(tr_X_resampled)
#y_resampled = y

model_lr = LogisticRegression(fit_intercept = False,solver='sag',multi_class='multinomial',max_iter=5000)
model_svc = SVC(kernel="poly", degree=2)
model_svc1 = SVC(kernel="linear")
model_dt = DecisionTreeClassifier(criterion='entropy')
model_ada = AdaBoostClassifier(
		model_dt, 
		n_estimators=500,
		algorithm='SAMME.R',
		learning_rate=1,
		random_state=1)

model_rf = RandomForestClassifier(criterion='entropy', 
#		class_weight={1:20, -1:10, -2:10, 2:10, 101:20},
#		class_weight={99:0.1},
#		class_weight='balanced',
		n_estimators=500,
		random_state=1)

model=model_rf

#feature_select_test(X, y)
#cross_val_test(model, X, y, 10)

#clf = model.fit(tr_X_resampled, tr_y_resampled)
#feature_rank_test(model_svc1, X_resampled, y_resampled)
#feature_importance_test(model, X, y)

#my_val_test(model, X, y)

#evaluation
#generate_model_predict(model, X, y)

generate_predict_2(model)



