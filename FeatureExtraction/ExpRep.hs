module ExpRep where

import Control.Applicative    
import Helium.Main.PhaseLexer
import Helium.Main.PhaseParser
import Helium.Main.PhaseImport
import Helium.Main.PhaseResolveOperators
import Helium.Main.PhaseStaticChecks
import Helium.Main.PhaseKindInferencer
import Helium.Main.PhaseTypingStrategies
import Helium.Main.PhaseTypeInferencer
import Helium.Main.PhaseDesugarer
import Helium.Main.PhaseCodeGenerator
import Helium.Main.CompileUtils
import Helium.StaticAnalysis.Messages.Messages
import Helium.StaticAnalysis.Messages.TypeErrors
import Helium.StaticAnalysis.Messages.TErrors
import Helium.StaticAnalysis.Messages.Warnings(Warning)
import Helium.StaticAnalysis.Inferencers.TypeInferencing(typeInferencing)
import Helium.ModuleSystem.DictionaryEnvironment (DictionaryEnvironment)
--import UHA_Syntax
import Helium.StaticAnalysis.Messages.Information (showInformation)
-- import System.FilePath.Posix
import Helium.Utils.Utils
import qualified Control.Exception as CE (catch, IOException)
import Data.IORef
import Helium.StaticAnalysis.Messages.StaticErrors(errorsLogCode)
import Helium.Parser.Parser(parseOnlyImports)
import Control.Monad
import System.FilePath(joinPath)
import Data.List(intersect,lookup,foldl', nub, elemIndex, isSuffixOf, intercalate,(\\),sortBy)
import Data.Maybe
import Data.Map(lookup,member,(!))
import Lvm.Path(explodePath,getLvmPath)
import System.Directory(doesFileExist, getModificationTime)
import Helium.Main.Args
import Paths_helium
import Top.Types
import Top.Types.Schemes
import Helium.StaticAnalysis.Miscellaneous.DoublyLinkedTree
import Helium.StaticAnalysis.Miscellaneous.UHA_Source
import Helium.Syntax.UHA_Syntax
import Helium.Parser.OperatorTable(Assoc(AssocLeft,AssocRight,AssocNone))
import Helium.Utils.OneLiner
import FEState
import TypeDiffNewNew
import Data.List.Split(splitOn)
import FeatureExtraction
import ExpRepHelper
import LocalPref
import Data.List (intersperse)

{- Example
Act: [String] -> Int
Exp: ([v191] -> v191) -> (Table -> Table) -> Table -> Int
Idx: 1

1st step: ([ [v191] -> v191, (Table -> Table), Table], Int) = functionSpineOfLength exp

This function requires relevant types to be exactly unifiable, 
which is maybe too restrictive
-}
repAddPar2 :: ErrorInfo -> (Tp,Tp) -> Int -> Rep
repAddPar2 errInfo (act,exp) idx = 
    either (const None) (decorate errInfo) (repAddPar2' (act,exp) idx)

repAddPar2' :: (Tp,Tp) -> Int -> Either UnificationError Rep
repAddPar2' (act,exp) idx = do
    let (etps,etp) = functionSpineOfLength (idx+2) exp
    retps <- if length etps < 3 then Left (InfiniteType 100001) 
                else Right (reverse etps)
    let newarg = head retps
    let newfunc = head . tail $ retps
    let newfuncfunc = head . drop 2 $ retps
    let restetps = reverse . drop 3 $ retps
    
    unier1 <- mgu newfunc (newarg .->. lgTv1) 
    unier2 <- mgu newfuncfunc ( unier1 |-> lgTv1 .->. lgTv2)
    unier3 <- mgu act (foldr (.->.) etp (restetps ++ [unier2 |-> lgTv2]))
    return (AddPar idx (idx + 2) `Seq` AddPar (idx+1) (idx+2))

{-
Example
Act: Table -> [Int]
Exp: ([v236] -> [v236]) -> Table -> [Int]
Idx: 1

1st step:
    ([ ([v236] -> [v236]), Table, [Int]) = functionSpineOfLength 2 exp
    
2nd step: unify
    ([v236] -> [v236])
with
    Table -> lgTv1
and lgTv1 will be refined to Table

3rd: unify 
    Table -> [Int]
with the Act
    Table -> [Int]    
-}    
repAddParDist1 :: ErrorInfo -> (Tp,Tp) -> Int -> Rep
repAddParDist1 errInfo (act,exp) idx = 
    let (etps,etp) = functionSpineOfLength (idx+1) exp in
    if length etps < 2 then (error "Error in repAddPar1")  else
      let retps = reverse etps
          newarg = head retps  
          newfunc = head . tail $ retps
          restetps = reverse . drop 2 $ retps
          uniRes1 = mgu newfunc (newarg .->. lgTv1)
         -- in error $ show newfunc ++ "\t" ++ show (newarg .->. lgTv1)
         
          offender = (offendingInfoTree . snd) errInfo
            
          emsg1 = "repAddParDist1: sit: no sibling at: " ++ show idx
          sit = maybe (error emsg1) id (siblingAtAsIT idx offender)
            
          emsg2 = "repAddParDist1: eit: no sibling at: " ++ show (idx+1)
          eit = maybe (error emsg2) id (siblingAtAsIT (idx+1) offender)
                
          rep = AddParIT sit eit         
      in case uniRes1 of
          Left _ -> rep `Seq` Hint "But need to make some change first"
          Right unier1 -> 
           case mgu act (foldr (.->.) etp (restetps ++ [unier1 |-> lgTv1])) of
                Right _ -> rep 
                Left _ -> rep `Seq` Hint "And need some further changes"

{-
Example: 
Act: [v344] -> [v344]
Exp: (v285 -> Int -> v285) -> v285 -> Int -> v285
I  : 1

1st, break the Exp with functionSpineOfLength at Int, yields
([(v285 -> Int -> v285), v285, Int], v285). 

2nd, unify 
    v285 -> Int -> v285
with
    v285 -> lgTv1
and lgTv1 willbe Int -> v285   
   
3rd, unify 
    Int -> v285 
with
    Int -> lgTv2
and lgTv2 will be v285

4th, build a function type from v285 (result from unification) 
as argument type and v285 (result from functionSpineOfLength)

5th unify Act type with v285 -> v285
-}                              
repAddParDist2 :: ErrorInfo -> (Tp,Tp) -> Int -> Rep
repAddParDist2 errInfo (act,exp) i =                 
    let (etps,etp) = functionSpineOfLength (i+2) exp in
      if length etps < 3 then (error . ("In repAddParDist2: Noe enough etps: " ++) . show) etps  else
        let retps = reverse etps
            newarg2 = head retps
            newarg1 = head . tail $ retps
            newfunc = head . tail . tail $ retps
            restetps = reverse . drop 3 $ retps
            
            offender = (offendingInfoTree . snd) errInfo
            
            emsg1 = "repAddParDist2: sit: no sibling at: " ++ show i
            sit = maybe (error emsg1) id (siblingAtAsIT i offender)
            
            emsg2 = "repAddParDist2: eit: no sibling at: " ++ show (i+2)
            eit = maybe (error emsg2) id (siblingAtAsIT (i+2) offender)
                
            rep = AddParIT sit eit
                
            uniRes1 = mgu newfunc (newarg1 .->. newarg2 .->. lgTv1)
         -- in error $ show newfunc ++ "\t" ++ show (newarg .->. lgTv1)
        in case uniRes1 of
              Left _ -> rep `Seq` Hint "But need to make some change first"
              Right unier1 -> 
                case mgu act (foldr (.->.) etp (restetps ++ [unier1 |-> lgTv1])) of
                    Right _ -> rep
                    Left _ -> rep `Seq` Hint "And need some further changes"
                              
-- decorate add InfoTree to Rep if it doesn't have one            
decorate :: ErrorInfo -> Rep -> Rep
decorate errInf (AddPar s end) =         
    let offender = offendingInfoTree (snd errInf)
        Just sit = siblingAtAsIT s offender
        Just eit = siblingAtAsIT end offender
    in AddParIT sit eit
decorate errInf (rep1 `Seq` rep2) = 
    decorate errInf rep1 `Seq` decorate errInf rep2
decorate _ rep = rep
    
interpret1 None = ""
interpret1 (Seq r1 r2) = interpret1 r1 ++ "\n"
                                ++ interpret1 r2
interpret1 (Hint str) = str
interpret1 (Pull extraArgNode num) = 
    let all = children extraArgNode
        toBePulled = drop (length all - num) all
    in 
       if length toBePulled < num
       then error $ "interpret1: Pull: can pull out: " ++ show (length toBePulled) ++ " while required: " ++ show num
       else "Pull *" ++ (concat . intersperse " " . map showITJust) toBePulled ++"* out of the parentheses"
       
interpret1 (AddParIT st ed) =
    "Add an open parenthesis before *" ++ showITJust st
    ++ "* and a closing parenthesis after *" ++ showITJust ed ++ "*"

interpret1 (AddBrac it lev) = "Add " ++ show lev ++ " [] at " ++ showITJust it ++ errLoc it
interpret1 (RmBrac it lev) = "Remove " ++ show lev ++ " [] at " ++ showITJust it ++ errLoc it
    
interpret1 rep = error $ "Unhandled interpretation, Rep:" ++ show rep                               
repPullArg errInf idx feat1 numArgs
    | feat1 == Just 3 = 
        case (adds,adde) of
            (Just s, Just t) -> AddParIT s t
            (Just s, Nothing) -> error  "repPullArg invalid index: 1"
            (_ , Just t) ->  error "repPullArg invalid index: -1"
            _ ->  error "repPullArg invalid index: -1 and 1"
        `Seq`
        case pullinfix of 
            Just pinf -> Pull pinf numArgs
            _ -> error "repPullArg invalid pullinfix index: 1"
    | feat1 == Just 1 = 
        case pullprefix of 
            Just pinf -> Pull pinf numArgs
            _ -> error $ "repPullArg invalid pullprefix index: " ++ show idx
    | otherwise = error $ "Unhandled case in repPullArg, feat1: " ++ show feat1
    where offender = offendingInfoTree (snd errInf)
          adds = siblingAtAsIT (-1) offender
          adde = siblingAtAsIT 1 offender
          pullinfix = siblingAtAsIT 1 offender
          pullprefix = siblingAtAsIT idx offender 

repBrac combinedEnv localEnv importEnv errInfo =
    let TypeDiff brackets funcs topDiff _ topBrac = getFeatures5To9 errInfo combinedEnv
    in repBracLev1 brackets topBrac errInfo combinedEnv localEnv importEnv
    
-- repBrac' :: [(Int,Int)] -> ErrorInfo -> Rep
repBracLev1 [] 0 errInfo combinedEnv localEnv importEnv = None
repBracLev1 [] topBrac errInfo combinedEnv localEnv importEnv
    -- if the reported location is the explicitly-typed binding
    {-
    When Changing the retrn type of type annotations, we have to flip the 
    result of bracket differences
    -}
    | msgTyp == Just 12 = 
                     if topBrac > 0
                     then Hint ("Remove " ++ show topBrac ++" [] from the return type of annotation")
                     else Hint ("Add " ++ show (-topBrac) ++" [] to the return type of annotation")
    where msgTyp = getFeature1 errInfo
    
repBracLev1 ((i,diff):_) _ errInfo combinedEnv localEnv importEnv
    -- if the reported location is the RHS
    {-
    When Changing the retrn type of type annotations, we have to flip the 
    result of bracket differences
    -}
    | msgTyp == Just 5 = 
                     if diff > 0
                     then Hint ("Remove " ++ show diff ++" [] from the return type of annotation")
                     else Hint ("Add " ++ show (-diff) ++" [] to the return type of annotation")
    
    -- The case that Helium suggests an expression change and 
    -- the error is at (:) or (++)
    -- Todo: further check if the value of the excerpt is (:) or (++)
    | feature2 == 2 && excerpt == 4 =
            Hint "Use Helium's suggestion"
    -- if the offender is an argument 
    -- if the offender is a constant
    | excerpt == 5 || excerpt == 6 = 
                     if diff > 0  
                     then AddBrac actualSrc diff 
                     else RmBrac actualSrc (-diff)
    | nodeType == 2 && (funcOp == 1 || funcOp == 2) = 
                     if diff >0  
                     then AddBrac actualSrc diff 
                     else RmBrac actualSrc (-diff)
    -- 
    -- if the offender is a leaf and the leaf is a function
    -- or operator
    | nodeType == 3 && ( msgTyp == Just 1 || msgTyp == Just 3) = 
        -- error $ show (i,diff)
        repBracLev2 (i,diff) (fromJust msgTyp) actualSrc combinedEnv localEnv importEnv
    | otherwise = None
    where (excerpt,funcOp) = getFeature11And17 combinedEnv localEnv importEnv errInfo
          nodeType = getFeature10 errInfo
          msgTyp = getFeature1 errInfo
          paraStyle = getFeature16 errInfo
          actualSrc = offendingInfoTree $ snd errInfo
          feature2 = getFeature2 errInfo

-- repBracLev2 :: (Int,Int) -> Int -> InfoTree -> Rep
repBracLev2 (i,diff) msgTyp opIT combinedEnv localEnv importEnv =
    let idx = case (msgTyp,i) of
                (3,1) -> -1
                (3,2) -> 1
                (1,i) -> i
        errmsg = "repBraLev2: no sibling at: " ++ show i
        actIT = maybe (error errmsg) id (siblingAtAsIT idx opIT)
        (excerpt,funcOp) = feature11And17ForIT combinedEnv localEnv importEnv actIT
    -- in error $ subTreeStr 0 (fromJust . parent $ actIT)
    -- in error $ show (msgTyp,i,idx,diff) ++ showIT (Just actIT) ++ show (excerpt,funcOp)
    in if excerpt == 5 || excerpt == 6 
       then if diff >0  
            then AddBrac actIT diff 
            else RmBrac actIT (-diff)
       else if (funcOp == 1 || funcOp == 2)
            then if diff >0  
                 then AddBrac actIT diff 
                 else RmBrac actIT (-diff)
            else None
        
repSig topLevelTypes subs combinedEnv localEnv importEnv errInfo =
    let tpss = getFeatures3And4 errInfo
        (act,exp) = tpScToTp combinedEnv tpss
        TypeDiff brackets funcs topDiff lkFuncs topBra= getFeatures5To9 errInfo combinedEnv
    in 

    case (topDiff, funcs) of
        (1, (i,1):_) -> repAddParDist1 errInfo (act,exp) i
        (2, (i,1):_) -> repAddPar2 errInfo (act,exp) i
        (2, (i,2):_) -> repAddParDist2 errInfo (act,exp) i
        -- (1, _, (i,1):_) -> repAddParDist1 errInfo (act,exp) i
        -- (2, _, (i,1):_) -> repAddPar2 errInfo (act,exp) i
        -- (2, _, (i,2):_) -> repAddParDist2 errInfo (act,exp) i
        
        (-1,(i,-1):_) -> repPullArg errInfo i (getFeature1 errInfo) 1
        (-2,(i,-2):_) -> repPullArg errInfo i (getFeature1 errInfo) 2

        -- In rest cases, we try to make use of likelyFuncs
        _             -> None
                
repPaired topLevelTypes subs combinedEnv localEnv importEnv eI1 eI2 =
    let (act1,exp1) = tpScToTp combinedEnv . getFeatures3And4 $ eI1
        (act2,exp2) = tpScToTp combinedEnv . getFeatures3And4 $ eI1
        features1 = getFeatures5To9 eI1 combinedEnv
        features2 = getFeatures5To9 eI2 combinedEnv
        reportChild  = usesOtherSource . snd $ eI2
    in
    -- error $ show features1 ++ "\t" ++ show features2
    if isComp features1 features2 && closeEnough eI1 eI2 then
        let offender1 = offendingInfoTree . snd $ eI1
            offender2 = offendingInfoTree . snd $ eI2
            feat1 = getFeature1 eI1
        -- in error $ show feat1 ++ "\t" ++ show (sameGen eI1 eI2)
        in if feat1 == Just 3 then
                AddParIT (fromJust $ siblingAtAsIT (-1) offender1) offender2
                `Seq`
                Pull offender2 1
           else if sameGen eI1 eI2 then
                -- error $ show (numRgtSiblings offender1)
                AddParIT (fromJust $ siblingAtAsIT (numRgtSiblings offender1) offender1) (fromJust . parent $ offender2)
                `Seq`
                Pull (fromJust . parent $ offender2) 1
           else
                -- error $ show offender2
                -- (error . show . fromJust . parent)  offender2
                -- error $ show reportChild
                if reportChild then Pull offender2 1
                else Pull (fromJust . parent $ offender2) 1
    else 
        None

repTopTypMinusOne combinedEnv localEnv importEnv errInfo
    -- if the reported location is the RHS
    -- Example: Code-39-17
    | msgTyp == Just 12 = Hint "Remove a parameter"
    -- If the function is point-free and the topLevel diff is -1
    -- Example: Database/group119 25||27||30||52
    | paraStyle == Just 4 && rgtMost parActualSrc = 
        Hint $ "Add a parameter to point-free function, and add" 
               ++ " the parameter after *" ++ showITJust parActualSrc ++"*"
    | otherwise = None
    where (excerpt,funcOp) = getFeature11And17 combinedEnv localEnv importEnv errInfo
          nodeType = getFeature10 errInfo
          msgTyp = getFeature1 errInfo
          paraStyle = getFeature16 errInfo
          parActualSrc = (fromJust . parent . offendingInfoTree . snd) errInfo
          feature2 = getFeature2 errInfo

        
{-repSigLessLikely combinedEnv errInfo =
    let tpss = getFeatures3And4 errInfo
        (act,exp) = tpScToTp combinedEnv tpss
        TypeDiff brackets funcDiff topDiff lkFuncs topBra= getFeatures5To9 errInfo combinedEnv
        (lastIdx, lastDiff) = last lkFuncs
        shortLen = min ((length . spines) exp) ((length . spines) act)
        newTopDiff = if lastIdx == shortLen && topDiff ==0 
                     then lastDiff
                     else topDiff
        newLkFuncs = if lastIdx == shortLen
                     then init lkFuncs
                     else lkFuncs
        newFuncDiff = if null funcDiff then newLkFuncs else funcDiff
    in if null lkFuncs then None
       else case (newTopDiff, newFuncDiff) of
        (1, (i,1):_) -> repAddParDist1 errInfo (act,exp) i
        (2, (i,1):_) -> repAddPar2 errInfo (act,exp) i
        (2, (i,2):_) -> repAddParDist2 errInfo (act,exp) i
        -- (1, _, (i,1):_) -> repAddParDist1 errInfo (act,exp) i
        -- (2, _, (i,1):_) -> repAddPar2 errInfo (act,exp) i
        -- (2, _, (i,2):_) -> repAddParDist2 errInfo (act,exp) i
        
        (-1,(i,-1):_) -> repPullArg errInfo i (getFeature1 errInfo) 1
        (-2,(i,-2):_) -> repPullArg errInfo i (getFeature1 errInfo) 2

        _               -> None

repSigLessCertain topLevelTypes subs combinedEnv localEnv importEnv errInfo
    | (not . isNone) useLikelyFunc = useLikelyFunc
    | (not . isNone) bracSug = bracSug
    | otherwise = 
        case (topDiff, funcs, lkFuncs) of
                (1,[],[])  -> Hint "Remove an argument"        
                (-1,[],[]) -> repTopTypMinusOne combinedEnv localEnv importEnv errInfo        
                _          -> Hint $ "No suggestion: \n" ++ show (TypeDiff brackets funcs topDiff lkFuncs topBra)
    where useLikelyFunc = repSigLessLikely combinedEnv errInfo
          bracSug = repBrac combinedEnv localEnv importEnv errInfo
        -- tpss = getFeatures3And4 errInfo
        -- (act,exp) = tpScToTp combinedEnv tpss
          TypeDiff brackets funcs topDiff lkFuncs topBra = getFeatures5To9 errInfo combinedEnv-}

--repSigLessCertain topLevelTypes subs combinedEnv localEnv importEnv errInfo =
--    let bracSug = repBrac combinedEnv localEnv importEnv errInfo
--        tpss = getFeatures3And4 errInfo
        
repSigLessLikely combinedEnv errInfo =
    let tpss = getFeatures3And4 errInfo
        (act,exp) = tpScToTp combinedEnv tpss
--        TypeDiff brackets funcs topDiff lkFuncs topBra = getFeatures5To9 errInfo combinedEnv
--    in if (not.isNone) bracSug then bracSug
--       else case (topDiff, funcs, lkFuncs) of
        TypeDiff brackets funcDiff topDiff lkFuncs topBra= getFeatures5To9 errInfo combinedEnv
        (lastIdx, lastDiff) = last lkFuncs
        shortLen = min ((length . spines) exp) ((length . spines) act)
        newTopDiff = if lastIdx == shortLen && topDiff ==0 
                     then lastDiff
                     else topDiff
        newLkFuncs = if lastIdx == shortLen
                     then init lkFuncs
                     else lkFuncs
        newFuncDiff = if null funcDiff then newLkFuncs else funcDiff
    in if null lkFuncs then None
       else case (newTopDiff, newFuncDiff) of
        (1, (i,1):_) -> repAddParDist1 errInfo (act,exp) i
        (2, (i,1):_) -> repAddPar2 errInfo (act,exp) i
        (2, (i,2):_) -> repAddParDist2 errInfo (act,exp) i
        -- (1, _, (i,1):_) -> repAddParDist1 errInfo (act,exp) i
        -- (2, _, (i,1):_) -> repAddPar2 errInfo (act,exp) i
        -- (2, _, (i,2):_) -> repAddParDist2 errInfo (act,exp) i
        
        (-1,(i,-1):_) -> repPullArg errInfo i (getFeature1 errInfo) 1
        (-2,(i,-2):_) -> repPullArg errInfo i (getFeature1 errInfo) 2

        _               -> None

repSigLessCertain topLevelTypes subs combinedEnv localEnv importEnv errInfo
    | (not . isNone) useLikelyFunc = useLikelyFunc
    | (not . isNone) bracSug = bracSug
    | otherwise = 
        case (topDiff, funcs, lkFuncs) of
                 (1,[],[])  -> Hint "Remove an argument"        
                 (-1,[],[]) -> repTopTypMinusOne combinedEnv localEnv importEnv errInfo        
                 _          -> Hint $ "No suggestion: \n" ++ show (TypeDiff brackets funcs topDiff lkFuncs topBra)
    where useLikelyFunc = repSigLessLikely combinedEnv errInfo
          bracSug = repBrac combinedEnv localEnv importEnv errInfo
        -- tpss = getFeatures3And4 errInfo
        -- (act,exp) = tpScToTp combinedEnv tpss
          TypeDiff brackets funcs topDiff lkFuncs topBra = getFeatures5To9 errInfo combinedEnv
       
repExp topLevelTypes subs combinedEnv localEnv importEnv _ [] = return []
repExp topLevelTypes subs combinedEnv localEnv importEnv idx (eri1:eris) = do
    let repS = repSig topLevelTypes subs combinedEnv localEnv importEnv eri1
    if (not.isNone) repS then do

        -- if debug then putStrLn $ "Suggestions----------\n" ++ interpret1 repS ++ "\n" else putStr ""
        

        rests <- repExp topLevelTypes subs combinedEnv localEnv importEnv (idx+1) eris
        return ((idx,repS):rests)
    else if (not . null) eris then do 
        let repP = repPaired topLevelTypes subs combinedEnv localEnv importEnv eri1 (head eris)
        if (not.isNone) repP then do

            -- if debug then putStrLn $ "Suggestions----------\n" ++ interpret1 repP ++ "\n" else putStr ""
            
            rests2 <- repExp topLevelTypes subs combinedEnv localEnv importEnv (idx+2) (tail eris)
            return ((idx,repP): (idx+1,repP):rests2)
        else do
            let repI = repSigLessCertain topLevelTypes subs combinedEnv localEnv importEnv eri1
            
            -- if debug then putStrLn $ interpret1 repI else putStr ""
            
            rest <- repExp topLevelTypes subs combinedEnv localEnv importEnv (idx+1) eris
            return ((idx,repI):rest)
    else do
        let repI = repSigLessCertain topLevelTypes subs combinedEnv localEnv importEnv eri1
        
        -- if debug then putStrLn $ interpret1 repI else putStr ""
        
        return [(idx,repI)]

isNone None = True
isNone _ = False
    
