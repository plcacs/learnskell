{-| Module      :  Compile
    License     :  GPL

    Maintainer  :  helium@cs.uu.nl
    Stability   :  experimental
    Portability :  portable
-}

--Right now everything is in this file, so there's far too many imports
module Main where

import Control.Applicative    
import Helium.Main.PhaseLexer
import Helium.Main.PhaseParser
import Helium.Main.PhaseImport
import Helium.Main.PhaseResolveOperators
import Helium.Main.PhaseStaticChecks
import Helium.Main.PhaseKindInferencer
import Helium.Main.PhaseTypingStrategies
import Helium.Main.PhaseTypeInferencer
import Helium.Main.PhaseDesugarer
import Helium.Main.PhaseCodeGenerator
import Helium.Main.CompileUtils
import Helium.StaticAnalysis.Messages.Messages
import Helium.StaticAnalysis.Messages.TypeErrors
import Helium.StaticAnalysis.Messages.TErrors
import Helium.StaticAnalysis.Messages.Warnings(Warning)
import Helium.StaticAnalysis.Inferencers.TypeInferencing(typeInferencing)
import Helium.ModuleSystem.DictionaryEnvironment (DictionaryEnvironment)
--import UHA_Syntax
import Helium.StaticAnalysis.Messages.Information (showInformation)
-- import System.FilePath.Posix
import Helium.Utils.Utils
import qualified Control.Exception as CE (catch, IOException)
import Data.IORef
import Helium.StaticAnalysis.Messages.StaticErrors(errorsLogCode)
import Helium.Parser.Parser(parseOnlyImports)
import Control.Monad
import System.FilePath(joinPath)
import Data.List(intersect,lookup,foldl', nub, elemIndex, isSuffixOf, intercalate,(\\),sortBy)
import Data.Maybe
import Data.Map(lookup,member,(!))
import Lvm.Path(explodePath,getLvmPath)
import System.Directory(doesFileExist, getModificationTime)
import Helium.Main.Args
import Paths_helium
import Top.Types
import Top.Types.Schemes
import Helium.StaticAnalysis.Miscellaneous.DoublyLinkedTree
import Helium.StaticAnalysis.Miscellaneous.UHA_Source
import Helium.Syntax.UHA_Syntax
import Helium.Parser.OperatorTable(Assoc(AssocLeft,AssocRight,AssocNone))
import FEState
-- import TypeDiff
-- import Data.List.Split(splitOn)
import ExpRepHelper
import FeatureExtraction
import ExpRepNew
import ExpRepBrac
import LocalPref

import Data.Time

-- import Data.Either

-- Don't defined ErrorInfo, it's defined in FeatureExtractions.hs already
--An unused ADT that will eventually be used to hold the type error info
-- data ErrorInfo = ErrorInfo {errorStringHash :: Int, expType :: TpScheme, actType :: TpScheme}

-- Prelude will be treated specially
prelude :: String
prelude = "Prelude.hs"

-- Order matters
coreLibs :: [String]
coreLibs = ["LvmLang", "LvmIO", "LvmException", "HeliumLang", "PreludePrim"]

--The compile function has been modified to stop after type checking and just
--return the errors from type checking or unit, if no errors occurred
compile :: String -> String -> [Option] -> [String] -> [String] -> IO ((Either ([(TypeError,ConstraintInfo)],TypeEnvironment,FixpointSubstitution) (DictionaryEnvironment, ImportEnvironment, TypeEnvironment, [Warning])),ImportEnvironment,TypeEnvironment,ImportEnvironment)
compile basedir fullName options lvmPath doneModules =
    do
        let compileOptions = (options, fullName, doneModules)
        putStrLn ("Compiling " ++ fullName)

        -- Store the current module file-name and its context in
        -- two IO refs (unsafe! only used for internal error bug-report)
        writeIORef refToCurrentFileName fullName
        writeIORef refToCurrentImported doneModules

        contents <- safeReadFile fullName

        -- Phase 1: Lexing
        (lexerWarnings, tokens) <- 
            doPhaseWithExit 20 (const "L") compileOptions $
               phaseLexer fullName contents options
        
        unless (NoWarnings `elem` options) $
            showMessages lexerWarnings

        -- Phase 2: Parsing
        parsedModule <- 
            doPhaseWithExit 20 (const "P") compileOptions $
               phaseParser fullName tokens options

        -- Phase 3: Importing
        (indirectionDecls, importEnvs) <-
            phaseImport fullName parsedModule lvmPath options
        
        -- Phase 4: Resolving operators
        resolvedModule <- 
            doPhaseWithExit 20 (const "R") compileOptions $
               phaseResolveOperators parsedModule importEnvs options
            
        stopCompilingIf (StopAfterParser `elem` options)

        -- Phase 5: Static checking
        (localEnv, typeSignatures, staticWarnings) <-
            doPhaseWithExit 20 (("S"++) . errorsLogCode) compileOptions $
               phaseStaticChecks fullName resolvedModule importEnvs options        

        unless (NoWarnings `elem` options) $
            showMessages staticWarnings

        stopCompilingIf (StopAfterStaticAnalysis `elem` options)

        -- Phase 6: Kind inferencing (by default turned off)
        let combinedEnv = foldr combineImportEnvironments localEnv importEnvs
        let importEnv = foldr combineImportEnvironments (head importEnvs) (tail importEnvs)
        when (KindInferencing `elem` options) $
           doPhaseWithExit maximumNumberOfKindErrors (const "K") compileOptions $
              phaseKindInferencer combinedEnv resolvedModule options
              
        -- Phase 7: Type Inference Directives
        (beforeTypeInferEnv, typingStrategiesDecls) <-
            phaseTypingStrategies fullName combinedEnv typeSignatures options

        --putStrLn "About to infer types"
        -- Phase 8: Type inferencing
        (terr) <-  phaseTypeInferencer basedir fullName resolvedModule {-doneModules-} localEnv beforeTypeInferEnv options
        let terrEnv = case terr of
                        (Left (_,tenv,_)) -> tenv
                        _ -> typeEnvironment localEnv
        -- (putStrLn . show ) (getOrderedTypeSynonyms localEnv)        
        return (terr, combinedEnv,terrEnv,importEnv)

        
fromRight :: Either a b -> b
fromRight (Right v) = v
        
fromLeft :: Either a b -> a
fromLeft (Left v) = v
        
isLeft, isRight :: Either a b -> Bool
isLeft (Left v) = True
isLeft _ = False
isRight (Right _) = True
isRight _ = False
        
{-begin utility functions for compile
===============================================================-}
safeReadFile :: String -> IO String
safeReadFile fullName = 
    CE.catch 
        (readFile fullName)
        (\ioErr -> 
            let message = "Unable to read file " ++ show fullName 
                       ++ " (" ++ show (ioErr :: CE.IOException) ++ ")"
            in throw message)

stopCompilingIf :: Bool -> IO ()
stopCompilingIf bool = when bool(exitWith (ExitFailure 1))

maximumNumberOfTypeErrors :: Int
maximumNumberOfTypeErrors = 3 --999999999

maximumNumberOfKindErrors :: Int
maximumNumberOfKindErrors = 3 --999999999

printResult = printResult' 1

printResult' idx fPrinter  [] _ sep filename = return () 
printResult' idx fPrinter  (teinffeat:rest) reps sep filename= do
    let repTxt = maybe ""
                       (\x -> "Suggestion:\n" ++ 
                       filename ++ compSep ++ extractHeader x ++ "\n" ++ interpret1 x) 
                       (Data.List.lookup idx reps)
                       
    fPrinter teinffeat 
    putStr repTxt
    putStr sep

    printResult' (idx + 1) fPrinter rest reps sep filename

concisePrint ((_,inf),feat) =
    (putStrLn . show . self . attribute . offendingInfoTree) inf 
    >> (putStrLn .conciseText) feat

getHeader filename rep = 
    if isNone rep then ""
    else filename ++ compSep ++ extractHeader rep ++ "\n"
    
-- prettyPrintSuggestions [] [] = return ()
-- prettyPrintSuggestions (feats:lst) ((idx,repI):xs) =
    -- do
      -- prettyPrintFeats feats
      -- putStrLn "Suggestion:"
      -- putStrLn $ (interpret1 repI ++ "\n")
      -- prettyPrintSuggestions lst xs
-- prettyPrintSuggestions (feats:lst) [] = 
    -- do
      -- prettyPrintFeats feats
      -- prettyPrintSuggestions lst []

-- printSuggestions [] [] = return ()
-- printSuggestions (feats:lst) ((idx,repI):xs) =
    -- do
      -- putStrLn $ (show feats)
      -- putStrLn "Suggestion:"
      -- putStrLn $ (interpret1 repI ++ "\n")
      -- printSuggestions lst xs
-- printSuggestions (feats:lst) [] =
    -- do
      -- putStrLn $ (show feats)
      -- printSuggestions lst []

{-end utility functions for compile
==============================================================-}

numIterations = 20

main = do
  heliumS <- getCurrentTime
  
  argss                     <- getArgs
  mapM (putStrLn) argss
  let filename = head argss
--  let pp = if null argss
  let pp = "-p" `elem` argss
  let featureOnly = "-ml" `elem` argss
  let inter = "-inter" `elem` argss

  -- let args = if pp
             -- then
                 -- argss \\ ["-p"]
             -- else
                 -- argss
  let args = argss \\["-p","-ml","-inter"]

  (options, Just fullName) <- processHeliumArgs args -- Can't fail, because processHeliumArgs checks it.
    
  lvmPathFromOptionsOrEnv <- case lvmPathFromOptions options of 
                               Nothing -> getLvmPath
                               Just s  -> return (explodePath s)
    
    -- Choose the right libraries to use based on whether overloading is turned off or on
  baseLibs <- getDataFileName $ 
              if overloadingFromOptions options 
              then "lib" 
              else joinPath ["lib","simple"]

  --let baseLibs = joinPath ["lib","simple"]
  let (filePath, moduleName, _) = splitFilePath fullName
      filePath' = if null filePath then "." else filePath
      lvmPath   = filter (not.null) . nub
                  $ (filePath' : lvmPathFromOptionsOrEnv) ++ [baseLibs] -- baseLibs always last
    
    -- File that is compiled must exist, this test doesn't use the search path
  fileExists <- doesFileExist fullName
  newFullName <- 
      if fileExists then 
          return fullName
      else do
        let filePlusHS = fullName ++ ".hs"
        filePlusHSExists <- doesFileExist filePlusHS
        unless filePlusHSExists $ do
             putStrLn $ "Can't find file " ++ show fullName ++ " or " ++ show filePlusHS
             exitWith (ExitFailure 1)
        return filePlusHS

    -- Ensure .core libs are compiled to .lvm
  --mapM_ (makeCoreLib baseLibs) coreLibs    
    -- And now deal with Prelude
  preludeRef <- newIORef []
  _ <- make filePath' (joinPath [baseLibs,prelude]) lvmPath [prelude] options preludeRef

  doneRef <- newIORef []
  newDone <- readIORef doneRef
  --putStrLn "About to compile"
  (results,combinedEnv,localEnv,importEnv) <- (compile filePath' newFullName options lvmPath (map fst newDone))
  --let output = show results
  case results of
    Right _ -> do
          putStrLn "No errors"
    Left (errInfo,topLevelTypes,subs) -> do 
          -- putStrLn "There are some errors"

        (perrInfo) <-  getFeatures errInfo topLevelTypes subs combinedEnv localEnv importEnv pp filename
        let info = map fst perrInfo
        
        -- if debug then putStrLn "**********" >>
                      -- (putStrLn . show ) (shareDollar (info !! 1) (info !! 2))
                 -- else putStr ""
                 
        -- putStrLn "-------------------------"
        
        reparations <- repExp topLevelTypes subs combinedEnv localEnv importEnv 1 info (map snd perrInfo) (length info > 1)

        heliumEnd <- getCurrentTime
                 
        -- reparations <- repExp topLevelTypes subs combinedEnv localEnv importEnv 1 perrInfo

--      if pp   
--      then
--          printResult (prettyPrintFeats . snd) perrInfo reparations ""
--      else if featureOnly 
--          then printResult (putStrLn . show . snd) perrInfo [] ""
--          else printResult concisePrint perrInfo reparations ""
        case (pp, featureOnly, inter) of
            (True,_, False) -> 
                printResult (prettyPrintFeats . snd) perrInfo [] "" filename
                >> mapM (putStrLn . (\(x,y) -> show x ++ " Suggestion: \n" ++ 
                getHeader filename y ++
                interpret1 y)) reparations >> return ()
            (True,_,True) -> printResult (prettyPrintFeats . snd) perrInfo reparations "" filename
            
            (_,True,_) -> mapM_ (putStrLn . \(x,y) -> filename ++ ";" ++ show x ++ ";" ++ show y ) (zip [1..] (map snd perrInfo))
            -- printResult (putStrLn . show . snd) perrInfo [] ""filename
            
            (False,False,True) -> printResult concisePrint perrInfo reparations "\n\n" filename
            (False,False,False) -> 
                printResult concisePrint perrInfo [] "" filename
                >>
                mapM (putStrLn . (\(x,y) -> show x ++ " Suggestion: \n" ++
                getHeader filename y ++
                interpret1 y ++ "\n")) reparations >> return ()



        -- if debug then (putStrLn . show) (queryType combinedEnv "++") else putStr ""
        
        extt <- featureExtractionTime errInfo topLevelTypes subs combinedEnv localEnv importEnv pp filename
        
        rept <-  expRepTime topLevelTypes subs combinedEnv localEnv importEnv info perrInfo 
        
        -- error "Stop"
        
        appendFile  "Timing.txt" (filename ++ "\t" ++ show (extt/20) ++ "\t"++ show (rept/20) ++ "\t" ++ show (diffUTCTime heliumEnd heliumS) ++  "\t" ++ show (length perrInfo) ++ "\n")
        
        (putStrLn . show) (extt/20)

        (putStrLn . show) (rept/20)
        return ()
          
  --code from end of original main below
  --return (make filePath' newFullName lvmPath [moduleName] options doneRef)
  --return results

featureExtractionTime errInfo topLevelTypes subs combinedEnv localEnv importEnv pp filename = do
    start <- getCurrentTime
    
    mapM ( \x -> 
        getFeatures errInfo topLevelTypes subs combinedEnv localEnv importEnv pp filename >>= \perrInfo -> printResult concisePrint perrInfo [] "" filename) [1..numIterations]
    
    perrInfo <-  getFeatures errInfo topLevelTypes subs combinedEnv localEnv importEnv pp filename
    
    mid <- getCurrentTime
    
    mapM ( \x -> printResult concisePrint perrInfo [] "" filename) [1..numIterations]
    
    end <- getCurrentTime
    
    return ((diffUTCTime mid start) - (diffUTCTime end mid))
    -- return (diffUTCTime instant start)
    
expRepTime topLevelTypes subs combinedEnv localEnv importEnv info perrInfo = do
    start <- getCurrentTime

    mapM ( \x -> 
        repExp topLevelTypes subs combinedEnv localEnv importEnv 1 info (map snd perrInfo) (length info > 1) >>= mapM (putStrLn . interpret1 . snd) ) [1..numIterations]

    mid <- getCurrentTime

    reparations <- repExp topLevelTypes subs combinedEnv localEnv importEnv 1 info (map snd perrInfo) (length info > 1)

    
    mapM (\x -> mapM (putStrLn . interpret1 . snd) reparations) [1..numIterations]    
    
    end <- getCurrentTime
    
    -- return (diffUTCTime instant start)
    return ((diffUTCTime mid start) - (diffUTCTime end mid))
    
--might need to reconstruct make to do some of the original utility stuff in
--Helium. Right now, compile is directly called from main for simplicity
make basedir fullName lvmPath chain options doneRef = do
  newDone <- readIORef doneRef
  return (compile basedir fullName options lvmPath (map fst newDone))

