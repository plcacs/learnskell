{-# LANGUAGE TypeSynonymInstances, FlexibleInstances #-} 

module ExpRepHelper where

import Control.Applicative    
import Helium.Main.PhaseLexer
import Helium.Main.PhaseParser
import Helium.Main.PhaseImport
import Helium.Main.PhaseResolveOperators
import Helium.Main.PhaseStaticChecks
import Helium.Main.PhaseKindInferencer
import Helium.Main.PhaseTypingStrategies
import Helium.Main.PhaseTypeInferencer
import Helium.Main.PhaseDesugarer
import Helium.Main.PhaseCodeGenerator
import Helium.Main.CompileUtils
import Helium.StaticAnalysis.Messages.Messages
import Helium.StaticAnalysis.Messages.TypeErrors
import Helium.StaticAnalysis.Messages.TErrors
import Helium.StaticAnalysis.Messages.Warnings(Warning)
import Helium.StaticAnalysis.Inferencers.TypeInferencing(typeInferencing)
import Helium.ModuleSystem.DictionaryEnvironment (DictionaryEnvironment)
--import UHA_Syntax
import Helium.StaticAnalysis.Messages.Information (showInformation)
-- import System.FilePath.Posix
import Helium.Utils.Utils
import qualified Control.Exception as CE (catch, IOException)
import Data.IORef
import Helium.StaticAnalysis.Messages.StaticErrors(errorsLogCode)
import Helium.Parser.Parser(parseOnlyImports)
import Control.Monad
import System.FilePath(joinPath)
import Data.List(intersect,lookup,foldl', nub, elemIndex, isSuffixOf, intercalate,(\\),sortBy, findIndex,minimumBy, find)
import Data.Maybe
import Data.Map(lookup,member,(!))
import Lvm.Path(explodePath,getLvmPath)
import System.Directory(doesFileExist, getModificationTime)
import Helium.Main.Args
import Paths_helium
import Top.Types
import Top.Types.Schemes
import Helium.StaticAnalysis.Miscellaneous.DoublyLinkedTree
import Helium.StaticAnalysis.Miscellaneous.UHA_Source
import Helium.Syntax.UHA_Syntax
import Helium.Parser.OperatorTable(Assoc(AssocLeft,AssocRight,AssocNone))
import Helium.Utils.OneLiner
import FEState
import TypeDiffNewNew
import Data.List.Split(splitOn)
import Data.Function
import Data.Maybe
import Data.List (intersperse)


data ErrorKind = Isolated | Paired deriving (Eq, Show)

data Rep = AddParIT InfoTree InfoTree String
         | AddBrac InfoTree Int String
         | RmBrac InfoTree Int String -- 
         | Error String String  -- Needs reordering after
         -- | RmBraVag Int     -- Vaguely says adding somewhere
         -- | AddBraVag Int    -- Vaguely says removing somewhere
         | Seq Rep Rep 
         | Hint String String
         | Pull InfoTree Int String  -- Pull the last argument in the InfoTree out
         -- Pull at least 2 last arguments and add a pair of parentheses 
         -- around them.
         | PullAddPar InfoTree Int String 
         | Concrete [Rep] String
         | None
          deriving (Show)

interpret1 None = ""
interpret1 (Seq r1 r2) = interpret1 r1 ++ "\n" ++ interpret1 r2
interpret1 (Hint str _) = str
interpret1 (Error str _) = "Error" ++ str ++ "(Will be removed in final test.)"

interpret1 (Pull extraArgNode num _) = 
    let all = children extraArgNode
        toBePulled = drop (length all - num) all
    in 
       if length toBePulled < num
       -- then error $ "interpret1: Pull: can pull out: " ++ show (length toBePulled) ++ " while required: " ++ show num
       then interpret1 (Error ("interpret1: Pull: can pull out: " ++ show (length toBePulled) ++ " while required: " ++ show num) "")
       else "Pull *" ++ (concat . intersperse " " . map showITJust) toBePulled ++"* out of the parentheses"       

interpret1 (PullAddPar extraArgNode num _) = 
    let all = children extraArgNode
        toBePulled = drop (length all - num) all
    in 
       if length toBePulled < num
       -- then error $ "interpret1: Pull: can pull out: " ++ show (length toBePulled) ++ " while required: " ++ show num
       then interpret1 (Error ("interpret1: Pull: can pull out: " ++ show (length toBePulled) ++ " while required: " ++ show num) "")
       else "Pull *" ++ (concat . intersperse " " . map showITJust) toBePulled ++"* out of the parentheses, and add a pair of () around them."  
       
interpret1 (AddParIT st ed _) =
    "Add an open parenthesis before *" ++ showITJust st
    ++ "* and a closing parenthesis after *" ++ showITJust ed ++ "*"

interpret1 (AddBrac it lev _) = "Add " ++ show lev ++ " [] at " ++ showITJust it ++ errLoc it
interpret1 (RmBrac it lev _) = "Remove " ++ show lev ++ " [] at " ++ showITJust it ++ errLoc it

interpret1 (Concrete fixes header) = "\n\t" ++ header ++ "\n"++ concatMap (\(x,y) ->'\t' : show x ++ "): " ++ interpret1 y++"\n") (zip ['a'..] fixes)

    
interpret1 rep = error $ "Unhandled interpretation, Rep:" ++ show rep          

extractHeader :: Rep -> String
extractHeader (AddParIT _ _ header) = header
extractHeader (AddBrac _ _ header) = header
extractHeader (RmBrac _ _ header) = header
extractHeader (Error _ header) = header
extractHeader (Seq lft _) = extractHeader lft
extractHeader (Hint _ header) = header
extractHeader (Pull _ _ header) = header
extractHeader (PullAddPar _ _ header) = header
extractHeader (Concrete _ header) = header
extractHeader None = ""

instance Show InfoTree where
    show itt = showITJust itt

lgTv1 = TVar 1000000
lgTv2 = TVar 1000001


symSq = "[]"
symPar = "()"
symDollar = "$"
symParam = "Para"
symArg= "Arg"


strAdd = replicate 5 '+'
strRm = replicate 5 '-'

locAnnot = "Annot"
locBody = "Body"

pairedYes = "Paired"
pairedNo = "NotPaired"    

likelyYes = "Likely"
likelyNo = "Definite"

compSep = ";"

generateHeader sym str loc paired likely caller num = 
    let comps = [sym,str,loc,paired,likely,caller,show num]
    in concat . intersperse compSep $ comps


type ErrorInfo = (TypeError,ConstraintInfo)

showIT = showOneLine 80 . oneLinerSource . self . attribute . fromJust
errLoc = show . rangeOfSource . self . attribute
showITJust = showOneLine 80 . oneLinerSource . self . attribute
showUHA = showOneLine 80 . oneLinerSource

uha_source = self . attribute

siblingAtAsExpr :: Int -> InfoTree -> Maybe Expression 
siblingAtAsExpr idx offender  = 
    case offenderParent offender of
        Just (UHA_Expr (Expression_NormalApplication _ fun args)) -> 
            if (self . attribute) offender == UHA_Expr fun && length args >= idx 
            then Just (args !! (idx -1))
            else Nothing
        _ -> Nothing
        
siblingAtAsIT :: Int -> InfoTree -> Maybe InfoTree
siblingAtAsIT idx offender = do
    op <- parent offender
    i <- (findIndex (\x -> uha_source x == uha_source offender) . children) op
    if i + idx < (length . children) op && i + idx >= 0
       -- then error $ "i+idx: " ++ show (i)
       then Just (children op !! (i+idx))
       else Nothing

numRgtSiblings :: InfoTree -> Int
numRgtSiblings offender = 
    let Just op = parent offender
        remaining = dropWhile (\x -> uha_source x /= uha_source offender) (children op)
    in length remaining - 1

lgCuttOff = 1000
    
predecessors :: InfoTree -> [(String,Int,InfoTree)]
predecessors = predecessors' 1 lgCuttOff

predecessors' :: Int -> Int -> InfoTree -> [(String,Int,InfoTree)]
predecessors' height cutoff here 
    | height > cutoff = []
predecessors' height cuttoff here 
    | stop here = []
predecessors' height cutoff here  = 
    case parent here of
        Just par -> (showITJust par,height,par) : predecessors' (height+1) cutoff par 
        Nothing  -> []

predecessorsWithin = predecessors' 1 

stop inf = isRoot inf || (isFB . self . attribute) inf
                      || (rhsTest . self . attribute) inf

isRoot :: InfoTree -> Bool
isRoot iTree = isNothing $ parent iTree

isNode :: InfoTree -> Bool
isNode iTree = (not $ isRoot iTree) && (not $ null $ children iTree)

isLeaf :: InfoTree -> Bool
isLeaf iTree = (not $ isRoot iTree) && (null $ children iTree)

isFB = fbTest

fbTest (UHA_FB _) = True
fbTest _ = False

declTest (UHA_Decl _) = True
declTest _ = False

fbBody (UHA_FB fb) = Just fb
fbBody _ = Nothing

data NodeType = Root | Node | Leaf deriving (Show,Eq)

nodeType node 
    | (isRoot node) = Root
    | (isNode node) = Node
    | otherwise  = Leaf

getFB tree
    | nodeType tree == Root = tree
    | (fbTest ((self . attribute) tree)) == True = tree
    | (declTest ((self . attribute) tree)) == True = tree
    | otherwise = getFB (fromJust $ parent tree)
    
    
rhs :: InfoTree -> UHA_Source
rhs infTree 
    | rhsTest src = src
    | (isNothing . parent) infTree = error "rhs failed to find parent"
    | otherwise = rhs . fromJust . parent $ infTree
    where src = (self . attribute) infTree

sameRHS :: ErrorInfo -> ErrorInfo -> Bool
sameRHS lft rgt = 
    (rhs . offendingInfoTree . snd) lft == (rhs . offendingInfoTree . snd) rgt

sameFB :: ErrorInfo -> ErrorInfo -> Bool
sameFB lft rgt = 
    let func = self . attribute . getFB . offendingInfoTree . snd
    in func lft == func rgt
    
firstComPredIT :: ErrorInfo -> ErrorInfo -> String
firstComPredIT lft rgt = 
    let predfunc = predecessors . offendingInfoTree . snd
        lpreds = predfunc lft
        rpreds = predfunc rgt
    in firstComPred lpreds rpreds

firstComPred :: [(String,Int,InfoTree)] -> [(String,Int,InfoTree)] -> String
firstComPred left right = 
    let leftNms = nub . map fst3 $ left
        rgtNms  = nub . map fst3 $ right
    -- in error $ minimumBy (compare `on` length) $ intersect leftNms rgtNms
    in minimumBy (compare `on` length) $ intersect leftNms rgtNms
    -- in error $ "***\n" ++ concatMap (\n -> n ++ "\n") leftNms
                          -- ++ concatMap (\n -> n ++ "\n") rgtNms
    -- in error $ "***\n" ++ concat (intersect leftNms rgtNms)
        
comPredDist :: [(String,Int,InfoTree)] -> [(String,Int,InfoTree)] -> (Int,Int)
comPredDist lft rgt = 
    let predNm = firstComPred lft rgt
        dist = snd3 . fromJust . find ((== predNm) . fst3)
    in (dist lft, dist rgt)
    
-- The number of generation within which the two nodes having the same
-- predecesor are considered as close
generN = 2

sameGen, closeEnough, closeEnoughDollar :: ErrorInfo -> ErrorInfo -> Bool    
closeEnough = closeEnoughBy generN
closeEnoughDollar = closeEnoughBy 3


-- It turns out that this function is particularly
-- tricky. We will just do something simple with
-- the functoin shareDollarSimple
-- shareDollar :: ErrorInfo -> ErrorInfo -> Bool
-- shareDollar lft rgt = 
    -- let predfunc = predecessors . offendingInfoTree . snd
        -- lpreds = predfunc lft
        -- rpreds = predfunc rgt
        -- fstPred = firstComPred lpreds rpreds
    -- in closeLines lft rgt && sameFB lft rgt && '$' `elem` fstPred
    -- in error $ concatMap (\(n,i,z) -> n ++ "\t" ++ show i ++ "\n" ) lpreds ++
               -- concatMap (\(n,i,z) -> n ++ "\t" ++ show i ++ "\n" ) rpreds

shareDollarSimple :: ErrorInfo -> ErrorInfo -> Bool
shareDollarSimple lft rgt = 
    let predStr = concatMap fst3 . predecessorsWithin 4 . offendingInfoTree . snd $ lft
    in closeLines lft rgt && sameFB lft rgt && '$' `elem` predStr

lineFromErrInfo errInf =      
    let getLine (Range_Range (Position_Position _ line _ ) _) = line
        linefunc = getLine . rangeOfSource . self . attribute . offendingInfoTree . snd
    in linefunc errInf
    
closeLines :: ErrorInfo -> ErrorInfo -> Bool
closeLines lft rgt = lineFromErrInfo rgt - lineFromErrInfo lft <= 0
    -- in error $ showITJust (offendingInfoTree $ snd lft) ++ "\n" ++ showITJust (offendingInfoTree $ snd rgt) ++ show (linefunc rgt - linefunc lft)
        
    
closeEnoughBy :: Int -> ErrorInfo -> ErrorInfo -> Bool
closeEnoughBy distTolerance lft rgt = 
    let predfunc = predecessors . offendingInfoTree . snd
    in sameRHS lft rgt && 
       comPredDist (predfunc lft) (predfunc rgt) <= (distTolerance,distTolerance) 


sameGen lft rgt = 
    let predfunc = predecessors . offendingInfoTree . snd
        (dist1,dist2) = comPredDist (predfunc lft) (predfunc rgt) 
    in dist1 == dist2

          
isComp (TypeDiff _ [] (-1) _ _) (TypeDiff _ [] 1 _ _)  = True
isComp (TypeDiff _ [] (-1) _ _) (TypeDiff _ [] 2 _ _)  = True
isComp (TypeDiff _ [] (-2) _ _) (TypeDiff _ [] 2 _ _)  = True
isComp _ _ = False
    
rhsTest (UHA_RHS _) = True
rhsTest _ = False

--uses applicative functor <*> to pair subsequent elements in a tuple for a given list
pairs = zip <*> tail


findInTree :: [InfoTree] -> UHA_Source -> [InfoTree]
findInTree infs src 
    | null matches = concatMap (\inf -> findInTree (children inf) src) infs
    | otherwise    = matches
    where matches = filter ((src ==) . self . attribute) infs

offender = self . attribute . offendingInfoTree . snd
offenderParent = fmap (self . attribute) . parent

-- Function gets the actual error cause
-- Gets the last component, no matter if there are 1, 2, or 3 
-- components
offendingInfoTree :: ConstraintInfo -> InfoTree
offendingInfoTree = fromJust . offenderTree


-- The folloiwng function is used by Peter to decide the
-- correct actual error cause
-- Get the first component if there is only one and the second
-- component if there are 2 or 3 components.
getTreeForSrc :: ConstraintInfo -> InfoTree
getTreeForSrc (CInfo_ _ (_,Nothing) infoTree _ _ _ _ flag) = infoTree
getTreeForSrc (CInfo_ _ (_,Just offender) infoTree _ _ _ _ flag) 
    | null matches = infoTree
    | otherwise    = head matches
    where matches = findInTree [infoTree] offender

isLeft, isRight :: Either a b -> Bool
isLeft = either (const True) (const False)
isRight = either (const False) (const True)

sameLevLetWhereVal, sameLevLetWhereFunc :: InfoTree -> [String]
sameLevLetWhereVal info 
    | isNothing parfb = []
    | isDecls decls = concatMap extractVal (binds decls)
    | otherwise = []
    where fb = getFB info
          parfb = parent fb
          decls = self . attribute . fromJust $ parfb
          isDecls (UHA_Decls _) = True
          isDecls _ = False

          binds (UHA_Decls decs) = decs
          
          -- extractVal (UHA_Decl (Declaration_PatternBinding _ (Pattern_Variable _ val) _)) = [getNameName val]
          extractVal (Declaration_PatternBinding _ (Pattern_Variable _ val) _) = [getNameName val]
          extractVal _ = []

sameLevLetWhereFunc info 
    | isNothing parfb = []
    | isDecls decls = concatMap extractFunc (binds decls)
    | otherwise = []
    where fb = getFB info
          parfb = parent fb
          decls = self . attribute . fromJust $ parfb
          isDecls (UHA_Decls _) = True
          isDecls _ = False

          binds (UHA_Decls decs) = decs
          
          extractFunc (Declaration_FunctionBindings _ (FunctionBinding_FunctionBinding _ (LeftHandSide_Function _ func _) _:_) ) = [getNameName func]
          extractFunc _ = []
          

getPats :: InfoTree -> Patterns
getPats offender = (collectPats . self. attribute) offender
    where collectPats (UHA_FB (FunctionBinding_FunctionBinding _ (LeftHandSide_Function _ name argList) _)) = argList
          collectPats (UHA_FB (FunctionBinding_FunctionBinding _ (LeftHandSide_Infix _ pl _ pr) _)) = [pl,pr]
          collectPats (UHA_Decl (Declaration_PatternBinding _ pat _ )) = []

-- The input to getFuncName must be a return value from getFB
getFuncName :: InfoTree -> String
getFuncName offender = (getName . self . attribute) offender
    where getName (UHA_FB (FunctionBinding_FunctionBinding _ (LeftHandSide_Function _ name argList) _)) = getNameName name
          getName (UHA_FB (FunctionBinding_FunctionBinding _ (LeftHandSide_Infix _ pl func pr) _)) = getNameName func
          getName (UHA_Decl (Declaration_PatternBinding _ pat _ )) = (showUHA . UHA_Pat) pat
    

    
{-
withinList pats x 
decides whether x is in any of pats and is wrapped within a []
-}
withinList :: String -> Patterns -> Bool
withinList str = any (withinList' str)
    where withinList' str pat = 
            let patStr = (showUHA . UHA_Pat) pat
                (lpat:_) = splitOn str patStr
                numLB = (length . filter (=='[')) lpat
                numRB = (length . filter (==']')) lpat
            in numLB /=0 && numLB /= numRB

            
canIncrBrac,canDecrBrac :: String -> Patterns -> Bool
canIncrBrac str = any (canIncrBrac' str)
    where canIncrBrac' str pat = 
            let patStr = (showUHA . UHA_Pat) pat
                (lpat:rpat:_) = splitOn str patStr ++ [""]
            in ':' `elem` rpat

canDecrBrac str = any (canDecrBrac' str)
    where canDecrBrac' str pat = 
            let patStr = (showUHA . UHA_Pat) pat
                (lpat:rpat:_) = splitOn str (patStr++"|||") ++ [""]
            in ':' `elem` lpat && not (null rpat)
            -- in error $ lpat ++ "\t" ++ rpat
               
parAndGrandParOfOff :: InfoTree -> (Maybe InfoTree, Maybe InfoTree)
parAndGrandParOfOff infoT = 
    let par = parentOfOffender infoT
        grandPar = fmap parentOfOffender par
    in (par, join grandPar)
          
{-
Get the top node for a given complicated expression

topNode (x ++ y ++ z) -> first ++
topNode (map x y) -> map
-}
topNode :: InfoTree -> Maybe InfoTree
topNode node = 
    case (self . attribute) node of
        UHA_Expr (Expression_NormalApplication _ _ _) -> Just (children node !! 0)
        UHA_Expr (Expression_InfixApplication _ MaybeExpression_Nothing _ _) -> Nothing
        UHA_Expr (Expression_InfixApplication _ _ _ _) -> Just (children node !! 1)
        _ -> Nothing

getExpNode :: InfoTree -> InfoTree
getExpNode node =
    case (self . attribute) node of
      UHA_RHS (RightHandSide_Expression _ _ _) -> head . children $ node
      UHA_Expr _ -> node
      
parentOfOffender :: InfoTree -> Maybe InfoTree
parentOfOffender infoT = join . fmap topNode . parent $ infoT

    -- Old implementation. Now calls topNode
    -- | isNothing parOff = Nothing
    -- | otherwise =
        -- case (self . attribute . fromJust) parOff of
            -- UHA_Expr (Expression_NormalApplication _ _ _) -> Just (children (fromJust parOff) !! 0)
            -- UHA_Expr (Expression_InfixApplication _ _ _ _) -> Just (children (fromJust parOff) !! 1)
            -- _ -> Nothing
    -- where parOff = parent infoT

            
isNone None = True
isNone _ = False


{-
See each function for the concrete tree structure
-}
data ErrorTree = ErrorTree {
                    child :: InfoTree,
                    par :: InfoTree,
                    grandPar :: InfoTree,
                    rotateDn :: InfoTree
                    }

instance Show ErrorTree where
    show (ErrorTree chd par grandPar rn ) = show chd ++ "\t" ++ show par ++ "\t" ++ show grandPar ++ "\t" ++ show rn
                    
{-
getTreeRR iTree
Try to get relevant tree parts for rotating the 
tree in the following scheme
-}
{-
        grandPar                               @
        /     \                             /     \
       /       \                           /       \
      @       rotateDn      ---->         /      grandPar
   /    \                                /        /   \
  /      \                              /        /     \
par    child                           par     child  rotateDn
-}          

isInfixApp, isNormApp :: UHA_Source -> Bool
isNormApp (UHA_Expr (Expression_NormalApplication _ _ _)) = True
isNormApp _ = False

isInfixApp ( UHA_Expr (Expression_InfixApplication _ _ _ _)) = True
isInfixApp _ = False

-- canRotateRR :: ErrorTree -> Bool
canRotateRR sub env (ErrorTree chd par grandPar rotateDn) = do
    let expand tp = expandType (snd $ getOrderedTypeSynonyms env) tp
    
    chdType <- fmap (expand . (sub |-> )) (assignedType . attribute $ chd)
    parType <- fmap (expand . (sub |-> )) (assignedType . attribute $ par)
    gParType <- fmap (expand . (sub |-> )) (assignedType . attribute $ grandPar)
    rotateDnType <- fmap (expand . (sub |-> )) (assignedType . attribute $ rotateDn)
    
    let (args,_) = functionSpine parType
    let unified = unifiable noOrderedTypeSynonyms gParType (chdType .->. rotateDnType .->. last args)
    
    -- return $ show chdType ++ "    " ++ show parType ++ "    " ++ show gParType ++
       -- "    " ++ show rotateDnType ++ "    Unifiable? " ++ show unified
    
    -- return $ "Unifiable? " ++ show unified
        
    return unified 
    
getTreesRR :: InfoTree -> Maybe ErrorTree
getTreesRR iTree = do
    (par,child) <- 
      if isNormApp . self . attribute $ iTree 
      then Just (head . children $ iTree, last . children $ iTree)
      else if (not . isLeaf $ iTree) then Nothing
           else do         
             inclusive <- parent iTree
             let allChild = children inclusive
               
             if isNormApp . self . attribute $ inclusive 
             then Just (head allChild, last allChild)
             else Nothing

    -- (par,child) <- do
        -- inclusive <- parent iTree
        -- let allChild = children inclusive

        -- if isNormApp . self . attribute $ inclusive 
        -- then Just (head allChild, last allChild)
        -- else Nothing

    -- error $ show par ++ "\t" ++  show child
    parpar <- parent par
    parparpar <- parent parpar
    
    (grandPar,rotateDn)
        <- if (isInfixApp . self . attribute) parparpar 
              &&
              showITJust (children parparpar !! 0) == showITJust parpar
           then Just (children parparpar !! 1, children parparpar !! 2) 
           else Nothing
           
    return $ ErrorTree child par grandPar rotateDn

-- Decide if par and child are already parenthesized.
-- If so, don't suggest rotate    
alreadyParenthesizedRR :: InfoTree -> Bool
alreadyParenthesizedRR actIT =
    let isParenthesizedRR (UHA_Expr ((Expression_InfixApplication _ (MaybeExpression_Just (Expression_Parenthesized _ _ )) _ _) )) = True
        isParenthesizedRR _ = False
    in if isLeaf actIT 
       then isParenthesizedRR . self . attribute . fromJust . parent . fromJust . parent $ actIT 
       else isParenthesizedRR . self . attribute . fromJust . parent $ actIT 
    

-- canRegroupRR :: InfoTree -> Bool
canRegroupRR sub env actIT  = 
    case getTreesRR actIT of
        Nothing -> False
        Just errTrees -> maybe False id (canRotateRR sub env errTrees ) && (not . alreadyParenthesizedRR) actIT
    


                    



                    