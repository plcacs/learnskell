#!/bin/bash

pattern1="explicitly typed binding"
pattern2="infix pattern application"
#....do not ask why....I do not know.....
cp $1 test.hs

if [ "`helium -w test.hs | grep -e "$pattern1" -e "$pattern2"`" ]; then
	echo "-W -ml"
else
	echo "-ml"
fi

rm test.hs
