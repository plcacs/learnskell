module ExpRepBrac where

import Helium.Main.CompileUtils
import Helium.StaticAnalysis.Messages.Messages
import Helium.StaticAnalysis.Messages.TypeErrors
import Helium.StaticAnalysis.Messages.TErrors
import Helium.StaticAnalysis.Messages.Warnings(Warning)
import Helium.StaticAnalysis.Inferencers.TypeInferencing(typeInferencing)
import Helium.ModuleSystem.DictionaryEnvironment (DictionaryEnvironment)
--import UHA_Syntax
import Helium.StaticAnalysis.Messages.Information (showInformation)
-- import System.FilePath.Posix
import Helium.Utils.Utils

import Top.Types
import Top.Types.Schemes
import Helium.StaticAnalysis.Miscellaneous.DoublyLinkedTree
import Helium.StaticAnalysis.Miscellaneous.UHA_Source
import Helium.Syntax.UHA_Syntax
import Helium.Parser.OperatorTable(Assoc(AssocLeft,AssocRight,AssocNone))
import Helium.Utils.OneLiner
import FEState
import TypeDiffNewNew
import ExpRepHelper
import FeatureExtraction

import Data.List.Split(splitOn,splitOneOf)
import Data.Function
import Data.Maybe
import Data.List (intersperse,lookup,intersect)
import Data.Map (lookup)
import Control.Monad
import Helium.Syntax.UHA_Utils

repBracConArgAdd sub combinedEnv actIT note =
    let argName = showITJust actIT
        pats = (getPats . getFB) actIT
        funcName = getFuncName . getFB $ actIT
                
        fix1 = if withinList argName pats 
               then Hint "Remove the [] in the pattern" ""
               else None
               
        parName = fmap showITJust (parentOfOffender actIT)
        
        fix2 = case (canIncrBrac argName pats, Just funcName == parName) of
                (True, True) -> Hint ("Change: " ++ argName ++ " to tail of the pattern") ""
                (True,False) -> Hint ("Change: " ++ argName ++ " to the whole pattern") ""
                (False, _) -> Hint ("Change: " ++ argName ++ " itself") ""
                
        fix3 = Hint ("Change the type annotation of: " ++ funcName) ""
        fix4 = maybe None (\x -> Hint ("Change the use around: " ++ x) "") parName
        
        fix5 = Hint ("Change other places that use: " ++ argName) ""
        reGroupRR = repRegroup sub combinedEnv actIT

        fixes = [fix1, fix2, reGroupRR, fix4, fix3, fix5]
    in Concrete (filter (not . isNone) fixes) note

repBracConArgRm sub combinedEnv actIT note =
    let argName = showITJust actIT
        pats = (getPats . getFB) actIT
        funcName = getFuncName . getFB $ actIT
                       
        parName = fmap showITJust (parentOfOffender actIT)
        
        fix1 = if canDecrBrac argName pats
               then Hint ("Change: " ++ argName ++ " to the fst part of pattern") ""
               else Hint ("Change: " ++ argName ++ " itself") ""
               
        fix2 = maybe None (\x -> Hint ("Use another function instead of: " ++ x) "") parName
                
        fix3 = Hint ("Change the type annotation of: " ++ funcName) ""
        
        fix4 = Hint ("Change other places that use: " ++ argName) ""
        reGroupRR = repRegroup sub combinedEnv actIT

        fixes = [ fix1, fix2, reGroupRR, fix3,  fix4]
    in Concrete (filter (not . isNone) fixes) note
    
repBracConExp sub combinedEnv actualSrc diff actType libOrUser note =
    let chgArg = repBracByArg sub combinedEnv actualSrc diff note
        chgToSibling = repBracInfixToSibling sub combinedEnv actualSrc diff actType note
        rmFst = repRmFirst sub combinedEnv actualSrc diff note 
        rmPar = repRmPar sub combinedEnv actualSrc diff note

        expName = showITJust actualSrc
        parName = fmap showITJust (parentOfOffender actualSrc)
        funcName = getFuncName . getFB $ actualSrc
        
        fstChd = (showITJust . head . children) actualSrc 
        
        chgItself = case (isNone rmFst, diff < 0) of
                      (False,_) -> None
                      (True,True) -> Hint ("Make changes in: " ++ expName ++ errLoc actualSrc ++". Eg: add 'head', 'concat', 'concatMap' or change: " ++ fstChd) ""
                      (True,False) -> Hint ("Make changes in: " ++ expName ++ errLoc actualSrc) ""
                     
        chgPar = case (isNone rmPar, libOrUser) of
                    (False,_) -> None
                    -- a lib function, replaces it
                    (True,1) -> maybe None (\x -> Hint ("Make changes around: " ++ x ++ ". Eg: uses another function.") "") parName
                    (True,2) -> maybe None (\x -> Hint ("Make changes around: " ++ x ++ ". Eg: change the definition of: " ++ x) "") parName
                    
                    (True,_) -> maybe None (\x -> Hint ("Make changes around: " ++ x) "") parName
                    
        chgAnnot = Hint ("Change the type annotation of: " ++ funcName) ""
        reGroupRR = repRegroup sub combinedEnv actualSrc
    in Concrete (filter (not . isNone) [chgToSibling, rmFst, rmPar, chgArg, reGroupRR, chgItself,chgPar,chgAnnot]) note
    

infixChgMapSndAdd = [(":","++")]
infixChgMapFstRm = [(":","++")]
infixChgMapSndRm = [("++", ":")]
infixChgMapFstAdd = [("++", ":")]



repBracInfixToSibling sub combinedEnv off diff act note
    -- | True = error $ show diff
    | isNothing parOffM = None
    | not isInfix = None
    | not (isLftChdInfix off parOff) &&
      not (isRgtChdInfix off parOff) = None
    | abs diff > 1 = None
    -- | isNothing siblingTpMb = None
    | unifRes == Just True = Hint ("Change: "  ++ origOp ++ errLoc off ++ "to: " ++ fromJust siblingMb) ""
    | otherwise = None
    where parOffM = parentOfOffender off
          parOff = fromJust parOffM
          origOp = showITJust parOff
          
          isInfix = isParInfix parOff 
                    
          chgMap = case (isLftChdInfix off parOff, diff) of
                    (True,1) -> infixChgMapFstAdd
                    (True,-1) -> infixChgMapFstRm
                    (False,1) -> infixChgMapSndAdd
                    (False,-1) -> infixChgMapSndRm
                    
          siblingMb = Data.List.lookup origOp chgMap
          spies = fmap spines $ join (fmap (queryType combinedEnv) siblingMb)
          
          idx = if isLftChdInfix off parOff then 0 else 1
          expt = fmap (!! idx) spies
          
          unifRes = fmap (unifiable noOrderedTypeSynonyms act) expt
          
repRmPar subs combinedEnv actualSrc diff note
    | isNothing chgDiffM = None
    | diff + chgDiff == 0 = Hint ("Remove: " ++ showIT offPar ++ (show . rangeOfSource . self . attribute . fromJust) offPar) ""
    | otherwise = None
    where 
          offPar = parentOfOffender actualSrc
          parTpM = fmap (assignedType . attribute) offPar
          chgDiffM = fmap (\x -> bracChg combinedEnv (subs |->x)) (join parTpM)
        
          chgDiff = fromJust chgDiffM

repRmFirst subs combinedEnv actualSrc diff note 
    -- | True = error $ show diff ++ show chgDiff ++ showITJust fstChd
    | null . children $ actualSrc = None
    | isNothing tpFstChd = None
    | chgDiff == 0 = None
    | diff > 0 && chgDiff < 0 && diff + chgDiff >=0 
      || diff < 0 && chgDiff > 0 && diff + chgDiff <= 0 = 
        Hint ("Remove/change the identifier: " ++ showITJust fstChd ++ (show . rangeOfSource . self . attribute) fstChd) ""
    | otherwise = None
    where fstChd = head . children $ actualSrc
          tpFstChd = (assignedType . attribute) fstChd
          chgDiff = bracChg combinedEnv (subs |-> fromJust tpFstChd)

{-
Decide how []s change from the argument type to the return type

Egs:

bracChg (Int -> [Int]) = 1
bracChg ([[Int]] -> [Int]) = -1
bracChg (Int -> Int) = 0
bracChg (Int -> Bool -> Int) = 0
-}
bracChg :: ImportEnvironment -> Tp -> Int
bracChg ienv tp
    | numComps /= 2 = 0
    | null bracs = 0
    | otherwise = (snd . head) bracs
    where synonyms = snd $ getOrderedTypeSynonyms ienv
          tp1 = expandType synonyms tp
          numComps = (length . spines) tp1
          
          [arg,ret] = spines tp1
          
          TypeDiff bracs _ _ _ _ = typeDiff arg ret
          

-- 
-- lftChdInfix chd par
-- Deciding if chd is the left child of the infix application par
isLftChdInfix, isRgtChdInfix :: InfoTree -> InfoTree -> Bool
isLftChdInfix chd par = 
    isParInfix par && uha_source chd == (uha_source . fromJust) (siblingAtAsIT (-1) par)
    
isRgtChdInfix chd par = 
    isParInfix par && uha_source chd == (uha_source . fromJust) (siblingAtAsIT 1 par)
        
isParInfix :: InfoTree -> Bool
isParInfix par 
    | (isNothing . parent) par = False
    | otherwise = 
        case (self . attribute . fromJust . parent) par of 
            UHA_Expr (Expression_InfixApplication _ _ func _) -> True
            _ -> False
          
queryType :: ImportEnvironment -> String -> Maybe Tp
queryType env@(ImportEnvironment tcs tss te vcs ot _) str =
    let name = nameFromString str
        mtp = Data.Map.lookup name te `mplus` Data.Map.lookup name vcs 
    in fmap (tpScToTpSig env) mtp


{-
Decide which argument types determine the return types.

For example, in 

Int -> a -> [a]

the second argument determines the return type.

In 

a -> [a] -> [a]

both the first and second arguments determine the return

The function whoEffectRetSig returns Just idx
if idx is the only argument that affects the return type
-}
whoAffectRetSig :: Tp -> Maybe Int
whoAffectRetSig tp 
    | null ftvRet || length ftvRet > 1 = Nothing
    | any (\arg -> arg `intersect` ftvRet /= [] && length arg > 1) ftvArgs = Nothing
    | length sameFtv /= 1 = Nothing
    | isFunctionType (args !! (idx - 1) ) = Nothing
    | otherwise = Just idx
    where (args, ret) = functionSpine tp
          ftvRet = ftv ret
          ftvArgs = map ftv args
          
          sameFtv = filter (\x -> snd x == ftvRet) (zip [1..] ftvArgs)
          
          (idx,_) = head sameFtv

repBracByArg sub env actualSrc diff note =
    let arg = recurseDn sub env actualSrc diff
    in case (showITJust arg == showITJust actualSrc, diff > 0) of
        (True,_) -> None
        (False,True) -> Hint ("Add " ++ show diff ++ " [] at " ++ showITJust arg ++ errLoc arg ++ "\t Concrete:RecurseDown") ""
        (False,False) -> Hint ("Remove " ++ show (-diff) ++ " [] from " ++ showITJust arg ++ errLoc arg ++ "\tConcrete:RecurseDown") ""
          
-- This function checks to see if we can blam a single
-- argument of a complex expression
recurseDn sub env actualSrc diff = 
    if isLeaf actualSrc 
    then actualSrc
    else maybe actualSrc 
               (\chd -> recurseDn sub env chd diff)
               (stepDown sub env actualSrc diff)

-- We assume the passed in expression is a subexpression    
stepDown sub env actualSrc diff = do
    let func = head . children $ actualSrc
    let expand tp = expandType (snd $ getOrderedTypeSynonyms env) tp
    
    -- The following querying the type of the func is tricky.
    -- The one in the InfoTree is already initiated, which is not
    -- what we want. So we first get the type from the environment 
    -- and get it from InfoTree only when we that fails
    tpFunc <- queryType env (showITJust func) `mplus`
              fmap (expand . (sub |-> )) (assignedType . attribute $ func)
              
    -- error $ show tpFunc

    idx <- whoAffectRetSig tpFunc

    
    let allChd = children actualSrc 
    if length allChd < idx + 1
    then Nothing
    else do
        let chd = allChd !! idx
        tpChd <- fmap (expand . (sub |-> )) (assignedType . attribute $ chd)
        
        if diff > 0 || diff < 0 && isListType tpChd 
        then Just chd
        else Nothing
    

repRegroup sub env actIT 
    | canRegroupRR sub env actIT && numIdChd < 3 && not recursiveIntoRgt = Hint ("Add an ( before: " ++ showITJust chd ++ errLoc chd ++ " and an ) after: " ++ showITJust rotateDn ++ errLoc rotateDn ++ "\tRegroup;RotateRR;repRegroup") "Regroup;RotateRR;repRegroup"
    | otherwise = None
    where Just (ErrorTree chd _ _ rotateDn) = getTreesRR actIT
          numIdChd = length $ splitOneOf "() " (showITJust chd)
          recursiveIntoRgt = getFuncName (getFB actIT) `elem` splitOneOf "() []" (showITJust rotateDn)

repArityTopOp :: InfoTree -> Rep
repArityTopOp actIT =
    let oper = fromJust . topNode $ actIT
        (lftOp,rgtOp) = getOperands actIT
    in Hint ("Change: " ++ showITJust oper ++ errLoc oper ++ ". May need to further change: " ++ showITJust lftOp ++ errLoc lftOp) "" 
    
        
        
{-
Note that the argument to getOperands must be an infix application node
-}        
getOperands :: InfoTree -> (InfoTree,InfoTree)
getOperands node = 
        case (self . attribute) node of
            UHA_Expr (Expression_InfixApplication _ _ _ _) -> (children node !! 0, children node !! 2)
            _ -> error "The argument to getOperands should be an infix application"

            
          
-- repArityCon :: InfoTree -> Rep
repArityCon combinedEnv localEnv importEnv artIT 
    | isNothing topNMb = None
    | otherwise = Concrete (filter (not . isNone) fixes ) "Concrete:Arity"
    where topNMb = topNode (getExpNode artIT)
          topN = fromJust topNMb
          
          (_,userOrLib) = feature11And17ForIT combinedEnv localEnv importEnv topN
          
          chgTop = case userOrLib of
                    -- lib func
                    1 -> Hint ("Make changes in: " ++ showITJust topN ++ errLoc topN ++ " Eg: use another function") ""
                    2 -> Hint ("Make changes in: " ++ showITJust topN ++ errLoc topN ++ " Eg: change the definition of: " ++ showITJust topN ++ " or use another function") ""
                    3 -> repArityTopOp (getExpNode artIT)
                    
                    _ -> None
                    
          workInProgress = Hint ("Work in progress? Grow: " ++ showITJust artIT ++ errLoc artIT ++ " into a full expression") ""
          
          funcName = getFuncName . getFB $ artIT
          chgAnnot = Hint ("Change the type annotation of: " ++ funcName) ""
    
          fixes = [chgTop, workInProgress, chgAnnot]
    




