module TypeDiffNewNew where

import Top.Types

import Helium.ModuleSystem.ImportEnvironment

import Control.Monad.State
import Data.Map as M hiding (null,map)
import Data.Function
import Data.List (minimumBy, sortBy)

import FEState

tpSchemeDiff :: ImportEnvironment -> TpScheme -> TpScheme -> TypeDiff
tpSchemeDiff ienv tps1 tps2 = 
    let synonyms = snd $ getOrderedTypeSynonyms ienv
        tp1 = expandType synonyms . toType $ tps1
        tp2 = expandType synonyms . toType $ tps2
    in typeDiff tp1 tp2 
    
tpScToTp :: ImportEnvironment -> (TpScheme,TpScheme) -> (Tp,Tp)
tpScToTp ienv (tps1,tps2) = (tpScToTpSig ienv tps1, tpScToTpSig ienv tps2)
    -- let synonyms = snd $ getOrderedTypeSynonyms ienv
        -- tp1 = expandType synonyms . toType $ tps1
        -- tp2 = expandType synonyms . toType $ tps2
    -- in (tp1,tp2)

tpScToTpSig :: ImportEnvironment -> TpScheme -> Tp 
tpScToTpSig ienv tps = 
    let synonyms = snd $ getOrderedTypeSynonyms ienv
    in expandType synonyms . toType $ tps
    
        
    
typeDiff :: Tp -> Tp -> TypeDiff
typeDiff inf exp = 
    let cspines = corSpines inf exp
        cirPairs = makeCircular (zip [1..] cspines)
        llen = length cspines
        featureses = map (\spines -> snd . snd . runState (unifySpines emptySubst llen 1 spines) $ initFEState) cirPairs
    in (orderFeatures . rankFeatures) featureses
    -- in  featureses

orderFeatures  :: TypeDiff -> TypeDiff
orderFeatures TypeDiff {brackets = bs, funcs = fs, topLevelDiff = td,likelyFuncs = lfs, topLevBra = topBra}  
    = TypeDiff {brackets = sortBy (compare `on` fst) bs, 
                funcs    = sortBy (compare `on` fst) fs,
                topLevelDiff = td,
                likelyFuncs  = sortBy (compare `on` fst) lfs,
                topLevBra = topBra}
    
featureScore :: TypeDiff -> Int
featureScore TypeDiff {brackets = bs, funcs = fs, topLevelDiff = td, topLevBra = topBra} 
    = length bs + length fs 
      + if td /= 0 then 1 else 0
      + if topBra /=0 then 1 else 0
    
rankFeatures :: [TypeDiff] -> TypeDiff
rankFeatures fes = let scoredFes = map (\fs -> (featureScore fs, fs)) fes
                   -- in snd $ head scoredFes
                     in snd . minimumBy (compare `on` fst) $ scoredFes
    
makeCircular :: [a] -> [[a]]
-- makeCircular eles = [eles]
makeCircular eles = Prelude.map (\i -> drop i eles ++ take i eles) [0..length eles-1]
    
corSpines :: Tp -> Tp -> [(Tp,Tp)]
corSpines inf exp =
    let (infs',_) = functionSpine inf
        (exps',_) = functionSpine exp
        len = min (length infs') (length exps')
        (infs,inft) = functionSpineOfLength len inf
        (exps,expt) = functionSpineOfLength len exp
    in zip infs exps ++ [(inft,expt)]

unifySpines :: MapSubstitution -> Int -> Int -> [(Int,(Tp,Tp))] -> State FEState MapSubstitution
unifySpines sub _ _ [] = return sub
unifySpines sub llen lev ((idx, (inf, exp)) : res) = 
    case (sub |->inf, sub |-> exp) of
        (TVar i, expt) -> unifyVar False (idx == llen) lev idx (TVar i) exp 
                          >>= \sub' -> unifySpines (sub' @@ sub) llen  lev res
        (inft, TVar i) -> unifyVar True (idx == llen) lev idx (TVar i) inft
                          >>= \sub' -> unifySpines (sub' @@ sub) llen lev res
        ((TApp (TCon c1) inft), (TApp (TCon c2) expt)) -- This won't capture functoin types
            | c1 == c2  -> unifySpines sub llen lev ((idx,(inft,expt)):res)
            | otherwise -> unifySpines sub llen lev res
        (ninf,nexp)
            | isFunctionType ninf && isFunctionType nexp -> 
                let ncspines = corSpines ninf nexp
                in unifySpines sub llen (lev+1) (zip (repeat idx) ncspines) 
                   >>= \nsub -> unifySpines (nsub @@ sub) llen lev res
            | ninf == nexp -> unifySpines sub llen lev res
            -- | otherwise -> recordDiff False (null res) lev idx inf exp
            -- | llen == idx -> error $ show ninf ++ " , " ++ show nexp ++ " , lev: " ++ show lev ++ " , idx: " ++ show idx ++ " , inf: " ++ show inf ++ " , exp: " ++ show exp
            -- | otherwise -> error $ show ninf ++ "\t" ++ show nexp
            | otherwise -> recordDiff False (llen == idx) lev idx ninf nexp
                           >>= \nsub -> unifySpines (nsub @@ sub) llen lev res

unifyVar :: Bool -> Bool -> Int -> Int -> Tp -> Tp -> State FEState MapSubstitution
unifyVar flag end lev idx (TVar i) tp 
    | TVar i == tp = return emptySubst
    | i `elem` ftv tp =     -- This is an error case
             recordDiff flag end lev idx (TVar i) tp
    | ldiff /= 0 = addLikelyFuncs (idx, adjustV flag ldiff) >> 
                   return (singleSubstitution i tp) 
    | otherwise = return (singleSubstitution i tp)
    where ldiff = lenDiff (TVar i) tp
        
spines :: Tp -> [Tp]
spines tp = let (tps,t) = functionSpine tp
            in tps ++ [t]

-- 1st: Input substitution
-- 2nd: Level, the information is recorded only when level < 3
-- 3rd: Index
-- 4th: Inferred type
-- 5th: Expected type
    
recordDiff :: Bool -> Bool -> Int -> Int -> Tp -> Tp -> State FEState MapSubstitution
-- recordDiff flag end lev idx inf exp = error $ show lev
recordDiff flag end lev idx inf exp 
    | lev > 2 = return emptySubst
    | lev == 1 && ldiff /= 0 && end = recTopDiff (adjustV flag ldiff) >> return emptySubst
    | lev == 1 && bdiff /= 0 && (not end || idx == 1) = 
        addBrackets (idx,(adjustV (not end). adjustV flag ) bdiff) >> 
        addLikelyFuncs (idx, (adjustV flag lkFun)) >> return sub
    | lev == 1 && bdiff /= 0 && (end || idx /= 1) = 
        recTopBra ((adjustV (not end). adjustV flag ) bdiff) >> 
        addLikelyFuncs (idx, (adjustV flag lkFun)) >> return sub
    -- Uncomment the following case to return bracket difference in the
    -- return types.
    -- | lev == 1 && bdiff /= 0 = addBrackets (idx,(adjustV (not end). adjustV flag )bdiff) >> return sub
    -- | lev == 1 && ldiff /= 0 && end = error $ show ldiff
    | lev == 1 && ldiff /= 0 && not end = addFuncs (idx,adjustV flag ldiff) >> return emptySubst
    | lev == 2 && ldiff /= 0 && end = addFuncs (idx,adjustV flag ldiff) >> return emptySubst
    -- | otherwise = error "Result"
    | otherwise = return emptySubst
    where ldiff = lenDiff inf exp
          (bdiff,lkFun,sub) = case bracketDiffBoth inf exp of
            Left () -> (0,0,emptySubst)
            Right (diff,lkFun,sub) -> (diff,lkFun, sub)
            
            
adjustV :: Bool -> Int -> Int
adjustV True i = 0 - i
adjustV False i = i
            

bracketDiffBoth :: Tp -> Tp -> Either () (Int,Int,MapSubstitution)
bracketDiffBoth inf exp =
    case bracketDiff 0 inf exp of
        Left () -> case bracketDiff 0 exp inf of 
                    Right (i,j,sub) -> Right (0 - i, 0 - j, sub)
                    answer -> answer
        answer -> answer
        
-- Decides how many extra []s btps has over tp
bracketDiff :: Int -> Tp -> Tp -> Either () (Int,Int,MapSubstitution)
bracketDiff num tp (TVar i)
    | lkDiff /=0 = Right (num, lkDiff, emptySubst)
    | otherwise  = Right (num, 0, singleSubstitution i tp)
    where lkDiff = lenDiff tp (TVar i)
bracketDiff num tp (TApp (TCon "[]") btp) = bracketDiff (num+1) tp btp
bracketDiff num tp btp
    | tp == btp && num /= 0 = Right (num,0,emptySubst)
    | otherwise = Left ()

    
    
lenDiff inf exp = (length . spines) exp - (length . spines) inf

toType :: TpScheme -> Tp
toType (Quantification (qs,nm,(Qualification (_, tp)))) = tp

isListType (TApp (TCon "[]") _) = True
isListType _ = False