module TestTpDiff where

import Top.Types

import Control.Monad.State

import FEState
import TypeDiff

testTypeDiff inf exp = do
    let features = typeDiff inf exp
    
    putStrLn $ "Inferred type: " ++ show inf
    putStrLn $ "Expected type: " ++ show exp
    
    (putStrLn . show) features
    

t1 = listType intType .->. listType intType .->. listType (intType .->. boolType)
t2 = (listType intType .->. boolType) .->. intType .->. listType (intType .->. listType boolType)

t6 = intType
t3 = listType intType    
t4 = (listType intType .->. boolType)
t5 = listType (listType intType .->. boolType)
t7 = listType intType .->. listType intType
t8 = (listType intType .->. boolType) .->. intType
t9 = listType (intType .->. boolType)
t10 = listType (intType .->. listType boolType)

[tva,tvb,tvc,tvd,tve] = map TVar [0..4]

charL = listType charType

t11 = listType tva .->. listType tva .->. listType tva
t12 = charL .->. listType charL .->. listType charL

t13 = (tva .->. tvb) .->. (tvc .->. tva) .->. tvc .->. tvb
t14 = (listType tvd .->. tvd) .->. listType intType .->. intType

t15 = tva .->. tvc .->. tvb

t16 = listType tva
t17 = charL .->. charL

t18 = tva .->. tva .->. boolType
t181 = tva .->. tva .->. listType boolType
t19 = listType intType .->. intType .->. boolType


t20 = (tva .->. tvb) .->. listType tva .->. listType tvb
t21 = (listType tvc .->. intType) .->. listType (listType stringType .->. intType)

t22 = listType tvc
t23 = listType stringType .->. intType

t24 = listType tva .->. tva
t25 = (intType .->. stringType) .->. intType

t26 = listType (intType .->. stringType)
t27 = stringType

t28 = (tva .->. boolType) .->. (tva .->. tva) .->. tva .->. tva
t29 = (listType tvb .->. boolType) .->. (listType stringType .->. listType stringType) .->. stringType .->. stringType









