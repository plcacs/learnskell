module FeatureExtraction where

import Control.Applicative    
import Helium.Main.PhaseLexer
import Helium.Main.PhaseParser
import Helium.Main.PhaseImport
import Helium.Main.PhaseResolveOperators
import Helium.Main.PhaseStaticChecks
import Helium.Main.PhaseKindInferencer
import Helium.Main.PhaseTypingStrategies
import Helium.Main.PhaseTypeInferencer
import Helium.Main.PhaseDesugarer
import Helium.Main.PhaseCodeGenerator
import Helium.Main.CompileUtils
import Helium.StaticAnalysis.Messages.Messages
import Helium.StaticAnalysis.Messages.TypeErrors
import Helium.StaticAnalysis.Messages.TErrors
import Helium.StaticAnalysis.Messages.Warnings(Warning)
import Helium.StaticAnalysis.Inferencers.TypeInferencing(typeInferencing)
import Helium.ModuleSystem.DictionaryEnvironment (DictionaryEnvironment)
--import UHA_Syntax
import Helium.StaticAnalysis.Messages.Information (showInformation)
-- import System.FilePath.Posix
import Helium.Utils.Utils
import qualified Control.Exception as CE (catch, IOException)
import Data.IORef
import Helium.StaticAnalysis.Messages.StaticErrors(errorsLogCode)
import Helium.Parser.Parser(parseOnlyImports)
import Control.Monad
import System.FilePath(joinPath)
import Data.List(intersect,lookup,foldl', nub, elemIndex, isSuffixOf, intercalate,(\\),sortBy, findIndex,minimumBy, find)
import Data.Maybe
import Data.Map(lookup,member,(!))
import Lvm.Path(explodePath,getLvmPath)
import System.Directory(doesFileExist, getModificationTime)
import Helium.Main.Args
import Paths_helium
import Top.Types
import Top.Types.Schemes
import Helium.StaticAnalysis.Miscellaneous.DoublyLinkedTree
import Helium.StaticAnalysis.Miscellaneous.UHA_Source
import Helium.Syntax.UHA_Syntax
import Helium.Parser.OperatorTable(Assoc(AssocLeft,AssocRight,AssocNone))
import Helium.Utils.OneLiner
import FEState
import TypeDiffNewNew
-- import TypeDiffNew
import ExpRepHelper
import Data.List.Split(splitOn)
import Data.Function
import Data.List (lookup)
import LocalPref
import System.Process
import Data.Char (isSpace)
    
rstrip = reverse . dropWhile isSpace . reverse

-- 
-- Note the following list is used for change generations.
--
errorMessagesChgGen = [("Type error in application\n", 1), ("Type error in constructor\n", 2), ("Type error in infix application\n", 3), 
  ("Type error in literal\n", 4), ("Type error in right-hand side\n", 5), 
  ("Type error in variable\n", 6), ("Type error in enumeration\n", 6), 
  ("Type error in guard\n", 7), ("Type error in guarded expression\n", 8), ("Type signature is too general\n", 9),
  ("Don't know which instance to choose for Eq\n", 10), ("Don't know which instance to choose for Num\n", 10),
  ("Type error in negation\n", 11), ("Type error in overloaded function\n", 11),
  ("Type error in explicitly typed binding\n", 12), ("Type error in infix pattern application\n", 13), ("Type error in pattern of function binding\n", 14),("Type error in element of list\n",15)] 

  
getFeature1 errInfo =
  let fea1 = head . handleTErr . fst $ errInfo
  in Data.List.lookup (show fea1) errorMessagesChgGen

{-
"change suggestion"
(1: type; 2: expression; 3: reason)	
-}  
getFeature2 (tError,_) = 
  let causePair = hints tError
      cause = map snd causePair
      f2 = if null cause then Nothing else Just (head cause)
  in
    -- error $ show f2
    feature2Val f2

getFeature2Unparsed (tError,_) = 
  let causePair = hints tError
      cause = map snd causePair
      f2 = if null cause then Nothing else Just (head cause)
  in fmap show f2
    -- feature2Val f2
    
getFeature2New (tError,_) = 
  let causePair = hints tError
      cause = map snd causePair
      f2 = if null cause then Nothing else Just (head cause)
  in
    f2
feature2Val f2 =
    case f2 of
        (Just y) ->
            case Data.List.lookup (head (splitOn " " (show y))) suggests of
                  Just ind -> ind
                  _ -> 1
        _ -> 1

  
getFeatures3And4 errInfo =  
  let expTypeActType = (head . getTypeErrors) [fst errInfo]
  in ( (fromJust . typesOutOfBlock . fst ) expTypeActType, 
       (fromJust . typesOutOfBlock . snd ) expTypeActType)

getFeatures3And4New errInfo =  
  let expTypeActType = (head . getTypeErrors) [fst errInfo]
  in ( ( fst ) expTypeActType, 
       ( snd ) expTypeActType)

getFeatures5To9 errInfo combinedEnv =
  uncurry (tpSchemeDiff combinedEnv) . getFeatures3And4 $ errInfo

getFeatures5To9UseLikely errInfo combinedEnv 
    | null lkFuncs = TypeDiff brackets funcDiff topDiff lkFuncs topBra
    | (not . null) funcDiff = TypeDiff brackets funcDiff topDiff lkFuncs topBra
    -- To extract likelyFunc for top level func diff, uncommet the following
    -- line
    -- | otherwise = TypeDiff brackets newFuncDiff newTopDiff lkFuncs topBra
    | otherwise = TypeDiff brackets newFuncDiff topDiff lkFuncs topBra
    where tpss = getFeatures3And4 errInfo
          (act,exp) = tpScToTp combinedEnv tpss
          TypeDiff brackets funcDiff topDiff lkFuncs topBra= getFeatures5To9 errInfo combinedEnv
          (lastIdx, lastDiff) = last lkFuncs
          shortLen = min ((length . spines) exp) ((length . spines) act)
          newTopDiff = if lastIdx == shortLen && topDiff ==0 
                       then lastDiff
                       else topDiff
          newLkFuncs = if lastIdx == shortLen
                       then init lkFuncs
                       else lkFuncs
          newFuncDiff = if null funcDiff then newLkFuncs else funcDiff

        
actualSrcAndSort errInfo =
  let cinf = map snd errInfo
      --get the oldLocalInfo
      oldLocalInfo = map localInfo cinf
      --look for if there's extra source corresponding to the real error
      possibleErrorSources = map otherSource cinf
      --get the corresponding function bindings for these sources, or the root
      actErrorTrees = map (getFB . getTreeForSrc) cinf
      zErrTrees = zip possibleErrorSources actErrorTrees
      --get the actual error trees for the new source
      newCinfo = zipWith inOrderSearch possibleErrorSources actErrorTrees
      -- put the original offending error tree in the constraint info
      cinff = map (\x -> x {offenderTree = Just (getTreeForSrc x)}) cinf
      newTrees = zipWith correctTree newCinfo (map offenderTree cinff) --oldLocalInfo
      -- put the correct info tree for the error in cinfo
      cinfo = zipWith (\x -> \(z,y) -> x {offenderTree = y, usesOtherSource = z}) cinff newTrees

      newErrInfo = map (\(x,y) -> (fst x, y)) (zip errInfo cinfo)
      
      func = rangeOfSource . self . attribute . offendingInfoTree . snd    
  in sortBy (\x y -> func x `compare` func y) newErrInfo

{-
Feature11: type error related code excerpt
(1: expression; 2: lib function; 3: user defined function; 4: operator; 5: variable; 6: argument; 7: constant, 8: RHS)

Feature17: "function/operator"
(1: lib func; 2: user-defined func; 3: operator; -1: if not)"
-}  
feature11And17ForIT combinedEnv localEnv importEnv actInfoTree = 
  let (f11A,f17A) = feature11And17ForITAlg combinedEnv localEnv importEnv actInfoTree
  in (feature11Int f11A, feature17Int f17A)
  
feature11And17ForITAlg combinedEnv localEnv importEnv actInfoTree = 
  let varsForTrees = inOrder actInfoTree
      firstVar = (fmap UHA_Expr . safeHead) varsForTrees  

      infoTree = (self . attribute) actInfoTree
      offType = nodeType actInfoTree

      offenderTypeShort = offenderType combinedEnv localEnv importEnv
      sourcesNodes = (infoTree, offType)
      feature11 = (offenderTypeShort) sourcesNodes

      newFeature17 = case firstVar of
        Just z -> (offenderTypeShort) (z,Leaf)
        _ -> OffError
        
  in (feature11, newFeature17)

{-Returns an algebraic value-}
getFeature11And17Alg combinedEnv localEnv importEnv errInfoS = 
  let cinfo = snd errInfoS
      actInfoTree = offendingInfoTree cinfo
  in feature11And17ForITAlg combinedEnv localEnv importEnv actInfoTree
  
-- Returns an Int value  
getFeature11And17 combinedEnv localEnv importEnv errInfoS = 
  let cinfo = snd errInfoS
      actInfoTree = offendingInfoTree cinfo
  in feature11And17ForIT combinedEnv localEnv importEnv actInfoTree

{-
Type inconsistency in AST
1: top, 2: node, 3: leaf
-}  
getFeature10 errInfoS = 
  let cinfo = snd errInfoS
      feature10 = if (self . attribute . localInfo) cinfo 
                      == 
                     (self . attribute . offendingInfoTree) cinfo
                  then
                    feature10Int Root
                  else
                    feature10Int (offenderLocation cinfo)
  in feature10
     
getFeature12 combinedEnv feature11 =
    (\x ->
             case x of
               (Oper (UHA_Expr (Expression_Variable _ name))) ->
                   Just (snd $ (!) (operatorTable combinedEnv) name)
               (Oper (UHA_Expr (Expression_Constructor _ name))) ->
                   Just (snd $ (!) (operatorTable combinedEnv) name)
               _ -> Nothing) feature11
    
gf12 combinedEnv feature11 = map
       (\x ->
             case x of
               (Oper (UHA_Expr (Expression_Variable _ name))) ->
                   Just (snd $ (!) (operatorTable combinedEnv) name)
               (Oper (UHA_Expr (Expression_Constructor _ name))) ->
                   Just (snd $ (!) (operatorTable combinedEnv) name)
               _ -> Nothing) feature11
{-
feature16Int AList = 3
feature16Int ACons = 2
feature16Int AConstructor = 5
feature16Int Mixed = 6
feature16Int PointFree = 4
feature16Int Single = 1
feature16Int ParamError = -1
-}
getFeature16 errInfoS =
  let cinfo = snd errInfoS
      functionBinding = (getFB . offendingInfoTree) cinfo
      funct = (head . feature16Func) [functionBinding]
  in fmap (feature16Int . snd) funct

gf14 subTree = (totalParentheses (extractFunctionArguments subTree))

getFeature14 cinfo =
    let subTree = (fst . sources) cinfo
    in
      (totalParentheses (extractFunctionArguments subTree))
                  
varNamesFor11 =
    (\x ->
      
         case x of
           (Just a) -> a
           _ -> Nothing
    ) . (fmap getVarName) . fea11Body

varBodiesFor11 =
    (\x ->
      
         case x of
           (Just a) -> a
           _ -> Nothing
    ) . (fmap getVarBody) . fea11Body

getFeature18Val cinfo feat11 =
    let vars = varNamesFor11 feat11 
        fbForExp = (getFB . fromJust . offenderTree) cinfo
        expVars = inOrder fbForExp
        zippedOffVars =  case vars of
                           (Just a) ->
                               Just (length (filter ((a ==) . varName) expVars))
                           _ -> Nothing 
    in
      zippedOffVars

getFeature18 :: ConstraintInfo -> OffenderType -> Maybe Int
getFeature18 cinf offType = (fmap (\x -> if x > 1 then 2 else 1)) (getFeature18Val cinf offType)

getFeature19 cinfo feat11 feat18 =
    let expressionVar = varBodiesFor11 feat11
        fbForExp = (getFB . fromJust . offenderTree) cinfo
        f19 = determineIndex expressionVar fbForExp
    in
      case (f19,feat18) of
        (Just 1, Just z) -> 1
        (Just b, Just c) ->
            if b == c
            then 3
            else 2
        _ -> -1

getFeature20 cinfo combinedEnv fea11 fea12 =
    let expressionVar = varBodiesFor11 fea11
        opTable = operatorTable combinedEnv
        fbFor20 = (getFB . localInfo) cinfo
        variablesForFB = inOrder fbFor20
        operAssocs = (fea11, fea12)
        isOperators = isOper fea11
        operatorErrors =  (operAssocs, variablesForFB)
        varsToConsider = (\(a,b) ->  filter ((Nothing /=) . (operWithAssoc opTable (snd a))) b) operatorErrors
        zippedLeafOpers = (expressionVar, ( (zip [1..]) varsToConsider))
        getIndexes = 
            (case (fst zippedLeafOpers) of
              Just z -> Just ( (map fst) $ (filter ((z ==) . snd)) (snd zippedLeafOpers))
              _ -> Nothing)
        indexes = (case (fmap safeHead getIndexes) of
                     (Just (Just a)) -> a
                     _ -> -1)
        newVars = zip [1..] varsToConsider
        errWithVars = (indexes, newVars)        
        varLists =  (\(z,xs) -> filter ((z <) . fst) xs) errWithVars
        feats20 = length varLists 
        featu20 = Just feats20
        feature20 = (\x -> \y -> if y then x else Nothing) featu20 isOperators
        newFeature20 = case feature20 of
                                (Just 0) -> 0
                                (Just a) -> 1
                                _ -> -1            
    in
      newFeature20

gf21 combinedEnv feature11 =
    map (\x ->
             case x of
               (Oper (UHA_Expr (Expression_Variable _ name))) ->
                   Just (fst $ (!) (operatorTable combinedEnv) name)
               (Oper (UHA_Expr (Expression_Constructor _ name))) ->
                   Just (fst $ (!) (operatorTable combinedEnv) name)
               _ -> Nothing) feature11

getFeature21 combinedEnv feature11 =
    case feature11 of
      (Oper (UHA_Expr (Expression_Variable _ name))) ->
                   Just (fst $ (!) (operatorTable combinedEnv) name)
      (Oper (UHA_Expr (Expression_Constructor _ name))) ->
                   Just (fst $ (!) (operatorTable combinedEnv) name)
      _ -> Nothing
                    
getFeature22 cinfo =
    let actInfoTree = offendingInfoTree cinfo
    in
      ((\x -> if (length x) > 1 then 1 else 0) . children) actInfoTree

    
pairedError combinedEnv lft rgt = 
    let features1 = getFeatures5To9 lft combinedEnv
        features2 = getFeatures5To9 rgt combinedEnv
    in isComp features1 features2 && closeEnough lft rgt

isDollar (Expression_Variable _ (Name_Operator (_) (_) ("$"))) = True
isDollar (Expression_Variable _ (Name_Identifier (_) (_) ("$"))) = True
isDollar (Expression_Variable _ (Name_Special (_) (_) ("$"))) = True
isDollar _ = False

getFeature24 cinfo = (binaryLength . length . inOrderBracket . localInfo) cinfo
        
getFeature25 cinfo =
    let actInfoTree = offendingInfoTree cinfo
        varsForTrees = inOrder actInfoTree
        varsForFB = (inOrder . getFB) actInfoTree
        feature25 =  (binaryLength . length . (filter isDollar)) varsForFB
    in
      feature25

                 
getFeatures perrInfo topLevelTypes subs combinedEnv localEnv importEnv pp fname = do
  let errInfo = actualSrcAndSort perrInfo
  let errPair = pairs errInfo
  let errPairs = if null errPair then [] else (errPair ++ [(last errPair)])
  let actErrAndPairs = zip errInfo errPairs
  let feat23 = map
                  (\(actErr, (fstErr,sndErr)) ->
                       if pairedError combinedEnv fstErr sndErr
                       then
                          if (not $ null errInfo) && (((sources . snd) actErr) == ((sources . snd) sndErr))
                          then
                              2
                          else
                              1
                       else
                           0) actErrAndPairs
  let feature23 = if (null feat23) then feat23 ++ [0] else feat23 --the list only has n-1 consecutive pairs
  
  let tErrors = map fst errInfo
  let cinfo = map snd errInfo
  
  -- let offender = offendingInfoTree . head $ cinfo
  -- let fstChd = attribute . head . children $ offender
  -- let Just typ = assignedType fstChd
  
  -- putStrLn . show $ typ

  
  
  -- putStrLn . show $ lookupInt 128 subs
  -- putStrLn . show $ subs |-> typ
  
  let feature22 = map ((\x -> if (length x) > 1 then 2 else 1)) (map (inOrderConst . localInfo) cinfo)
  let fea1 = concat $ map handleTErr tErrors
  let feature1 = map show fea1

  -- putStrLn "Before closeEnough"
  -- (putStrLn . show) (pairedError combinedEnv (errInfo !! 1) (errInfo !! 2)) 
  --putStrLn "Testing feature 1"
  --mapM (putStrLn . show . getFeature1) errInfo
  
  --start feature 2
  let causePair = map hints tErrors
  let cause = map (map snd) causePair
  let feature2 = map  (\x -> if null x then Nothing else Just (head x)) cause
  --mapM (putStrLn . show) feature2                 
  --cinf has the original constraint info

  let actInfoTree = map offendingInfoTree cinfo
  --putStrLn "Actual Info Tree"
  --mapM (putStrLn . show . self . attribute) actInfoTree
  --putStrLn ""
  let varsForTrees = map inOrder actInfoTree
  let varsForFB = map (inOrder . getFB) actInfoTree
  let feature25 =  (map (binaryLength . length . (filter isDollar)) varsForFB)
  --putStrLn "Variables for FB"
  --mapM (putStrLn . show . self . attribute) varsForFB
  let firstVar = map ((fmap UHA_Expr) . safeHead) varsForTrees
  let infoTree = map (self . attribute . fromJust . offenderTree) cinfo
  --putStrLn "Offender Tree"
  --mapM (putStrLn . show) infoTree
  let locInfo = map localInfo cinfo
  
  --start feature10
  let origExps = map localInfo cinfo                 
  
  let offType = map offenderLocation cinfo
  let origsAndOffenders = zip origExps cinfo
  let feature10 = map (\(x,y) ->
                           if (self . attribute) x == (self . attribute . fromJust . offenderTree) y
                           then
                               feature10Int Root
                           else
                               (feature10Int (offenderLocation y))) origsAndOffenders
  --let feature10 = map feature10Int offType

  --start feature11
  let sourcesNodes = zip infoTree offType
  let fea1111 = map (offenderType combinedEnv localEnv importEnv) sourcesNodes
  -- mapM (putStrLn . show) fea111
  let fea111 = zip fea1111 firstVar
  let oneIdCheck = zip fea111 feature22
  let replaceIfOneId = map (\((x,y),z) ->
                                case ((x, y),z) of
                         ((Exp exp, Just nom), 1) ->
                             --(offenderType combinedEnv localEnv importEnv ((getFirstVar exp), Leaf))
                              (offenderType combinedEnv localEnv importEnv (nom, Leaf))
                         ((RHS rhs, Just nom), 1) ->
                             --(offenderType combinedEnv localEnv importEnv ((getFirstVar exp), Leaf))
                              (offenderType combinedEnv localEnv importEnv (nom, Leaf))
                         ((Exp exp, _), 1) -> Constant exp --assume constant
                         ((RHS rhs, _), 1) -> Constant rhs --assume constant
                         _ -> x) oneIdCheck
  let feature11 = map feature11Int replaceIfOneId
  --let feature11 = map (feature11Int . fst) fea111
  let fea11 = {-fea111-} map (\(x,y) ->
                       case (x, y) of
                         (Exp exp, Just nom) ->
                             --(offenderType combinedEnv localEnv importEnv ((getFirstVar exp), Leaf))
                              (offenderType combinedEnv localEnv importEnv (nom, Leaf))
                         (RHS rhs, Just nom) ->
                             --(offenderType combinedEnv localEnv importEnv ((getFirstVar exp), Leaf))
                              (offenderType combinedEnv localEnv importEnv (nom, Leaf))
                         _ -> x ) fea111

  -- putStrLn "\nFea11 values"
  -- mapM (putStrLn . show) feature11
  --start feature 12
  let fea12 = (gf12 combinedEnv) fea11
  let feature12 = map (fmap feature12Int) fea12
  --start special root check
  let rootOper = zip feature10 feature11
  let specialCase = elem True (map (\x -> x == (1, 4)) rootOper)
  --start feature 14
  let subTree = map (fst . sources) cinfo
  let sourceAndClass = zip subTree fea11

  let feature24 = map (binaryLength . length . inOrderBracket . localInfo) cinfo
  --putStrLn "Feature 24"
  --mapM (putStrLn . show) feature24
  let feat14 = map gf14 subTree
  let feature14 = map binaryLength feat14
  --start feature 16
  let functionBinding = map getFB (map (fromJust . offenderTree) cinfo)
  --putStrLn "Function binding"
  --mapM (putStrLn . show . self . attribute) functionBinding
  let functs = feature16Func functionBinding
  let feature16 = map (fmap (feature16Int . snd)) functs
  --start feature 17
  let feature17 = map getFeature11Name fea11
  let newf17 = zip firstVar (replicate (length feature17) Leaf)
  let newFeature17 =
          map (\(x,y) ->
                   case x of
                     (Just z) ->
                         (feature17Int . (offenderType combinedEnv localEnv importEnv)) (z,y)
                     _ -> -1)
          newf17
          
  --putStrLn "\n infoTree"
  --mapM (putStrLn . show . self . attribute . localInfo) cinfo 
  let f1117A =  map (getFeature11And17Alg combinedEnv localEnv importEnv) errInfo
  
  -- mapM (putStrLn . show) f1117A
  
  --start feature 18
  let vars = map ((\x ->
                       case x of
                         (Just a) -> a
                         _ -> Nothing
                  ) . (fmap getVarName) . fea11Body)
             fea11 --infoTree
  --putStrLn "\nfeature 18 variables"
  --mapM (putStrLn . show) vars
  let fbForExp =  map (getFB . fromJust . offenderTree) cinfo
  let expVars = map inOrder fbForExp
  --putStrLn "\nExprVars"
  --mapM (putStrLn . show) expVars
  let zippedOffVars = zipWith (\x -> \y ->
                              case x of
                                (Just a) ->
                                     Just (length (filter ((a ==) . varName) y))
                                _ -> Nothing) vars expVars
  --mapM (putStrLn . show) expVars
  
  let rhs = map (getRightHandSide . self . attribute) functionBinding
  --mapM (putStrLn . show) rhs
  let featur18 = zippedOffVars
  let featu18 = zip zippedOffVars feature1
  let feature18 = map (\(a,b) -> if (Data.List.lookup b errorMessages) /= (Just 9)
                  then
                      (fmap (\x -> if x > 1 then 2 else 1) a)
                  else
                      (Just (-1)))
                           featu18 --zipWith searchRightHandSideHelper vars rhs  
  --start feature 19
  --mapM (putStrLn . show) infoTree
  let expressionVar = map ((\x ->
                       case x of
                         (Just a) -> a
                         _ -> Nothing
                  ) . (fmap getVarBody) . fea11Body)
             fea11 --infoTree  --infoTree
  --mapM (putStrLn . show) expressionVar
  --mapM (putStrLn . show . fbTest . self . attribute) fbForExp 
  --putStrLn ""
  --mapM (putStrLn . show) (map (getVariablesFB . getFb) fbForExp)
                 
  let feature19 = 
          zipWith determineIndex expressionVar fbForExp
  --putStrLn "Feature 19"
  --mapM (putStrLn . show) (zip feature19 feature18)
  let newFeature19 = map (\(x,y) ->
                              case (x,y) of
                                (Just 1, Just z) -> 1
                                (Just b, Just c) ->
                                    if b == c
                                    then 3
                                    else 2
                                _ -> -1) (zip feature19 featur18)
  --mapM (putStrLn . show) feature19
  --start feature 20
  let opTable = operatorTable combinedEnv
  let fbFor20 = map (getFB . localInfo) cinfo
  let variablesForFB = map inOrder fbFor20
  let operAssocs = zip fea11 fea12
  let isOperators = map isOper fea11
  --mapM (putStrLn . show) isOperators
  let operatorErrors =  (zip operAssocs variablesForFB)
  --mapM (putStrLn . show) operatorErrors
  let varsToConsider = map (\(a,b) ->  filter ((Nothing /=) . (operWithAssoc opTable (snd a))) b) operatorErrors
  let zippedLeafOpers = zip expressionVar (map (zip [1..]) varsToConsider)
  --mapM (putStrLn . show) zippedLeafOpers
  let getIndexes = map (\(x,y) ->  case x of
                                     Just z -> Just ( (map fst) $ (filter ((z ==) . snd)) y)
                                     _ -> Nothing
                       ) zippedLeafOpers
  --mapM (putStrLn . show) getIndexes
  let indexes = map (\x ->
                              case (fmap safeHead x) of
                                (Just (Just a)) -> a
                                _ -> -1
                         ) getIndexes 
  --mapM (putStrLn . show) indexes
  let newVars = map (zip [1..]) varsToConsider
  --mapM (putStrLn . show) newVars
  let errWithVars = (zip indexes) newVars
  --mapM (putStrLn . show) errWithVars
  let varLists = map  (\(z,xs) -> filter ((z <) . fst) xs) errWithVars
  --mapM (putStrLn . show) varLists
  let feats20 = map length varLists 
  let featu20 = if null feats20 then (replicate (length feature19) Nothing) else (map Just feats20)
  let feature20 = zipWith (\x -> \y -> if y then x else Nothing) featu20 isOperators
  let newFeature20 = map (\num ->
                              case num of
                                (Just 0) -> 0
                                (Just a) -> 1
                                _ -> -1) feature20
  let feature21 = gf21 combinedEnv fea11
  --mapM (putStrLn . show) feature20
  --start features 3 and 4
  let expTypeActType = getTypeErrors tErrors
  let features34 = expTypeActType
  let types = map (\(x,y) -> (fromJust (typesOutOfBlock x), fromJust (typesOutOfBlock y))) features34
  let feature3 = map fst features34  
  let feature4 = map snd features34
  -- mapM (putStrLn . (\(x,y) -> show x ++ "\n" ++ show y) ) expTypeActType
  
  let features5To9func = if mergeLikely 
                         then getFeatures5To9UseLikely 
                         else getFeatures5To9
  let features5to9 = map (flip features5To9func combinedEnv) errInfo
  -- let features5to9New = map (\x -> tpSchemeDiffNew combinedEnv (fst x) (snd x)) types
 
  -- putStrLn "\nNew type diff"
  -- mapM (putStrLn . show) features5to9

  -- putStrLn "\nOld type diff"
  -- mapM (putStrLn . show) features5to9New
   
  -- if any (not . uncurry eqDefinite) (zip features5to9 features5to9New) then
    -- putStrLn "Unequal*****"
  -- else putStrLn "-----------"
  
  -- if any (\ (x,y) -> brackets x /= brackets y) (zip features5to9 features5to9New) then
    -- putStrLn "Bracketsdiff*****"
  -- else putStrLn "-----------"
  
  -- mapM (putStrLn . subTreeStr 0 . fromJust . parent . fromJust . parent . offendingInfoTree) cinfo   
  -- no feature 15 for now, features 5to8 handle arrow size
  --putStrLn "parsed Messages"
  -- feat26 <- mapM (\x ->
             -- readProcess "sh" ["msg_type.sh", fname, (show x)] [])
            -- [1..(length errInfo)]
  -- let feature26 = map rstrip feat26
    
  -- if debug then 
    -- mapM_ (putStrLn . concatMap (\(n,i,z) -> n ++ "\t" ++ show i ++ "\n" ) . 
           -- predecessors . offendingInfoTree ) cinfo
    -- >> mapM_ (putStrLn . errLoc . offendingInfoTree) cinfo
  -- else 
    -- putStr ""
    
  -- if debug then
    -- putStrLn . show $ combinedEnv
  -- else
    -- putStr ""
    
  -- if debug then
    -- mapM_ (putStrLn .(\tree -> subTreeStr 0 tree ++ show (fmap (subs |-> ) (assignedType . attribute $ tree )) ++ "\n") . offendingInfoTree . snd ) errInfo
    -- >>
    -- mapM_ (putStrLn . show . getFeature1) errInfo
  -- else 
    -- putStr ""
    
  -- if debug then
    -- mapM_ (putStrLn . (\info -> subTreeStr 0 info ++ "\n" ++ (show . fmap showITJust . topNode) info) . offendingInfoTree ) cinfo
  -- else 
    -- putStr ""
    
  -- if debug then
    -- putStrLn "Grand parents" >>
    -- mapM_ (putStrLn . (\rt -> show rt ++ "\t Can rotate? " ++ show (join $ fmap (canRotateRR subs combinedEnv) rt) ++ "\n") . getTreesRR . offendingInfoTree ) cinfo 
    -- >>
    -- mapM_ (putStrLn . (\x -> "Can regroup? " ++ show x) . canRegroupRR subs combinedEnv . offendingInfoTree ) cinfo 
  -- else 
    -- putStr ""
    
    
  -- Feature 26 is dropped
  let feature26 = replicate (length errInfo) " "
  
  let featList =  makeFeats  feature1 feature2  feature3  feature4
           features5to9  feature10  feature11  feature12
           feature14  feature16  newFeature17
           feature18  newFeature19  newFeature20 feature21 feature22
           feature23 feature24 feature25 feature26
 
  -- The features are printed out in HeliumTest.hs
  -- if pp
      -- then
          -- mapM prettyPrintFeats featList
      -- else
          -- mapM (putStrLn . \(x,y) -> (show . self. attribute . fromJust . offenderTree) x ++ "\n" ++ show y) (zip cinfo featList)
        
  return $ zipWith3 (\errinf cons feat -> ((fst errinf,cons),feat)) errInfo cinfo featList
  
{-
Begin utility code for main, handleTErr and the functions it calls are unused
but I don't want to delete them, just in case they come in handy for something
else. I'll clean them up if they don't get used again.
===============================================================================
-}
inOrderSearch err node =
    case err of
      Just e ->
          if e == (self . attribute) node
          then
              Just node
          else
              let search = (mapMaybe (inOrderSearch err) (children node))
              in
                if null search
                then
                    Nothing
                else
                    Just (head search) 
      _ -> Nothing
           
correctTree (Just new) old = (True,Just new)
correctTree Nothing old =  (False,old)

data F25 = HasDollar | NoDollar deriving(Eq)
instance Show F25 where
    show HasDollar = "1"
    show NoDollar = "0"
                           
type Feature1 = String
type Feature2 = Maybe MessageBlock
type Feature3 = MessageBlock
type Feature4 = MessageBlock
type Features5To8 = TypeDiff
type Feature10 = Int --NodeType
type Feature11 = Int --OffenderType
type Feature12 =  (Maybe Int) --Asoc
type Feature14 = Int
type Feature15 = Int
type Feature16 = Maybe Int --ParamStyle
type Feature17 =  Int
type Feature18 = Maybe Int
type Feature19 = Int
type Feature20 = Int
type Feature21 = Maybe Int
type Feature22 = Int
type Feature23 = Int
type Feature24 = Int
type Feature25 = Int
type Feature26 = String
data TheFeatures = TheFeatures
    {feat1 :: Feature1,
     feat2 :: Feature2,
     feat3 :: Feature3,
     feat4 :: Feature4,
     feats5To8 :: Features5To8,
     feat10 :: Feature10,
     feat11 :: Feature11,
     feat12 :: Feature12,
     feat14 :: Feature14,
     --feat15 :: Feature15,
     feat16 :: Feature16,
     feat17 :: Feature17,
     feat18 :: Feature18,
     feat19 :: Feature19,
     feat20 :: Feature20,
     feat21 :: Feature21,
     feat22 :: Feature22,
     feat23 :: Feature23,
     feat24 :: Feature24,
     feat25 :: Feature25,
     feat26 :: Feature26}

makeFeats feat1 
     feat2 
     feat3 
     feat4 
     feats5To8 
     feat10 
     feat11 
     feat12 
     feat14 
     --feat15 :: Feature15,
     feat16 
     feat17 
     feat18 
     feat19 
     feat20
     feat21
     feat22
     feat23
     feat24
     feat25
     feat26
    =
      if null feat1
         then
             []
         else
             (TheFeatures f1 f2 f3 f4 f5 f10 f11 f12 f14 f16 f17 f18 f19 f20 f21 f22 f23 f24 f25 f26) :
             (makeFeats
                 (tail feat1)
                 (tail feat2)
                 (tail feat3)
                 (tail feat4)
                 (tail feats5To8)
                 (tail feat10)
                 (tail feat11)
                 (tail feat12)
                 (tail feat14)
                 (tail feat16)
                 (tail feat17)
                 (tail feat18)
                 (tail feat19)
                 (tail feat20)
                 (tail feat21)
                 (tail feat22)
                 (tail feat23)
                 (tail feat24)
                 (tail feat25)
                 (tail feat26)
             )
      where
        f1 = (head feat1)
        f2 = (head feat2)
        f3 = (head feat3)
        f4 = (head feat4)
        f5 = (head feats5To8)
        f10 = (head feat10)
        f11 = (head feat11)
        f12 = (head feat12)
        f14 = (head feat14)
        f16 = (head feat16)
        f17 = (head feat17)
        f18 = (head feat18)
        f19 = (head feat19)
        f20 = (head feat20)
        f21 = (head feat21)
        f22 = (head feat22)
        f23 = (head feat23)
        f24 = (head feat24)
        f25 = (head feat25)
        f26 = (head feat26)

-- errorMessages :: [(String,Int)]
--errorMessages = [("Type error in right-hand side\n", 1), ("Type error in infix application\n", 2), ("Type error in application\n", 3), ("Type error in literal\n", 4), ("Type error in variable\n", 5), ("Type error in constructor\n", 6), ("Type error in guarded expression\n", 7), ("Type error in overloaded function\n", 8), ("Type signature is too general\n", 9), ("Type error in pattern of function binding\n", 10),("Type error in literal\n",11),("Type error in explicitly typed binding\n",12),("Don't know which instance to choose for Num\n",13),("Type error in element of list\n",14)]

errorMessages = [("Type error in application\n", 1), ("Type error in constructor\n", 2), ("Type error in infix application\n", 3), 
  ("Type error in literal\n", 4), ("Type error in right-hand side\n", 5), 
  ("Type error in variable\n", 6), ("Type error in element of list\n", 6), ("Type error in enumeration\n", 6), 
  ("Type error in guard\n", 7), ("Type error in guarded expression\n", 8), ("Type signature is too general\n", 9),
  ("Don't know which instance to choose for Eq\n", 10), ("Don't know which instance to choose for Num\n", 10),
  ("Type error in negation\n", 11), ("Type error in overloaded function\n", 11),
  ("Type error in explicitly typed binding\n", 12), ("Type error in infix pattern application\n", 12), ("Type error in pattern of function binding\n", 12)] 

suggests = [("not",3),("too",3),("use",2),("insert",2),("remove",2),("valid",3),("re-order",2),("swap",2),("instead",3),("it",3),("the",3),("unification",3),("write",3)]

typeValueExtract :: [(Int, Int)] -> Int
typeValueExtract [] = 0
typeValueExtract x = snd (head x)

printFeats :: TheFeatures -> String
printFeats x =
    "feature values:"
    ++
    (case ((Data.List.lookup f1 errorMessages)) of
               (Just x) -> show x
               _ -> "-1")
    ++ ","
    ++ (case f2 of
               (Just y) ->
                   (case (Data.List.lookup (head (splitOn " " (show y))) suggests) of
                      (Just ind) -> (show ind)
                      _ -> "1")
               _ -> "1")
    ++ ","
--    ++ "\nBrac diff: "
--    ++ show (brackets f5)
--    ++ ","
    ++ show (typeValueExtract (brackets f5))
    ++ ","
    ++ show (length (brackets f5))
--    ++ "\nfuncs:"
--    ++ show (funcs f5)
    ++ ","
    ++ show (typeValueExtract (funcs f5))
    ++ ","
    ++ show (length (funcs f5))
--    ++ "\ntopFunc:"
    ++ ","
    ++ show (topLevelDiff f5)
--    ++"\ntopBrac:"
    ++ ","
    ++ show (topLevBra f5)
    ++ ","
    ++ (show f10)
    ++ ","
    ++ (show f11)
    ++ ","
    ++ (case f12 of
          (Just a) -> show a
          _ -> (show (-1)))
    ++ ","
    ++ show f14
    ++ ","
    ++ (case f16 of
          (Just a) -> (show . (\x -> if x == 4 then 1 else 0))  a
          _ -> (show 0))
    ++ ","
    ++ show f17
    ++ ","
    ++ (case f18 of
          (Just a) -> show a
          _ -> (show (-1)))
    ++ ","
    ++ (show f19)
    ++ ","
    ++ (show f20)
    ++ ","  
    ++ (case f21 of
          (Just a) -> show a
          _ -> (show (-1)))
    ++ ","
    ++ (show f22)
    ++ ","
    ++ (show f23)
    ++ ","
    ++ (show f24)
    ++ ","
    ++ (show f25)
--    ++ ","
--    ++ f26
    ++ "\n"
    where
      f1 = (feat1 x)
      f2 = (feat2 x)
      f3 = (feat3 x)
      f4 = (feat4 x)
      f5 = (feats5To8 x)
      f10 = (feat10 x)
      f11 = (feat11 x)
      f12 = (feat12 x)
      f14 = (feat14 x)
      f16 = (feat16 x)    
      f17 = (feat17 x)
      f18 = (feat18 x)
      f19 = (feat19 x)
      f20 = (feat20 x)
      f21 = (feat21 x)
      f22 = (feat22 x)
      f23 = (feat23 x)
      f24 = (feat24 x)
      f25 = (feat25 x)
      f26 = (feat26 x)

conciseText :: TheFeatures -> String
conciseText x = 
    f1
    ++ show f2
    ++ "\n"
    ++ (show f3)
    ++ "\n"
    ++ (show f4)
    ++ "\nType diff: "
    ++ show (brackets f5)
    ++ ", (funcs): "
    ++ show (funcs f5)
    ++ ", "
    ++ show (topLevelDiff f5)
    ++ ", (likelyFuncs): "
    ++ show (likelyFuncs f5)
    ++", (topBrac): "
    ++ show (topLevBra f5)
    ++ "\n(f10): "
    ++ (show f10)
    ++ ", "
    ++ (show f11)
    ++ ", "
    ++ (case f12 of
          (Just a) -> show a
          _ -> (show (-1)))
    ++ ", (f14): "
    ++ show f14
    ++ ", (f16): "
    ++ (case f16 of
          (Just a) -> (show a)
          _ -> (show 0))
    ++ ", "
    ++ show f17
    ++ ", "
    ++ (case f18 of
          (Just a) -> show a
          _ -> (show (-1)))
    ++ ", (f19): "
    ++ (show f19)
    ++ ", "
    ++ (show f20)
    ++ ", "  
    ++ (case f21 of
          (Just a) -> show a
          _ -> (show (-1)))
    ++ ", (f22): "
    ++ (show f22)
    ++ ", "
    ++ (show f23)
    ++ ", "
    ++ (show f24)
    ++ ", "
    ++ (show f25)
    ++ ", "
    ++ (f26)
    ++ "\n"
    where
      f1 = (feat1 x)
      f2 = (feat2 x)
      f3 = (feat3 x)
      f4 = (feat4 x)
      f5 = (feats5To8 x)
      f10 = (feat10 x)
      f11 = (feat11 x)
      f12 = (feat12 x)
      f14 = (feat14 x)
      f16 = (feat16 x)    
      f17 = (feat17 x)
      f18 = (feat18 x)
      f19 = (feat19 x)
      f20 = (feat20 x)
      f21 = (feat21 x)
      f22 = (feat22 x)
      f23 = (feat23 x)
      f24 = (feat24 x)
      f25 = (feat25 x)
      f26 = (feat26 x)


prettyPrintFeats x = do    
  putStrLn ("Error message (f1):\t\t\t\t\t"
            ++
            (case ((Data.List.lookup f1 errorMessages)) of
               (Just x) -> show x
               _ -> "-1"))
  putStrLn ("Suggestion (f2):\t\t\t\t\t"
            ++            
            (case f2 of
               (Just y) ->
                   (case (Data.List.lookup (head (splitOn " " (show y))) suggests) of
                      (Just ind) -> (show ind)
                      _ -> "1")
               _ -> "1"))
  putStrLn ("Type diff:\t\t\t\t\t\t"
    ++ (show (brackets f5))
    ++ ","
    ++ (show (funcs f5))
    ++ ","
    ++ (show (topLevelDiff f5))
    ++ ","
    ++ (show (topLevBra f5)))
  putStrLn ("type inconsistency in AST(f10):\t\t\t\t"  ++ (show f10))
  putStrLn ("type error related code excerpt(f11):\t\t\t" ++ (show f11))
  putStrLn ("associativity(f12):...................................." 
    ++ (case f12 of
          (Just a) -> show a
          _ -> (show (-1))))
  putStrLn ("number of pairs of parentheses (f14):\t\t\t"
    ++ show f14)
  putStrLn ("function parameter style (f16):\t\t\t\t" 
    ++ (case f16 of
          (Just a) -> show a
          _ -> (show (-1))))
  putStrLn ("function/operator (f17):\t\t\t\t"
    ++ (show f17))
  putStrLn ("function/operator occurrence frequency (f18):\t\t"
    ++ (case f18 of
          (Just a) -> show a
          _ -> (show (-1))))              
  putStrLn ("function/operator appearance index (f19):\t\t"
    ++ show f19)
  putStrLn ("number of operator on the same associative side (f20):\t"
    ++ show f20)  
  putStrLn ("operator precedence (f21):\t\t\t\t"
    ++ (case f21 of
          (Just a) -> show a
          _ -> (show (-1))))
  putStrLn ("Number of identifiers in ill-typed expression (f22):\t"
    ++ show f22)
  putStrLn ("Errors are paired together:\t\t\t\t"
    ++ show f23)
  putStrLn ("Number of brackets in error expression (f24):\t\t"
    ++ show f24)
  putStrLn ("Function binding for error has $ (f25):\t\t"
    ++ show f25)
  putStrLn ("Parsed error message value (f26):\t\t\t"
    ++ f26)
  putStrLn ""
      where
        f1 = (feat1 x)
        f2 = (feat2 x)
        f3 = (feat3 x)
        f4 = (feat4 x)
        f5 = (feats5To8 x)
        f10 = (feat10 x)
        f11 = (feat11 x)
        f12 = (feat12 x)
        f14 = (feat14 x)
        f16 = (feat16 x)    
        f17 = (feat17 x)
        f18 = (feat18 x)
        f19 = (feat19 x)
        f20 = (feat20 x)
        f21 = (feat21 x)
        f22 = (feat22 x)
        f23 = (feat23 x)
        f24 = (feat24 x)
        f25 = (feat25 x)
        f26 = (feat26 x)

instance Show TheFeatures where
    show = printFeats        

           
feature10Int Root = 1
feature10Int Node = 2
feature10Int Leaf = 3

feature11Int (LibFunc _) = 2
feature11Int (Exp _) = 1
feature11Int   (UserFunc _) = 3
feature11Int (Oper _) = 4
feature11Int (Constant _) = 6
feature11Int (Argument _) = 5
-- feature11Int (RHS _) = 8
feature11Int (RHS _) = 1
feature11Int _ = -1

feature17Int (LibFunc _) = 1
feature17Int (UserFunc _) = 2
feature17Int (Oper _) = 3
feature17Int _ = -1

feature12Int AssocRight = 2
feature12Int AssocLeft = 1
feature12Int _ = 3

feature16Int AList = 3
feature16Int ACons = 2
feature16Int AConstructor = 5
feature16Int Mixed = 6
feature16Int PointFree = 4
feature16Int Single = 1
feature16Int ParamError = -1

-- probably need more pattern matches, won't handle anonymous functions
-- or applications of applications or parenthesized expressions
getFirstVar (UHA_Expr (Expression_NormalApplication rng e1 e2)) =
    getFirstVar (UHA_Expr e1)
getFirstVar (UHA_Expr (Expression_Variable rng name)) =
    (UHA_Expr (Expression_Variable rng name))
getFirstVar (UHA_Expr (Expression_InfixApplication _
                                mexp1
                                op
                                mexp2)) = (UHA_Expr op)
getFirstVar (UHA_Expr (Expression_Parenthesized rng exp)) = getFirstVar
                                                            (UHA_Expr exp)
getFirstVar (UHA_RHS (RightHandSide_Expression _ rhs _)) = getFirstVar (UHA_Expr rhs)
getFirstVar (UHA_FB (FunctionBinding_FunctionBinding _ _ rhs)) = getFirstVar (UHA_RHS rhs)
getFirstVar (UHA_Pat  (Pattern_Variable rng name)) = (UHA_Expr (Expression_Variable rng name))
getFirstVar  (UHA_Pat (Pattern_Constructor rng name _)) = (UHA_Expr (Expression_Constructor rng name))
getFirstVar (UHA_Pat (Pattern_Parenthesized _ pat)) = getFirstVar (UHA_Pat pat)
getFirstVar (UHA_Pat (Pattern_InfixConstructor rng p1 name p2)) =
    (UHA_Expr (Expression_Variable rng name))
getFirstVar _ = (UHA_Expr (Expression_Variable undefined (Name_Identifier undefined undefined "")))
                                                                 

                                                                 
--Feature 11 functions
getFeature11Name (LibFunc x) = Just (getVarName x)
getFeature11Name (Oper x) = Just (getVarName x)
getFeature11Name (Exp x) = Just (getVarName x)
getFeature11Name _ = Nothing
third (x,y,z) = z

--feature 3 and 4 function
feature3And4 theTerrs = map (\x -> (snd . fst $ x, snd . snd $ x)) theTerrs

--helper for feature12

   
--start of feature14 code





--end feature14 code

--Just get One line messages with a string
handleMessageLine (MessageOneLiner x) = case x of
                                      (MessageString msg) ->
                                          do putStrLn msg
                                      _ ->
                                          do putStrLn "Message too complicated"
handleMessageLine _ = do
  putStrLn "Not a one line error"

--a block contains block pairs
extractTypes (TypeError _ _ blocks _) = 
    map extract blocks

gBlocks (TypeError _ _ blocks _) = blocks
        
extractMessages (TypeError _ line blocks _) =
    (map zipMessages blocks)

suggestions (TypeError _ lines blocks _) =
    concatMap suggestion lines

hints (TypeError _ _ _ hintLst) = hintLst
suggestion line = 
    case line of
      (MessageOneLiner blck) -> [blck]
      (MessageTable tbl) ->
          let pairLst = unzip (map (\(x,y,z) -> (y, z)) tbl)
          in (fst pairLst) ++ (snd pairLst)
      _ -> []

--return the messages blocks as  pairs
zipMessages (_, block1, block2) = (block1, block2)
combineMessages (_,block1, block2) = [block1,block2]

messageCheck ( ((MessageString msg1), (MessageType ty1)), ((MessageString msg2), (MessageType ty2)) ) =
    (msg1 == "type" ||  msg1 == "inferred type" || msg1 == "declared type")  && (msg2 == "does not match" || msg2 == "expected type" || msg2 == "declared type" || msg2 == "inferred type" || msg2 == "used as")
messageCheck _ = False

causeCheck (MessageString msg1, MessageString msg2) =
    msg1 == "because" || msg2 == "because"
causeCheck (MessageString msg1, _) =
    msg1 == "because"
causeCheck (_, MessageString msg2) =
    msg2 == "because"
causeCheck _ = False
               
getCause messageList = do
  let blockMessages = map extractMessages messageList --look throught the message blocks
  let bms = concat blockMessages 
  --let bMsgs = pairs bms --combine the parts of the messages into pairs
  let theCause = filter causeCheck bms -- look for subsequent pairs where the
  return bms -- not sure why not using thecause
--get the typeScheme out of a block
extract (_, block1, block2) =  (typesOutOfBlock block1, typesOutOfBlock block2)

getOTrees terrs = do
  let blocks = map gBlocks terrs
  let blockMessages = map (concatMap combineMessages) blocks --look throught the message blocks
  let bms = blockMessages
  let ots = map (filter oneTree) bms
  --let bMsgs = pairs bms --combine the parts of the messages into pairs
  return ots                               

oneTree (MessageOneLineTree x) = True
oneTree _ = False
                                       
-- ===========================================================================
--end code for features 2 and 3 and 4
maybeTup (Nothing,Nothing) = Nothing
maybeTup (Just a,Nothing) = Just [a]
maybeTup (Just a, Just b) = Just [a,b]
maybeTup (_, Just b) = Just [b]

--start of feature 10, 11 code
-- ===========================================================================
getLocalInfo :: ConstraintInfo -> InfoTree
getLocalInfo cinfo = localInfo cinfo


offenderLocation :: ConstraintInfo -> NodeType
offenderLocation = nodeType . offendingInfoTree  --localInfo --offendingInfoTree

data OffenderType = Exp UHA_Source | LibFunc UHA_Source | UserFunc UHA_Source
                  | Oper UHA_Source | Constant UHA_Source | Argument UHA_Source
                  | RHS UHA_Source
                  | OffError deriving (Eq, Show)
{-instance Show OffenderType where
    show ot =
        case ot of
          (Exp _) -> "Expression"
          (LibFunc _) -> "Lib Function"
          (UserFunc _) -> "User Function"
          (Oper _) -> "Operator"
          (Constant _) -> "Constant"
          (Argument _) -> "Parameter"
          _ -> "Error"
-}

fea11Body (Oper body) = (Just body)
fea11Body (LibFunc body) = (Just body)
fea11Body (UserFunc body) = (Just body)
fea11Body _ = Nothing
offenderType combinedEnv localEnv importEnv (src,typ) =
    case typ of
      Leaf -> srcVariableType combinedEnv localEnv importEnv src
      Root -> srcVariableType combinedEnv localEnv importEnv src
      _ -> case src of
             (UHA_RHS rhs) -> RHS src
             _ -> Exp src

srcVariableType combinedEnv localEnv importEnv (UHA_Expr exp) =
    case exp of
      Expression_Literal _ literal -> (Constant (UHA_Expr exp))
      Expression_Variable _ name ->
          let look = member name
          in case ((look localEnv), look (typeEnvironment importEnv), look (operatorTable combinedEnv)) of
               (_,_,True) -> Oper (UHA_Expr exp)
               (True,_,_) -> UserFunc (UHA_Expr exp)
               (_,True,_) -> LibFunc (UHA_Expr exp)
               _ -> Argument (UHA_Expr exp) --if it's a variable and not a function, it's an argument
      Expression_Constructor _ name ->
          let look = member name
          in case ((look localEnv), look (typeEnvironment importEnv), look (operatorTable combinedEnv)) of
               (_,_,True) -> Oper (UHA_Expr exp)
               (True,_,_) -> UserFunc (UHA_Expr exp)
               (_,True,_) -> LibFunc (UHA_Expr exp)
               _ -> Argument (UHA_Expr exp) --if it's a variable and not a function, it's an argument
      _ -> OffError
srcVariableType _ _ _ (UHA_Def name) = UserFunc (UHA_Expr (Expression_Variable undefined name))
srcVariableType _ _ _ _ = OffError

isOper (Oper _) = True
isOper _ = False
-- ===========================================================================
--end feature 10, 11, 12 code

-- start of code for features 2 and 3 and 4
-- ===========================================================================
getTypeErrors messageList = 
  let blockMessages = map extractMessages messageList --look throught the message blocks
      bms = concat blockMessages 
      bMsgs = pairs bms --combine the parts of the messages into pairs
      theTerrs = filter messageCheck bMsgs -- look for subsequent pairs where the
                                           -- first pair's left element is "type"
                                           -- and the second pair's first is "does not match"
  in feature3And4 theTerrs --pull the type errors from the list

typesOutOfBlock (MessageType tyScheme) = Just tyScheme
typesOutOfBlock _ = Nothing


handleTErr (TypeError _ errs _ _) = errs

                    
consSubTree :: ConstraintInfo -> String
consSubTree (CInfo_ _ _ localinf _ _ _ _ flag) = subTreeStr 0 localinf

subTreeStr :: Int -> InfoTree -> String
subTreeStr ind inf = replicate (3*ind) ' ' ++
                    (show . self . attribute) inf ++ " (" ++ show (nodeType inf) ++')' : "\t\t" ++
                    -- showITJust inf ++ " (" ++ show (nodeType inf) ++')' : "\t\t" ++
                    (show . assignedType . attribute) inf ++ "\n" ++
                    concatMap (subTreeStr (ind+1)) (children inf)

                    
parPathStr ind inf 
    | nodeType inf == Root = replicate ind '\t' ++ show Root ++ "\n"
    | otherwise = parPathStr (ind+1) (fromJust $ parent inf) ++ selfStr
    where selfStr = replicate ind '\t' ++ show (nodeType inf) ++ "\t" ++
                    (show . self . attribute) inf ++ "\n"

-- Deciding if the currnet inf is the rightmost node in the tree                    
rgtMost inf 
    | nodeType inf == Root = True
    | otherwise = let par = (fromJust . parent) inf
                      all = children par
                  in if uha_source inf == uha_source (last all)
                     then rgtMost par
                     else False
           
getRoot iTree
    | nodeType iTree == Root = iTree
    | otherwise = getRoot (fromJust $ parent iTree)
                                                      
                    
--start of feature 14 helpers
-- =========================================================================================================
extractFunctionArguments (UHA_Expr (Expression_NormalApplication _ _ argList)) = map MaybeExpression_Just argList
extractFunctionArguments (UHA_Expr (Expression_InfixApplication _ left _ right)) = [left,right]
extractFunctionArguments _ = []



countParentheses (MaybeExpression_Just (Expression_Parenthesized _ exp)) = 1 + (countParentheses (MaybeExpression_Just exp))
countParentheses (MaybeExpression_Just (Expression_NormalApplication _ _ argList)) = totalParentheses (map MaybeExpression_Just argList)
countParentheses (MaybeExpression_Just (Expression_InfixApplication _ left _ right)) = totalParentheses [left,right]
countParentheses _ = 0

totalParentheses = sum . (map countParentheses) 

--start of feature 16 helpers
-- =========================================================================================================
data ParamStyle = AList | ACons | AConstructor | Mixed | PointFree | Single | ParamError deriving(Eq, Show)
-- getFeature16 = filter
               -- (\x -> case x of
                        -- (UHA_FB exp) -> True
                        -- _ -> False)
getPatName (Pattern_Variable _ name) = Just name
getPatName (Pattern_Constructor _ name _) = Just name
getPatName (Pattern_Parenthesized _ pat) = getPatName pat
getPatName _ = Nothing
               
declHelp (Declaration_PatternBinding _ pat _ ) = PointFree
                                                 
feature16Func = (map
                 (\x ->
                      let src = (self . attribute) x
                      in
                        case src of
                          (UHA_FB (FunctionBinding_FunctionBinding _ (LeftHandSide_Function _ name argList) _)) ->
                              if (null $ inOrderListSearch (head $ children x))
                              then
                                  (parameterStyle src)
                              else
                                  Just (name,AList)
                          _ -> (parameterStyle src)
                          --(parameterStyle src) -- . concat . (map children) . children --kind of ad hoc might not hold up
                 ))


                 
parameterStyle (UHA_FB (FunctionBinding_FunctionBinding _ (LeftHandSide_Function _ name argList) _)) = Just (name, (foldl' paramFoldHelp PointFree (map determineParamStyle argList)))
parameterStyle (UHA_Decl (Declaration_PatternBinding _ pat _ )) =
    case (getPatName pat) of
      (Just nom) -> Just (nom, PointFree)
      _ -> Nothing
parameterStyle (UHA_Decls decls) =
    if null decls
       then Nothing
       else let d1 = UHA_Decl (head decls)
            in case (parameterStyle d1) of
                 (Just (name,_)) ->
                      Just (name, 
                                foldl' paramFoldHelp PointFree (map declHelp decls))
                 _ -> Nothing
parameterStyle _ = Nothing
                   
paramFoldHelp x y = if x == y
                    then x
                    else if x == PointFree
                         then y
                         else
                             case (x,y) of
                               (Single,y) -> y
                               (z,Single) -> z
                               (AList,_) -> AList
                               (_,AList) -> AList
                               (ACons,_) -> ACons
                               (_,ACons) -> ACons
                              
determineParamStyle (Pattern_Parenthesized _ exp) = determineParamStyle exp
determineParamStyle (Pattern_Variable _ name) = Single
determineParamStyle (Pattern_InfixConstructor _ _ _ _) = ACons
-- determineParamStyle (Pattern_InfixConstructor _ lp _ rp) 
    -- | determineParamStyle lp 
    
-- ACons
determineParamStyle (Pattern_List _ _) = AList
determineParamStyle (Pattern_Constructor _ (Name_Special _ _ "[]") _) = AList
determineParamStyle (Pattern_Constructor _ _ _) = AConstructor
determineParamStyle _ = ParamError

--feature 18 help start
-- =========================================================================================================
getRightHandSide (UHA_FB (FunctionBinding_FunctionBinding _ _ rhs)) = Just rhs
getRightHandSide (UHA_RHS rhs) = Just rhs
getRightHandSide _ = Nothing

getVarBody (UHA_Expr (Expression_Variable rng exp)) = Just (Expression_Variable rng exp)
getVarBody (UHA_Expr (Expression_Constructor rng exp)) = Just (Expression_Constructor rng exp)
getVarBody _ = Nothing
                                                  {-
getVarBody (UHA_RHS (RightHandSide_Expression (rng) (exp) (maybeDecls))) = exp
getVarBody (UHA_FB (FunctionBinding_FunctionBinding (rng) (lhs) (rhs))) = getVarBody (UHA_RHS rhs)-}
   -- | UHA_Decl   Declaration

{-searchRightHandSideHelper (Just x) (Just y) = Just (searchRightHandSide x y)
searchRightHandSideHelper _ _ = Nothing
searchRightHandSide var rhs = findNameRHS var rhs
-}


{-findNameRHS var (RightHandSide_Expression _ exp _) = length (filter (var ==) (getVarsExpression exp))
--This is probably a bad idea
findNameRHS _ _ = -1-}

--feature 19 help start
-- =========================================================================================================
isExpParen (Expression_Parenthesized _ exp) = True
isExpParen _ = False

parenBody (Expression_Parenthesized _ exp) = Just exp
parenBody _ = Nothing

varName (Expression_Variable _ name) = name
varName (Expression_Constructor _ name) = name
{-varName (Pattern_InfixConstructor _ _ name _) = name
varName (Pattern_Variable _ name) = name-}
varName _ = Name_Identifier undefined undefined ""

{-determineIndex offLeaf (UHA_Expr (Expression_NormalApplication _ func arguments))
    = fst . head $ (filter ((offLeaf ==)  . snd)) indexedNameList
    where unparenthesizedList = concatMap (getExpressionVariables . unParen) ([func] ++ arguments)
          leafName = varName offLeaf
          indexedNameList = zip [1..] (filter ((leafName ==) . varName) unparenthesizedList)-}
getFb (UHA_FB fb) = fb

determineIndex offLeaf fb 
    =
      case offLeaf of
        (Just ol) ->
            let
                expVars = (inOrder fb)
                leafName = varName ol
                indexedNameList = zip [1..] (filter ((leafName ==) . varName) expVars)
            in
              (fmap fst . safeHead $ (filter ((ol ==) . snd)) indexedNameList)

        _ -> Nothing   

--determineIndex _ _ = -1 --error
                     
unParen (Expression_Parenthesized _ exp) = unParen exp
unParen notParen = notParen

-- Start Code for feature 20
-- ==========================================================================
rightSubTree (UHA_Expr (Expression_InfixApplication _ _ _ (MaybeExpression_Just right))) = Just right
rightSubTree _ = Nothing

leftSubTree (UHA_Expr (Expression_InfixApplication _ (MaybeExpression_Just left) _ _)) = Just left
leftSubTree _ = Nothing
       
inOrder :: InfoTree -> Expressions
inOrder node =
    case (self . attribute) node of
      UHA_Expr (Expression_Variable x y) -> [(Expression_Variable x y)] ++ (concatMap inOrder (children node))
      UHA_Expr (Expression_Constructor g h) -> [(Expression_Constructor g h)] ++ (concatMap inOrder (children node))
      _ -> concatMap inOrder (children node)

inOrderConst :: InfoTree -> Expressions
inOrderConst node =
    case (self . attribute) node of
      UHA_Expr (Expression_Variable x y) -> [(Expression_Variable x y)] ++ (concatMap inOrder (children node))
      UHA_Expr (Expression_Constructor g h) -> [(Expression_Constructor g h)] ++ (concatMap inOrder (children node))
      UHA_Expr (Expression_Literal a b) -> [(Expression_Literal a b)] ++ (concatMap inOrder (children node))
      _ -> concatMap inOrder (children node)

inOrderBracket :: InfoTree -> Expressions
inOrderBracket node =
    case (self . attribute) node of
      UHA_Expr (Expression_Constructor x (Name_Special y z "[]")) -> [(Expression_Constructor x (Name_Special y z "[]"))] ++ (concatMap inOrderBracket (children node))
      UHA_Expr (Expression_List x y) -> [(Expression_List x y)] ++ (concatMap inOrderBracket (children node))
      _ -> concatMap inOrderBracket (children node)


inOrderListSearch node =
    case (self . attribute) node of
      (UHA_Pat (Pattern_Constructor x (Name_Special y z "[]") a)) -> [(Pattern_Constructor x (Name_Special y z "[]") a)] ++ (concatMap inOrderListSearch (children node))
      _ -> concatMap inOrderListSearch (children node)

operWithAssoc opTable assoc node =
    case assoc of
      Just asso ->
          case (Data.Map.lookup (varName node) opTable) of
            (Just (prec,assc)) -> if assc == asso
                                  then Just node
                                  else Nothing
            _ -> Nothing
      _ -> Nothing

safeHead (x:xs) = Just x
safeHead [] = Nothing

binaryLength x = if x>0 then 1 else 0
{-
Start of code to extract variables from source tree
===============================================================================
-}

getVarName (UHA_Expr (Expression_Variable _ name)) = Just name
getVarName (UHA_Expr (Expression_Constructor _ name)) = Just name
getVarName (UHA_Pat (Pattern_InfixConstructor _ _ name _)) = Just name
getVarName (UHA_Pat ((Pattern_Variable _ name))) = Just name
getVarName _ = Nothing

--Start of commented out printing utilites in feature extraction code

--mapM (putStrLn . show . fmap (self . attribute)) newCinfo

  -- mapM (putStrLn . show) infoTree
  --mapM (putStrLn . show . otherSource) cinfo
  --mapM  handleMessageLine (concatMap handleTErr tErrors)
  --let cinfo = 
  -- putStrLn . show $ combinedEnv
  --mapM (putStrLn . show . sources) cinf

  --putStrLn "The subtree structure"
  --mapM (putStrLn . consSubTree) cinfo
  --putStrLn "The path to the root"
  --mapM (putStrLn . parPathStr 0 . localInfo) cinfo
          -- mapM (putStrLn . show . self . attribute) locals
  --mapM (putStrLn . show . nodeType . getLocalInfo) cinfo          
  --putStrLn "Testing Location"
  --mapM (putStrLn . show . self . attribute . offendingInfoTree) cinf

  --mapM (putStrLn . show . (map (self . attribute)) . children . offendingInfoTree) cinfo
  --mapM (putStrLn . show . (fmap (self . attribute)) . parent . offendingInfoTree) cinfo

  
  --mapM (putStrLn . show) infoTree
  --putStrLn "offenderParent"
  --mapM (putStrLn . show . offenderParent . offendingInfoTree) cinfo  



  -- putStrLn "offender"
  -- mapM (putStrLn . ("\n" ++) . show . self. attribute . offendingInfoTree) cinfo
  -- mapM (putStrLn . ("\n" ++) . showITJust . offendingInfoTree) cinfo
  
  -- mapM (putStrLn . concatMap (\(x,y,z) -> show (x, y)++"\n") . predecessors . offendingInfoTree) cinfo
  

  -- putStrLn "siblingAtAsExpr"
  -- mapM (putStrLn . show . siblingAtAsExpr 1 . offendingInfoTree) cinfo
  
  -- putStrLn "siblingAtAsIT"
  -- mapM (putStrLn . show . uha_source . fromJust . siblingAtAsIT 1 . offendingInfoTree) cinfo
  
  -- putStrLn "Subtree"
  -- mapM (putStrLn . subTreeStr 0. fromJust . parent . offendingInfoTree) cinfo
  

  --putStrLn "Offender location"
