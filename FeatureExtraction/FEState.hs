module FEState where

import Control.Monad.State
import LocalPref

{-
FEState 
1st: records the level in the unification process
-}

type FEState = (Integer,TypeDiff)

initFEState = (0,TypeDiff { brackets = [], funcs = [], topLevelDiff = 0, topLevBra = 0, likelyFuncs = []})

data TypeDiff = TypeDiff { brackets :: [(Int,Int)]
                         , funcs    :: [(Int,Int)]
                         , topLevelDiff :: Int
                         , likelyFuncs :: [(Int,Int)]
                         , topLevBra :: Int}
                deriving (Eq,Show)

eqDefinite :: TypeDiff -> TypeDiff -> Bool
eqDefinite (TypeDiff bra1 funcs1 topL1 likely1 _ ) ((TypeDiff bra2 funcs2 topL2 likely2 _)) = likely1 == likely2 && funcs1 == funcs2 && topL1 == topL2                         
                         
incIdx :: State FEState ()
incIdx = do
    (i,fs) <- get
    put (i+1,fs)

addFuncs :: (Int,Int) -> State FEState ()
addFuncs func = do
    (i,features) <- get
    put (i,features{funcs = func:funcs features})

addLikelyFuncs :: (Int,Int) -> State FEState ()
addLikelyFuncs (_,0) = return ()
addLikelyFuncs like = do
    (i,features) <- get
    put (i,features{likelyFuncs = like:likelyFuncs features})
    
addBrackets :: (Int,Int) -> State FEState ()
addBrackets brac = do
    (i,features) <- get
    put (i,features{brackets = brac:brackets features})

recTopDiff :: Int -> State FEState ()
recTopDiff diff = do
    (i,features) <- get
    put (i,features{topLevelDiff = diff})

recTopBra brac = do
    (i,features) <- get
    put (i, features{topLevBra = brac})
