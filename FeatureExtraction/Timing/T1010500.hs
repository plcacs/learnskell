import List
type Table = [[String]]

maxWidth :: Table -> [Int]
maxWidth tabel = map f t
               where t = transpose tabel
                     f kolom = maximum (map length kolom)

writeLines :: [Int] -> String
writeLines []     = "+\n"
writeLines (x:xs) = "+" ++ replicate x '-'
                        ++ writeLines xs

writeWords :: [Int] -> [String] -> String
writeWords [] []         = "|\n"
writeWords (x:xs) (y:ys) = "|" ++
                           y ++
                           replicate (x - (length y)) ' '  ++
                           writeWords xs ys

writeTableN table@(x:xs) = (writeLines (maxWidth table))++ (writeWords (maxWidth table) (table !! 0)) ++ (writeLines (maxWidth table))++ (writeWords (maxWidth table) map (table !!) [1,2 .. length table])

writeTableN2 table@(x:xs) = (writeLines (maxWidth table))++ (writeWords (maxWidth table) (table !! 0)) ++ (writeLines (maxWidth table))++ (writeWords (maxWidth table) map (table !!) [1,2 .. length table])

writeTableN3 table@(x:xs) = (writeLines (maxWidth table))++ (writeWords (maxWidth table) (table !! 0)) ++ (writeLines (maxWidth table))++ (writeWords (maxWidth table) map (table !!) [1,2 .. length table])

writeTableN4 table@(x:xs) = (writeLines (maxWidth table))++ (writeWords (maxWidth table) (table !! 0)) ++ (writeLines (maxWidth table))++ (writeWords (maxWidth table) map (table !!) [1,2 .. length table])

writeTableN5 table@(x:xs) = (writeLines (maxWidth table))++ (writeWords (maxWidth table) (table !! 0)) ++ (writeLines (maxWidth table))++ (writeWords (maxWidth table) map (table !!) [1,2 .. length table])

writeTableN6 table@(x:xs) = (writeLines (maxWidth table))++ (writeWords (maxWidth table) (table !! 0)) ++ (writeLines (maxWidth table))++ (writeWords (maxWidth table) map (table !!) [1,2 .. length table])

writeTableN7 table@(x:xs) = (writeLines (maxWidth table))++ (writeWords (maxWidth table) (table !! 0)) ++ (writeLines (maxWidth table))++ (writeWords (maxWidth table) map (table !!) [1,2 .. length table])

writeTableN8 table@(x:xs) = (writeLines (maxWidth table))++ (writeWords (maxWidth table) (table !! 0)) ++ (writeLines (maxWidth table))++ (writeWords (maxWidth table) map (table !!) [1,2 .. length table])

writeTableN9 table@(x:xs) = (writeLines (maxWidth table))++ (writeWords (maxWidth table) (table !! 0)) ++ (writeLines (maxWidth table))++ (writeWords (maxWidth table) map (table !!) [1,2 .. length table])

writeTableN10 table@(x:xs) = (writeLines (maxWidth table))++ (writeWords (maxWidth table) (table !! 0)) ++ (writeLines (maxWidth table))++ (writeWords (maxWidth table) map (table !!) [1,2 .. length table])
                   
writeTableS table = (writeLines (maxWidth table))++ (writeWords (maxWidth table) (table !! 0)) ++ (writeLines (maxWidth table))++ (writeWords (maxWidth table) (map (table !!) [1,2 .. length table]))

writeTableS2 table = (writeLines (maxWidth table))++ (writeWords (maxWidth table) (table !! 0)) ++ (writeLines (maxWidth table))++ (writeWords (maxWidth table) (map (table !!) [1,2 .. length table]))

writeTableS3 table = (writeLines (maxWidth table))++ (writeWords (maxWidth table) (table !! 0)) ++ (writeLines (maxWidth table))++ (writeWords (maxWidth table) (map (table !!) [1,2 .. length table]))

writeTableS4 table = (writeLines (maxWidth table))++ (writeWords (maxWidth table) (table !! 0)) ++ (writeLines (maxWidth table))++ (writeWords (maxWidth table) (map (table !!) [1,2 .. length table]))

writeTableS5 table = (writeLines (maxWidth table))++ (writeWords (maxWidth table) (table !! 0)) ++ (writeLines (maxWidth table))++ (writeWords (maxWidth table) (map (table !!) [1,2 .. length table]))

writeTableS6 table = (writeLines (maxWidth table))++ (writeWords (maxWidth table) (table !! 0)) ++ (writeLines (maxWidth table))++ (writeWords (maxWidth table) (map (table !!) [1,2 .. length table]))

writeTableS7 table = (writeLines (maxWidth table))++ (writeWords (maxWidth table) (table !! 0)) ++ (writeLines (maxWidth table))++ (writeWords (maxWidth table) (map (table !!) [1,2 .. length table]))

writeTableS8 table = (writeLines (maxWidth table))++ (writeWords (maxWidth table) (table !! 0)) ++ (writeLines (maxWidth table))++ (writeWords (maxWidth table) (map (table !!) [1,2 .. length table]))

writeTableS9 table = (writeLines (maxWidth table))++ (writeWords (maxWidth table) (table !! 0)) ++ (writeLines (maxWidth table))++ (writeWords (maxWidth table) (map (table !!) [1,2 .. length table]))

writeTableS10 table = (writeLines (maxWidth table))++ (writeWords (maxWidth table) (table !! 0)) ++ (writeLines (maxWidth table))++ (writeWords (maxWidth table) (map (table !!) [1,2 .. length table]))

writeTableWellL9 :: Table -> String
writeTableWellL9 table@(x:xs) = (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++ 
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))))
                   
writeTableWellL8 :: Table -> String
writeTableWellL8 table@(x:xs) = (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++ 
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))))
                   
writeTableWellL7 :: Table -> String
writeTableWellL7 table@(x:xs) = (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++ 
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))))
                   
writeTableWellL6 :: Table -> String
writeTableWellL6 table@(x:xs) = (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++ 
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))))
                   
writeTableWellL5 :: Table -> String
writeTableWellL5 table@(x:xs) = (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++ 
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) 
                   
