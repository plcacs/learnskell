import List

type Table = [[String]]


compilers :: Table
compilers =
  [ ["Compiler", "Universiteit/bedrijf"]
  , ["Helium", "Universiteit van Utrecht"]
  , ["NHC", "University of York"]
  , ["GHC", "Microsoft Research"]
  , ["Hugs", "Galois Connections"]
  , ["Hugs.NET", "Galois Connections"]
  , ["O'Haskell", "Oregon Graduate Institute"]
  , ["O'Haskell", "Chalmers University of Technology"]
  , ["HBC", "Chalmers University of Technology"]
  ]

locaties :: Table
locaties =
  [ ["Universiteit/bedrijf", "Land", "Stad"]
  , ["Universiteit van Utrecht", "Nederland", "Utrecht"]
  , ["University of York", "Engeland", "York"]
  , ["Microsoft Research", "Engeland", "Cambridge"]
  , ["Galois Connections", "Verenigde Staten", "Beaverton"]
  , ["Oregon Graduate Institute", "Verenigde Staten", "Beaverton"]
  , ["Chalmers University of Technology", "Zweden", "Goteborg"]
  ]






maxWidth :: Table -> [Int]
maxWidth tabel = map f t
               where t = transpose tabel
                     f kolom = maximum (map length kolom)

writeLines :: [Int] -> String
writeLines []     = "+\n"
writeLines (x:xs) = "+" ++ replicate x '-'
                        ++ writeLines xs

writeWords :: [Int] -> [String] -> String
writeWords [] []         = "|\n"
writeWords (x:xs) (y:ys) = "|" ++
                           y ++
                           replicate (x - (length y)) ' '  ++

                           writeWords xs ys

writeTableN :: Table -> String
writeTableN table@(x:xs) = (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) map (table !!) [1,2 .. length table])

writeTableN2 :: Table -> String
writeTableN2 table@(x:xs) = (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) map (table !!) [1,2 .. length table])

writeTableN3 :: Table -> String
writeTableN3 table@(x:xs) = (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) map (table !!) [1,2 .. length table])
                   
writeTableS :: Table -> String
writeTableS table = (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (map (table !!) [1,2 .. length table]))
                   
writeTableN5 :: Table -> String
writeTableN5 table@(x:xs) = (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) map (table !!) [1,2 .. length table])
                   
writeTableS5 :: Table -> String
writeTableS5 table = (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (map (table !!) [1,2 .. length table]))
                   
writeTableN4 :: Table -> String
writeTableN4 table@(x:xs) = (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) map (table !!) [1,2 .. length table])
                   
writeTableS4 :: Table -> String
writeTableS4 table = (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (map (table !!) [1,2 .. length table]))
                   
writeTableS2 :: Table -> String
writeTableS2 table = (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (map (table !!) [1,2 .. length table]))

writeTableS3 :: Table -> String
writeTableS3 table = (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (map (table !!) [1,2 .. length table]))

                   
writeTableWell4 :: Table -> String
writeTableWell4 table@(x:xs) = (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++ 
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))))


                  
                   
writeTableWellL1 :: Table -> String
writeTableWellL1 table@(x:xs) = (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++ 
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))))
