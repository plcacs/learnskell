import List
type Table = [[String]]

maxWidth :: Table -> [Int]
maxWidth tabel = map f t
               where t = transpose tabel
                     f kolom = maximum (map length kolom)

writeLines :: [Int] -> String
writeLines []     = "+\n"
writeLines (x:xs) = "+" ++ replicate x '-'
                        ++ writeLines xs

writeWords :: [Int] -> [String] -> String
writeWords [] []         = "|\n"
writeWords (x:xs) (y:ys) = "|" ++
                           y ++
                           replicate (x - (length y)) ' '  ++
                           writeWords xs ys

writeTableN table@(x:xs) = (writeLines (maxWidth table))++ (writeWords (maxWidth table) (table !! 0)) ++ (writeLines (maxWidth table))++ (writeWords (maxWidth table) map (table !!) [1,2 .. length table])

writeTableN2 table@(x:xs) = (writeLines (maxWidth table))++ (writeWords (maxWidth table) (table !! 0)) ++ (writeLines (maxWidth table))++ (writeWords (maxWidth table) map (table !!) [1,2 .. length table])

writeTableN3 table@(x:xs) = (writeLines (maxWidth table))++ (writeWords (maxWidth table) (table !! 0)) ++ (writeLines (maxWidth table))++ (writeWords (maxWidth table) map (table !!) [1,2 .. length table])

writeTableN4 table@(x:xs) = (writeLines (maxWidth table))++ (writeWords (maxWidth table) (table !! 0)) ++ (writeLines (maxWidth table))++ (writeWords (maxWidth table) map (table !!) [1,2 .. length table])

writeTableN5 table@(x:xs) = (writeLines (maxWidth table))++ (writeWords (maxWidth table) (table !! 0)) ++ (writeLines (maxWidth table))++ (writeWords (maxWidth table) map (table !!) [1,2 .. length table])

writeTableN6 table@(x:xs) = (writeLines (maxWidth table))++ (writeWords (maxWidth table) (table !! 0)) ++ (writeLines (maxWidth table))++ (writeWords (maxWidth table) map (table !!) [1,2 .. length table])

writeTableN7 table@(x:xs) = (writeLines (maxWidth table))++ (writeWords (maxWidth table) (table !! 0)) ++ (writeLines (maxWidth table))++ (writeWords (maxWidth table) map (table !!) [1,2 .. length table])

writeTableN8 table@(x:xs) = (writeLines (maxWidth table))++ (writeWords (maxWidth table) (table !! 0)) ++ (writeLines (maxWidth table))++ (writeWords (maxWidth table) map (table !!) [1,2 .. length table])

writeTableN9 table@(x:xs) = (writeLines (maxWidth table))++ (writeWords (maxWidth table) (table !! 0)) ++ (writeLines (maxWidth table))++ (writeWords (maxWidth table) map (table !!) [1,2 .. length table])

writeTableN10 table@(x:xs) = (writeLines (maxWidth table))++ (writeWords (maxWidth table) (table !! 0)) ++ (writeLines (maxWidth table))++ (writeWords (maxWidth table) map (table !!) [1,2 .. length table])

writeTableN11 table@(x:xs) = (writeLines (maxWidth table))++ (writeWords (maxWidth table) (table !! 0)) ++ (writeLines (maxWidth table))++ (writeWords (maxWidth table) map (table !!) [1,2 .. length table])

writeTableN12 table@(x:xs) = (writeLines (maxWidth table))++ (writeWords (maxWidth table) (table !! 0)) ++ (writeLines (maxWidth table))++ (writeWords (maxWidth table) map (table !!) [1,2 .. length table])

writeTableN13 table@(x:xs) = (writeLines (maxWidth table))++ (writeWords (maxWidth table) (table !! 0)) ++ (writeLines (maxWidth table))++ (writeWords (maxWidth table) map (table !!) [1,2 .. length table])

writeTableN14 table@(x:xs) = (writeLines (maxWidth table))++ (writeWords (maxWidth table) (table !! 0)) ++ (writeLines (maxWidth table))++ (writeWords (maxWidth table) map (table !!) [1,2 .. length table])

writeTableN15 table@(x:xs) = (writeLines (maxWidth table))++ (writeWords (maxWidth table) (table !! 0)) ++ (writeLines (maxWidth table))++ (writeWords (maxWidth table) map (table !!) [1,2 .. length table])

writeTableN16 table@(x:xs) = (writeLines (maxWidth table))++ (writeWords (maxWidth table) (table !! 0)) ++ (writeLines (maxWidth table))++ (writeWords (maxWidth table) map (table !!) [1,2 .. length table])

writeTableN17 table@(x:xs) = (writeLines (maxWidth table))++ (writeWords (maxWidth table) (table !! 0)) ++ (writeLines (maxWidth table))++ (writeWords (maxWidth table) map (table !!) [1,2 .. length table])

writeTableN18 table@(x:xs) = (writeLines (maxWidth table))++ (writeWords (maxWidth table) (table !! 0)) ++ (writeLines (maxWidth table))++ (writeWords (maxWidth table) map (table !!) [1,2 .. length table])

writeTableN19 table@(x:xs) = (writeLines (maxWidth table))++ (writeWords (maxWidth table) (table !! 0)) ++ (writeLines (maxWidth table))++ (writeWords (maxWidth table) map (table !!) [1,2 .. length table])

writeTableN20 table@(x:xs) = (writeLines (maxWidth table))++ (writeWords (maxWidth table) (table !! 0)) ++ (writeLines (maxWidth table))++ (writeWords (maxWidth table) map (table !!) [1,2 .. length table])
                   
writeTableS table = (writeLines (maxWidth table))++ (writeWords (maxWidth table) (table !! 0)) ++ (writeLines (maxWidth table))++ (writeWords (maxWidth table) (map (table !!) [1,2 .. length table]))

writeTableS2 table = (writeLines (maxWidth table))++ (writeWords (maxWidth table) (table !! 0)) ++ (writeLines (maxWidth table))++ (writeWords (maxWidth table) (map (table !!) [1,2 .. length table]))

writeTableS3 table = (writeLines (maxWidth table))++ (writeWords (maxWidth table) (table !! 0)) ++ (writeLines (maxWidth table))++ (writeWords (maxWidth table) (map (table !!) [1,2 .. length table]))

writeTableS4 table = (writeLines (maxWidth table))++ (writeWords (maxWidth table) (table !! 0)) ++ (writeLines (maxWidth table))++ (writeWords (maxWidth table) (map (table !!) [1,2 .. length table]))

writeTableS5 table = (writeLines (maxWidth table))++ (writeWords (maxWidth table) (table !! 0)) ++ (writeLines (maxWidth table))++ (writeWords (maxWidth table) (map (table !!) [1,2 .. length table]))

writeTableS6 table = (writeLines (maxWidth table))++ (writeWords (maxWidth table) (table !! 0)) ++ (writeLines (maxWidth table))++ (writeWords (maxWidth table) (map (table !!) [1,2 .. length table]))

writeTableS7 table = (writeLines (maxWidth table))++ (writeWords (maxWidth table) (table !! 0)) ++ (writeLines (maxWidth table))++ (writeWords (maxWidth table) (map (table !!) [1,2 .. length table]))

writeTableS8 table = (writeLines (maxWidth table))++ (writeWords (maxWidth table) (table !! 0)) ++ (writeLines (maxWidth table))++ (writeWords (maxWidth table) (map (table !!) [1,2 .. length table]))

writeTableS9 table = (writeLines (maxWidth table))++ (writeWords (maxWidth table) (table !! 0)) ++ (writeLines (maxWidth table))++ (writeWords (maxWidth table) (map (table !!) [1,2 .. length table]))

writeTableS10 table = (writeLines (maxWidth table))++ (writeWords (maxWidth table) (table !! 0)) ++ (writeLines (maxWidth table))++ (writeWords (maxWidth table) (map (table !!) [1,2 .. length table]))

writeTableS11 table = (writeLines (maxWidth table))++ (writeWords (maxWidth table) (table !! 0)) ++ (writeLines (maxWidth table))++ (writeWords (maxWidth table) (map (table !!) [1,2 .. length table]))

writeTableS12 table = (writeLines (maxWidth table))++ (writeWords (maxWidth table) (table !! 0)) ++ (writeLines (maxWidth table))++ (writeWords (maxWidth table) (map (table !!) [1,2 .. length table]))

writeTableS13 table = (writeLines (maxWidth table))++ (writeWords (maxWidth table) (table !! 0)) ++ (writeLines (maxWidth table))++ (writeWords (maxWidth table) (map (table !!) [1,2 .. length table]))

writeTableS14 table = (writeLines (maxWidth table))++ (writeWords (maxWidth table) (table !! 0)) ++ (writeLines (maxWidth table))++ (writeWords (maxWidth table) (map (table !!) [1,2 .. length table]))

writeTableS15 table = (writeLines (maxWidth table))++ (writeWords (maxWidth table) (table !! 0)) ++ (writeLines (maxWidth table))++ (writeWords (maxWidth table) (map (table !!) [1,2 .. length table]))

writeTableS16 table = (writeLines (maxWidth table))++ (writeWords (maxWidth table) (table !! 0)) ++ (writeLines (maxWidth table))++ (writeWords (maxWidth table) (map (table !!) [1,2 .. length table]))
                    
writeTableS17 table = (writeLines (maxWidth table))++ (writeWords (maxWidth table) (table !! 0)) ++ (writeLines (maxWidth table))++ (writeWords (maxWidth table) (map (table !!) [1,2 .. length table]))

writeTableS18 table = (writeLines (maxWidth table))++ (writeWords (maxWidth table) (table !! 0)) ++ (writeLines (maxWidth table))++ (writeWords (maxWidth table) (map (table !!) [1,2 .. length table]))

writeTableS19 table = (writeLines (maxWidth table))++ (writeWords (maxWidth table) (table !! 0)) ++ (writeLines (maxWidth table))++ (writeWords (maxWidth table) (map (table !!) [1,2 .. length table]))

writeTableS20 table = (writeLines (maxWidth table))++ (writeWords (maxWidth table) (table !! 0)) ++ (writeLines (maxWidth table))++ (writeWords (maxWidth table) (map (table !!) [1,2 .. length table]))


                  
