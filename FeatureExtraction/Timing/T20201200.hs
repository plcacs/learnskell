import List

type Table = [[String]]


compilers :: Table
compilers =
  [ ["Compiler", "Universiteit/bedrijf"]
  , ["Helium", "Universiteit van Utrecht"]
  , ["NHC", "University of York"]
  , ["GHC", "Microsoft Research"]
  , ["Hugs", "Galois Connections"]
  , ["Hugs.NET", "Galois Connections"]
  , ["O'Haskell", "Oregon Graduate Institute"]
  , ["O'Haskell", "Chalmers University of Technology"]
  , ["HBC", "Chalmers University of Technology"]
  ]

locaties :: Table
locaties =
  [ ["Universiteit/bedrijf", "Land", "Stad"]
  , ["Universiteit van Utrecht", "Nederland", "Utrecht"]
  , ["University of York", "Engeland", "York"]
  , ["Microsoft Research", "Engeland", "Cambridge"]
  , ["Galois Connections", "Verenigde Staten", "Beaverton"]
  , ["Oregon Graduate Institute", "Verenigde Staten", "Beaverton"]
  , ["Chalmers University of Technology", "Zweden", "Goteborg"]
  ]






maxWidth :: Table -> [Int]
maxWidth tabel = map f t
               where t = transpose tabel
                     f kolom = maximum (map length kolom)

writeLines :: [Int] -> String
writeLines []     = "+\n"
writeLines (x:xs) = "+" ++ replicate x '-'
                        ++ writeLines xs

writeWords :: [Int] -> [String] -> String
writeWords [] []         = "|\n"
writeWords (x:xs) (y:ys) = "|" ++
                           y ++
                           replicate (x - (length y)) ' '  ++

                           writeWords xs ys

writeTable1N :: Table -> String
writeTable1N table@(x:xs) = (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) map (table !!) [1,2 .. length table])

writeTableN12 :: Table -> String
writeTableN12 table@(x:xs) = (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) map (table !!) [1,2 .. length table])

writeTableN13 :: Table -> String
writeTableN13 table@(x:xs) = (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) map (table !!) [1,2 .. length table])
                   
writeTableS20 :: Table -> String
writeTableS20 table = (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (map (table !!) [1,2 .. length table]))
                   
writeTableN20 :: Table -> String
writeTableN20 table@(x:xs) = (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) map (table !!) [1,2 .. length table])
                   
writeTableS1 :: Table -> String
writeTableS1 table = (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (map (table !!) [1,2 .. length table]))
                   
                   
writeTableN19 :: Table -> String
writeTableN19 table@(x:xs) = (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) map (table !!) [1,2 .. length table])
                   
writeTableS19 :: Table -> String
writeTableS19 table = (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (map (table !!) [1,2 .. length table]))
                   
                   
writeTableN18 :: Table -> String
writeTableN18 table@(x:xs) = (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) map (table !!) [1,2 .. length table])
                   
writeTableS18 :: Table -> String
writeTableS18 table = (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (map (table !!) [1,2 .. length table]))
                   
writeTableN17 :: Table -> String
writeTableN17 table@(x:xs) = (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) map (table !!) [1,2 .. length table])
                   
writeTableS17 :: Table -> String
writeTableS17 table = (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (map (table !!) [1,2 .. length table]))
                   
writeTableN16 :: Table -> String
writeTableN16 table@(x:xs) = (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) map (table !!) [1,2 .. length table])
                   
writeTableS16 :: Table -> String
writeTableS16 table = (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (map (table !!) [1,2 .. length table]))
                   
writeTableN15 :: Table -> String
writeTableN15 table@(x:xs) = (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) map (table !!) [1,2 .. length table])
                   
writeTableS15 :: Table -> String
writeTableS15 table = (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (map (table !!) [1,2 .. length table]))
                   
writeTableN14 :: Table -> String
writeTableN14 table@(x:xs) = (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) map (table !!) [1,2 .. length table])
                   
writeTableS14 :: Table -> String
writeTableS14 table = (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (map (table !!) [1,2 .. length table]))
                   
writeTableS12 :: Table -> String
writeTableS12 table = (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (map (table !!) [1,2 .. length table]))

writeTableS13 :: Table -> String
writeTableS13 table = (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (map (table !!) [1,2 .. length table]))
                           
                           
writeTableN :: Table -> String
writeTableN table@(x:xs) = (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) map (table !!) [1,2 .. length table])

writeTableN2 :: Table -> String
writeTableN2 table@(x:xs) = (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) map (table !!) [1,2 .. length table])

writeTableN3 :: Table -> String
writeTableN3 table@(x:xs) = (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) map (table !!) [1,2 .. length table])
                   
writeTableS :: Table -> String
writeTableS table = (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (map (table !!) [1,2 .. length table]))
                   
writeTableN10 :: Table -> String
writeTableN10 table@(x:xs) = (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) map (table !!) [1,2 .. length table])
                   
writeTableS10 :: Table -> String
writeTableS10 table = (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (map (table !!) [1,2 .. length table]))
                   
                   
writeTableN9 :: Table -> String
writeTableN9 table@(x:xs) = (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) map (table !!) [1,2 .. length table])
                   
writeTableS9 :: Table -> String
writeTableS9 table = (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (map (table !!) [1,2 .. length table]))
                   
                   
writeTableN8 :: Table -> String
writeTableN8 table@(x:xs) = (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) map (table !!) [1,2 .. length table])
                   
writeTableS8 :: Table -> String
writeTableS8 table = (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (map (table !!) [1,2 .. length table]))
                   
writeTableN7 :: Table -> String
writeTableN7 table@(x:xs) = (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) map (table !!) [1,2 .. length table])
                   
writeTableS7 :: Table -> String
writeTableS7 table = (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (map (table !!) [1,2 .. length table]))
                   
writeTableN6 :: Table -> String
writeTableN6 table@(x:xs) = (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) map (table !!) [1,2 .. length table])
                   
writeTableS6 :: Table -> String
writeTableS6 table = (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (map (table !!) [1,2 .. length table]))
                   
writeTableN5 :: Table -> String
writeTableN5 table@(x:xs) = (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) map (table !!) [1,2 .. length table])
                   
writeTableS5 :: Table -> String
writeTableS5 table = (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (map (table !!) [1,2 .. length table]))
                   
writeTableN4 :: Table -> String
writeTableN4 table@(x:xs) = (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) map (table !!) [1,2 .. length table])
                   
writeTableS4 :: Table -> String
writeTableS4 table = (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (map (table !!) [1,2 .. length table]))
                   
writeTableS2 :: Table -> String
writeTableS2 table = (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (map (table !!) [1,2 .. length table]))

writeTableS3 :: Table -> String
writeTableS3 table = (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (map (table !!) [1,2 .. length table]))

                   
writeTableWell4 :: Table -> String
writeTableWell4 table@(x:xs) = (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++ 
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) 

writeTableWellL1 :: Table -> String
writeTableWellL1 table@(x:xs) = (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++ 
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))))


                  
                   
writeTableWellL9 :: Table -> String
writeTableWellL9 table@(x:xs) = (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++ 
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))))
                   
writeTableWellL8 :: Table -> String
writeTableWellL8 table@(x:xs) = (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++ 
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))))
                   
writeTableWellL7 :: Table -> String
writeTableWellL7 table@(x:xs) = (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++ 
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))))
                   
writeTableWellL6 :: Table -> String
writeTableWellL6 table@(x:xs) = (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++ 
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))))
                   
writeTableWellL5 :: Table -> String
writeTableWellL5 table@(x:xs) = (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++ 
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))))
                   
writeTableWellL4 :: Table -> String
writeTableWellL4 table@(x:xs) = (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++ 
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))))
                   
writeTableWellL3 :: Table -> String
writeTableWellL3 table@(x:xs) = (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++ 
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))))
                   
writeTableWellL2 :: Table -> String
writeTableWellL2 table@(x:xs) = (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++ 
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table])))) ++
                   (((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))) ++
                   ((writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (table !! 0)) ++
                   (writeLines (maxWidth table))++
                   (writeWords (maxWidth table) (concatMap (table !!) [1,2 .. length table]))))
                   
                   