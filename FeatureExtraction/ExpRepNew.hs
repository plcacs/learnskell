module ExpRepNew where

import Control.Applicative    
import Helium.Main.PhaseLexer
import Helium.Main.PhaseParser
import Helium.Main.PhaseImport
import Helium.Main.PhaseResolveOperators
import Helium.Main.PhaseStaticChecks
import Helium.Main.PhaseKindInferencer
import Helium.Main.PhaseTypingStrategies
import Helium.Main.PhaseTypeInferencer
import Helium.Main.PhaseDesugarer
import Helium.Main.PhaseCodeGenerator
import Helium.Main.CompileUtils
import Helium.StaticAnalysis.Messages.Messages
import Helium.StaticAnalysis.Messages.TypeErrors
import Helium.StaticAnalysis.Messages.TErrors
import Helium.StaticAnalysis.Messages.Warnings(Warning)
import Helium.StaticAnalysis.Inferencers.TypeInferencing(typeInferencing)
import Helium.ModuleSystem.DictionaryEnvironment (DictionaryEnvironment)
--import UHA_Syntax
import Helium.StaticAnalysis.Messages.Information (showInformation)
-- import System.FilePath.Posix
import Helium.Utils.Utils
import qualified Control.Exception as CE (catch, IOException)
import Data.IORef
import Helium.StaticAnalysis.Messages.StaticErrors(errorsLogCode)
import Helium.Parser.Parser(parseOnlyImports)
import Control.Monad
import System.FilePath(joinPath)
import Data.List(intersect,lookup,foldl', nub, elemIndex, isSuffixOf, intercalate,(\\),sortBy)
import Data.Maybe
import Data.Map(lookup,member,(!))
import Lvm.Path(explodePath,getLvmPath)
import System.Directory(doesFileExist, getModificationTime)
import Helium.Main.Args
import Paths_helium
import Top.Types
import Top.Types.Schemes
import Helium.StaticAnalysis.Miscellaneous.DoublyLinkedTree
import Helium.StaticAnalysis.Miscellaneous.UHA_Source
import Helium.Syntax.UHA_Syntax
import Helium.Parser.OperatorTable(Assoc(AssocLeft,AssocRight,AssocNone))
import Helium.Utils.OneLiner
import FEState
import TypeDiffNewNew
import Data.List.Split(splitOn,splitOneOf)
import Data.Either
import FeatureExtraction
import ExpRepHelper
import ExpRepBrac
import LocalPref

{- Example
Act: [String] -> Int
Exp: ([v191] -> v191) -> (Table -> Table) -> Table -> Int
Idx: 1

1st step: ([ [v191] -> v191, (Table -> Table), Table], Int) = functionSpineOfLength exp

This function requires relevant types to be exactly unifiable, 
which is maybe too restrictive
-}
repAddPar2 :: ErrorInfo -> (Tp,Tp) -> Int -> String -> Rep
repAddPar2 errInfo (act,exp) idx header = repAddPar2' errInfo (act,exp) idx header

repAddPar2' :: ErrorInfo -> (Tp,Tp) -> Int -> String -> Rep
repAddPar2' errInf (act,exp) idx header
    | length etps < 3 = error "repAddPar2': not enough type arity"
    
    -- The point-free function definition omitted two parameters
    | isNothing mit && paraStyle == Just 4 =
        Hint ( "Add two parameters named x and y to the point-free function,\n"
               ++ "Add an ( before: " 
               ++ showIT lit ++ errLoc (fromJust lit) ++
               ", and\nAdd (x y)) after that.") header

    -- The point-free function definition omitted one parameter
    | isNothing rit && paraStyle == Just 4 = 
        Hint ("Add a parameters named x to the point-free function,\n"
               ++ "Add an ( before: " 
               ++ showIT mit ++ errLoc (fromJust mit)
               ++ ",\nAdd x) after that," 
               ++ "\nAdd an ( before: " 
               ++ showIT lit ++ errLoc (fromJust lit)
               ++ ", and\nAdd an ) after x.") header
    -- If unification fails
    | isLeft unier1Ei || 
      isLeft unier2Ei || 
      isLeft unier3   = rep `Seq` (Hint "But need to make other changes" "")
      -- isLeft unier3   = error header
      
    | otherwise = rep
    where (etps,etp) = functionSpineOfLength (idx+2) exp
          paraStyle = getFeature16 errInf -- 4 is point-free

          retps = reverse etps
          
          newarg = head retps
          newfunc = head . tail $ retps
          newfuncfunc = head . drop 2 $ retps
          restetps = reverse . drop 3 $ retps
           
          offender = offendingInfoTree . snd $ errInf
          
          -- May need to consider if the offender is an infix
          -- operator
          lit = siblingAtAsIT idx offender
          mit = siblingAtAsIT (idx+1) offender
          rit = siblingAtAsIT (idx+2) offender
          rep = AddParIT (fromJust mit) (fromJust rit) header
                `Seq` 
                AddParIT (fromJust lit) (fromJust rit) ""
          
          unier1Ei = mgu newfunc (newarg .->. lgTv1) 
          unier1 = either (error . show) id unier1Ei
          unier2Ei = mgu newfuncfunc (unier1 |-> lgTv1 .->. lgTv2)
          unier2 = either (error . show) id unier2Ei 
          unier3 = mgu act (foldr (.->.) etp (restetps ++ [unier2 |-> lgTv2]))
          
{-
Example
Act: Table -> [Int]
Exp: ([v236] -> [v236]) -> Table -> [Int]
Idx: 1

1st step:
    ([ ([v236] -> [v236]), Table, [Int]) = functionSpineOfLength 2 exp
    
2nd step: unify
    ([v236] -> [v236])
with
    Table -> lgTv1
and lgTv1 will be refined to Table

3rd: unify 
    Table -> [Int]
with the Act
    Table -> [Int]    
-}    
repAddParDist1 :: ErrorInfo -> (Tp,Tp) -> Int -> String -> Rep
repAddParDist1 errInfo (act,exp) idx header
    | length etps < 2 = error "Error in repAddPar1"
    | msgType == Just 3 && idx == 1 = Error  "Undesirable situation: Adding a () it the first operand of an infix application" ""
    -- Maybe need to call rgtMost parActualSrc in the following guard
    -- to avoid capturing too many cases
    | isNothing eit && paraStyle == Just 4 =
        Hint ("Add a parameter named x to the point-free function,\n"
               ++ "Add an ( before: " 
               ++ showITJust sit ++ errLoc sit ++
               ", and\nAdd x ) after that." ) header
    
    -- Maybe generalize this to infix operator with predecence less than 9
    | isNothing eit && containsDollar = AddParIT sit (fromJust eitDollar) header
        
    -- | isNothing eit = error (subTreeStr 0 (getFB offender))
    | isNothing eit = Error "repAddParDist1: unable to handle it" ""

    | otherwise = 
          let retps = reverse etps
              newarg = head retps
              newfunc = head . tail $ retps
              restetps = reverse . drop 2 $ retps
              uniRes1 = mgu newfunc (newarg .->. lgTv1)
             -- in error $ show newfunc ++ "\t" ++ show (newarg .->. lgTv1)

              rep = AddParIT sit (fromJust eit) header
          in case uniRes1 of
              Left _ -> rep `Seq` Hint "But need to make some change first" ""
              Right unier1 -> 
               case mgu act (foldr (.->.) etp (restetps ++ [unier1 |-> lgTv1])) of
                    Right _ -> rep 
                    Left _ -> rep `Seq` Hint "And need some further changes" ""
    
    where (etps,etp) = functionSpineOfLength (idx+1) exp
          msgType = getFeature1 errInfo -- 3 is infix application
          paraStyle = getFeature16 errInfo -- 4 is point-free

          offender = (offendingInfoTree . snd) errInfo
          parActualSrc = (parent . offendingInfoTree . snd) errInfo          
          
          
          astStr = showIT . siblingAtAsIT 1 . fromJust $ parActualSrc
          containsDollar = isJust parActualSrc &&
                           isJust (siblingAtAsIT 1 (fromJust parActualSrc)) &&
                           isJust (siblingAtAsIT 2 (fromJust parActualSrc)) &&
                           '$' `elem` astStr
          eitDollar = siblingAtAsIT 2 (fromJust parActualSrc)

          
          
          sidx = if msgType == Just 3 && idx == 2 then 1 else idx
          
          emsg1 = "repAddParDist1: sit: no sibling at: " ++ show idx
          sit = maybe (error emsg1) id (siblingAtAsIT sidx offender)
            
          eit = siblingAtAsIT (sidx+1) offender

{-
Example: 
Act: [v344] -> [v344]
Exp: (v285 -> Int -> v285) -> v285 -> Int -> v285
I  : 1

1st, break the Exp with functionSpineOfLength at Int, yields
([(v285 -> Int -> v285), v285, Int], v285). 

2nd, unify 
    v285 -> Int -> v285
with
    v285 -> lgTv1
and lgTv1 willbe Int -> v285   
   
3rd, unify 
    Int -> v285 
with
    Int -> lgTv2
and lgTv2 will be v285

4th, build a function type from v285 (result from unification) 
as argument type and v285 (result from functionSpineOfLength)

5th unify Act type with v285 -> v285
-} 
repAddParDist2 :: ErrorInfo -> (Tp,Tp) -> Int -> String -> Rep
repAddParDist2 errInfo (act,exp) idx header
    | length etps < 3 = error "Error in repAddPar2: not enough type arity"
    | msgType == Just 3 && idx == 1 = Error  "Undesirable situation: Adding a () it the first operand of an infix application" ""
    -- Maybe need to call rgtMost parActualSrc in the following guard
    -- to avoid capturing too many cases
    -- Maybe need to check if two parameters are omitted
    | isNothing eit && paraStyle == Just 4 =
        Hint ("Add a parameter named x to the point-free function,\n"
               ++ "Add an ( before: " 
               ++ showITJust sit ++ errLoc sit ++
               ", and\nAdd x ) to the end." ) header
    
    -- Maybe generalize this to infix operator with predecence less than 9
    | isNothing eit && containsDollar && 
      isNothing eitDollar && paraStyle == Just 4 =
        Hint ("Add a parameter named x to the point-free function,\n"
               ++ "Add an ( before: " 
               ++ showITJust sit ++ errLoc sit ++
               ", and\nAdd x ) to the end." ) header

    -- Maybe generalize this to infix operator with predecence less than 9
    | isNothing eit && containsDollar = AddParIT sit (fromJust eitDollar) header
        
    -- | isNothing eit = error (subTreeStr 0 (getFB offender))
    | isNothing eit = Error "repAddParDist1: unable to handle it" ""

    | otherwise = 
          let retps = reverse etps
              newarg2 = head retps
              newarg1 = head . tail $ retps
              newfunc = head . tail . tail $ retps
              restetps = reverse . drop 3 $ retps
              uniRes1 = mgu newfunc (newarg1 .->. newarg2 .->. lgTv1)
             -- in error $ show newfunc ++ "\t" ++ show (newarg .->. lgTv1)

              rep = AddParIT sit (fromJust eit) header
          in case uniRes1 of
              Left _ -> rep `Seq` Hint "But need to make some change first" ""
              Right unier1 -> 
               if unifiable noOrderedTypeSynonyms  act (foldr (.->.) etp (restetps ++ [unier1 |-> lgTv1])) 
               then rep 
               else rep `Seq` Hint "And need some further changes" ""
    
    where (etps,etp) = functionSpineOfLength (idx+2) exp
          msgType = getFeature1 errInfo -- 3 is infix application
          paraStyle = getFeature16 errInfo -- 4 is point-free

          offender = (offendingInfoTree . snd) errInfo
          parActualSrc = (parent . offendingInfoTree . snd) errInfo          
          
          
          astStr = showIT . siblingAtAsIT 1 . fromJust $ parActualSrc
          containsDollar = isJust parActualSrc &&
                           isJust (siblingAtAsIT 1 (fromJust parActualSrc)) &&
                           isJust (siblingAtAsIT 2 (fromJust parActualSrc)) &&
                           '$' `elem` astStr
          eitDollar = siblingAtAsIT 3 (fromJust parActualSrc)

          
          
          sidx = if msgType == Just 3 && idx == 2 then 1 else idx
          
          emsg1 = "repAddParDist1: sit: no sibling at: " ++ show idx
          sit = maybe (error emsg1) id (siblingAtAsIT sidx offender)
            
          eit = siblingAtAsIT (sidx+2) offender

{-
The old version of repAddParDist2

repAddParDist2 :: ErrorInfo -> (Tp,Tp) -> Int -> String -> Rep
repAddParDist2 errInfo (act,exp) i header =                 
    let (etps,etp) = functionSpineOfLength (i+2) exp in
      if length etps < 3 then (error . ("In repAddParDist2: Noe enough etps: " ++) . show) etps  else
        let retps = reverse etps
            newarg2 = head retps
            newarg1 = head . tail $ retps
            newfunc = head . tail . tail $ retps
            restetps = reverse . drop 3 $ retps
            
            offender = (offendingInfoTree . snd) errInfo
            
            emsg1 = "repAddParDist2: sit: no sibling at: " ++ show i
            sit = maybe (error emsg1) id (siblingAtAsIT i offender)
            
            emsg2 = "repAddParDist2: eit: no sibling at: " ++ show (i+2)
            eit = maybe (error emsg2) id (siblingAtAsIT (i+2) offender)
                
            rep = AddParIT sit eit header
            
            uniRes1 = mgu newfunc (newarg1 .->. newarg2 .->. lgTv1)
         -- in error $ show newfunc ++ "\t" ++ show (newarg .->. lgTv1)
        in case uniRes1 of
              Left _ -> rep `Seq` Hint "But need to make some change first" ""
              Right unier1 -> 
                case mgu act (foldr (.->.) etp (restetps ++ [unier1 |-> lgTv1])) of
                    Right _ -> rep
                    Left _ -> rep `Seq` Hint "And need some further changes" ""
-}
                                                                 
repPullArg errInf idx feat1 numArgs header
    | feat1 == Just 3 = 
        case (adds,adde) of
            (Just s, Just t) -> AddParIT s t header
            (Just s, Nothing) -> error  "repPullArg invalid index: 1"
            (_ , Just t) ->  error "repPullArg invalid index: -1"
            _ ->  error "repPullArg invalid index: -1 and 1"
        `Seq`
        case pullinfix of 
            Just pinf -> Pull pinf numArgs ""
            _ -> error "repPullArg invalid pullinfix index: 1"
    | feat1 == Just 1 = 
        case pullprefix of 
            Just pinf -> Pull pinf numArgs header
            _ -> error $ "repPullArg invalid pullprefix index: " ++ show idx
    | otherwise = None
 -- | otherwise = error $ "Unhandled case in repPullArg, feat1: " ++ show feat1
    where offender = offendingInfoTree (snd errInf)
          adds = siblingAtAsIT (-1) offender
          adde = siblingAtAsIT 1 offender
          pullinfix = siblingAtAsIT 1 offender
          pullprefix = siblingAtAsIT idx offender 

repBrac sub combinedEnv localEnv importEnv errInfo feat isMulti
    -- It seems that when we have other changes, suggesting 
    -- adding or removing [] is not working well
   
    -- | not (null funcs) || topDiff /= 0 = None
    
    -- If we have both func diff and bracket diff at the same location
    -- Then we don't do anything.
    | not (null brackets) && 
      fstBracIdx `elem` map fst (funcs ++ likelyDiff)
       = None
       
    | otherwise = repBracLev1 sub brackets topBrac errInfo combinedEnv localEnv importEnv feat isMulti
    where TypeDiff brackets funcs topDiff likelyDiff topBrac = getFeatures5To9 errInfo combinedEnv
          fstBracIdx = fst . head $ brackets

    
-- repBrac' :: [(Int,Int)] -> ErrorInfo -> Rep
repBracLev1 sub [] 0 errInfo combinedEnv localEnv importEnv _ _ = None
repBracLev1 sub [] topBrac errInfo combinedEnv localEnv importEnv feat isMulti
    -- if the reported location is the explicitly-typed binding
    {-
    When Changing the retrn type of type annotations, we have to flip the 
    result of bracket differences
    -}
    | msgTyp == Just 12 && not isMulti && paraStyle /= Just 4
                        && excerpt /= 5 && excerpt /=6 = 
                     if topBrac > 0
                     then Hint ("Remove " ++ show topBrac ++" [] from the return type of annotation") 
                     (generateHeader symSq strRm locAnnot pairedNo likelyNo "repBracLev1" topBrac)
                     else Hint ("Add " ++ show (-topBrac) ++" [] to the return type of annotation")
                     (generateHeader symSq strAdd locAnnot pairedNo likelyNo "repBracLev1" (-topBrac))
    where msgTyp = getFeature1 errInfo
          (excerpt, _) = getFeature11And17 combinedEnv localEnv importEnv errInfo
          paraStyle = feat16 feat
          
repBracLev1 sub [] brac errInfo combinedEnv localEnv importEnv _ _ = Error "No suggestion for top brac diff only" ""
    
-- repBracLev1 ((i,diff):_) _ errInfo combinedEnv localEnv importEnv
repBracLev1 sub ((i,diff):[]) _ errInfo combinedEnv localEnv importEnv feat isMulti
    -- if the reported location is the RHS
    {-
    When Changing the retrn type of type annotations, we have to flip the 
    result of bracket differences
    -}
    | msgTyp == Just 5 && not isMulti && paraStyle /= Just 4
                       && excerpt /= 5 && excerpt /=6 = 
        -- error $ show excerpt
                     if diff > 0
                     then Hint ("Remove " ++ show diff ++" [] from the return type of annotation")
                     (generateHeader symSq strRm locAnnot pairedNo likelyNo "repBracLev1" diff)
                     else Hint ("Add " ++ show (-diff) ++" [] to the return type of annotation")
                     (generateHeader symSq strAdd locAnnot pairedNo likelyNo "repBracLev1" (-diff))
    
    | msgTyp == Just 13    -- Type error in pattern
      || msgTyp == Just 14 -- Type error in infix pattern
      || msgTyp == Just 15 -- Type error in element of list
      = None           -- Then *don't* suggest a change
      
    -- | getFeature2Unparsed errInfo == (Just "unification would give infinite type") = Hint "No bracket diff suggestion for infinite type" ""
      
    -- The case that Helium suggests an expression change and 
    -- the error is at (:) or (++)
    -- Todo: further check if the value of the excerpt is (:) or (++)
    | feature2 == 2 && excerpt == 4 =
            Hint "Use Helium's suggestion" ""
    -- if the offender is an argument 
    -- if the offender is a constant
    -- Since feature extraction still contains bugs when by
    -- identifying where bound functions as arguments, here
    -- we need to decide whether the offender is actually a
    -- let/where bound function
    
    -- if the offender is a nullary function
    |  excerpt == 6 && feature2 == 2 = 
                     if diff > 0  
                     then Hint "Constants: Use Helium's Suggestion"
                          (generateHeader symSq strAdd locBody pairedNo likelyNo "repBracLev1" diff)
                     else Hint "Constants: Use Helium's Suggestion"
                          (generateHeader symSq strRm locBody pairedNo likelyNo "repBracLev1" (-diff))
                          
    | excerpt == 6 = 
                     if diff > 0  
                     then AddBrac actualSrc diff 
                          (generateHeader symSq strAdd locBody pairedNo likelyNo "repBracLev1:Constants:NoSuggestion" diff)
                          `Seq`
                          repBracConArgAdd sub combinedEnv actualSrc "Concrete:Constants:NotHelium:repBracConArgAdd"
                     else RmBrac actualSrc (-diff)
                          (generateHeader symSq strRm locBody pairedNo likelyNo "repBracLev1:Constants:NoSuggestion" (-diff))
                          `Seq`
                          repBracConArgRm sub combinedEnv actualSrc "Concrete:Constants:NotHelium:repBracConArgRm"
                          
    | excerpt == 5  && not isLetWhereFunc =
                     if diff > 0  
                     then AddBrac actualSrc diff 
                          (generateHeader symSq strAdd locBody pairedNo likelyNo "repBracLev1:argument" diff)
                          `Seq`
                          repBracConArgAdd sub combinedEnv actualSrc "Concrete:Arguments:AddBar"
                     else RmBrac actualSrc (-diff)
                          (generateHeader symSq strRm locBody pairedNo likelyNo "repBracLev1:argument" (-diff))
                          `Seq`
                          repBracConArgRm sub combinedEnv actualSrc "Concrete:Arguments:RmBar"

                    
                          
    | nodeType == 2 && (funcOp == 1 || funcOp == 2) = 
                     (
                     if diff >0  
                     then AddBrac actualSrc diff 
                          (generateHeader symSq strAdd locBody pairedNo likelyNo "repBracLev1:Exp" diff)
                     else RmBrac actualSrc (-diff)
                          (generateHeader symSq strRm locBody pairedNo likelyNo "repBracLev1:Exp" (-diff))
                     )
                    `Seq`
                    repBracConExp sub combinedEnv actualSrc diff act  funcOp "Concrete:Lev1:Exp"
                    
                    
    | actArity == 1 = 
                     (
                     if diff > 0  
                     then AddBrac actualSrc diff 
                          (generateHeader symSq strAdd locBody pairedNo likelyNo "repBracLev1:Arity" diff)
                     else RmBrac actualSrc (-diff)
                          (generateHeader symSq strRm locBody pairedNo likelyNo "repBracLev1:Arity" (-diff))
                     )
                     `Seq`
                     repArityCon combinedEnv localEnv importEnv actualSrc 
                    
    
    -- 
    -- if the offender is a leaf and the leaf is a function
    -- or operator
    | nodeType == 3 && ( msgTyp == Just 1 || msgTyp == Just 3) = 
        -- error $ show (i,diff)
        repBracLev2 sub (i,diff) (fromJust msgTyp) actualSrc combinedEnv localEnv importEnv act
    -- | otherwise = None
    | otherwise = repRegroup sub combinedEnv actualSrc 
    where (excerpt,funcOp) = (feat11 feat, feat17 feat)
    -- getFeature11And17 combinedEnv localEnv importEnv errInfo
          nodeType = getFeature10 errInfo
          msgTyp = getFeature1 errInfo
          paraStyle = getFeature16 errInfo
          actualSrc = offendingInfoTree $ snd errInfo
          feature2 = getFeature2 errInfo
          
          tpss = getFeatures3And4 errInfo
          (act,_) = tpScToTp combinedEnv tpss
          
          actArity = (length . spines) act
          
          isLetWhereFunc = showITJust actualSrc `elem` 
                           sameLevLetWhereFunc actualSrc

          
-- If we have multiple bracket changes, we just don't
-- generate a suggestion

repBracLev1 sub ((i,diff):_) _ errInfo combinedEnv localEnv importEnv feat isMulti = None

                           
-- repBracLev2 :: (Int,Int) -> Int -> InfoTree -> Rep
repBracLev2 sub (i,diff) msgTyp opIT combinedEnv localEnv importEnv act
    | excerpt == 6 && msgTyp == 2 =
        if diff >0  
            then Hint "Constants: Use Helium's Suggestion" 
                (generateHeader symSq strAdd locBody pairedNo likelyNo "repBracLev2" diff)
            else Hint "Constants: Use Helium's Suggestion"
                (generateHeader symSq strRm locBody pairedNo likelyNo "repBracLev2" (-diff))
        
    | excerpt == 6 =
        if diff >0  
            then AddBrac actIT diff 
                (generateHeader symSq strAdd locBody pairedNo likelyNo "repBracLev2:Constants:NoSuggestion" diff)
                `Seq`
                repBracConArgAdd sub combinedEnv actIT "Concrete:Constants:NotHelium:repBracConArgAdd"
            else RmBrac actIT (-diff)
                (generateHeader symSq strRm locBody pairedNo likelyNo "repBracLev2:Constants:NoSuggestion" (-diff))
                `Seq`
                repBracConArgRm sub combinedEnv actIT "Concrete:Constants:NotHelium:repBracConArgRm"
    
    | excerpt == 5 && not isLetWhereFunc = 
        case diff >0 of
            True -> AddBrac actIT diff 
                (generateHeader symSq strAdd locBody pairedNo likelyNo "repBracLev2:Argument" diff)
                `Seq`
                repBracConArgAdd sub combinedEnv actIT "Concrete:Arguments:AddBar"
            False -> RmBrac actIT (-diff)
                (generateHeader symSq strRm locBody pairedNo likelyNo "repBracLev2:Argument" (-diff))
                `Seq`
                repBracConArgRm sub combinedEnv actIT "Concrete:Arguments:RmBar"
            
            -- Don't try to remove [] at []. This won't work.
            -- _ -> None
                
    | funcOp == 1 || funcOp == 2 =
                 (
                 if diff >0  
                 then AddBrac actIT diff 
                      (generateHeader symSq strAdd locBody pairedNo likelyNo "repBracLev2:Exp" diff)
                 else RmBrac actIT (-diff)
                      (generateHeader symSq strRm locBody pairedNo likelyNo "repBracLev2:Exp" (-diff))
                 )
                 `Seq`
                 repBracConExp sub combinedEnv actIT diff (spines act !! (i-1) ) funcOp "Concrete:Lev2:Exp"
    
    | otherwise = None
    where idx = case (msgTyp,i) of
                (3,1) -> -1
                (3,2) -> 1
                (1,i) -> i
          errmsg = "repBraLev2: no sibling at: " ++ show msgTyp
          -- errmsg = "repBraLev2: no sibling at: " ++ show i
          actIT = maybe (error errmsg) id (siblingAtAsIT idx opIT)
          (excerpt,funcOp) = feature11And17ForIT combinedEnv localEnv importEnv actIT
          isLetWhereFunc = showITJust actIT `elem` 
                           sameLevLetWhereFunc actIT
                                     
    -- in error $ show (excerpt,funcOp)
    -- in error $ show (msgTyp,i,idx,diff) ++ showIT (Just actIT) ++ show (excerpt,funcOp)
        
repSig topLevelTypes subs combinedEnv localEnv importEnv errInfo =
    let tpss = getFeatures3And4 errInfo
        (act,exp) = tpScToTp combinedEnv tpss
        TypeDiff brackets funcs topDiff lkFuncs topBra= getFeatures5To9 errInfo combinedEnv
    in 

    case (topDiff, funcs) of
        (1, (i,1):_) -> repAddParDist1 errInfo (act,exp) i
                        (generateHeader symPar strAdd locBody pairedNo likelyNo "repSig" 1)
        (2, (i,1):_) -> repAddPar2 errInfo (act,exp) i
                        (generateHeader symPar strAdd locBody pairedNo likelyNo "repSig" 2)
        (2, (i,2):_) -> repAddParDist2 errInfo (act,exp) i
                        (generateHeader symPar strAdd locBody pairedNo likelyNo "repSig" 2)
        
        (-1,(i,-1):_) -> repPullArg errInfo i (getFeature1 errInfo) 1
                         (generateHeader symPar strRm locBody pairedNo likelyNo "repSig" 1)

        (-2,(i,-2):_) -> repPullArg errInfo i (getFeature1 errInfo) 2
                         (generateHeader symPar strRm locBody pairedNo likelyNo "repSig" 2)

        -- In rest cases, we try to make use of likelyFuncs. This is done
        -- in the function repSigLessLikely. 
        _             -> None
                
repPaired topLevelTypes subs combinedEnv localEnv importEnv eI1 eI2 =
    let (act1,exp1) = tpScToTp combinedEnv . getFeatures3And4 $ eI1
        (act2,exp2) = tpScToTp combinedEnv . getFeatures3And4 $ eI1
        features1 = getFeatures5To9 eI1 combinedEnv
        td1 = topLevelDiff features1
        features2 = getFeatures5To9 eI2 combinedEnv
        td2 = topLevelDiff features2
        reportChild  = usesOtherSource . snd $ eI2
        
        puller = if td1 == -1 && td2 == 2 then PullAddPar 
                 else Pull
    in
    -- error $ show features1 ++ "\t" ++ show features2
    if isComp features1 features2 && closeEnough eI1 eI2 then
        let offender1 = offendingInfoTree . snd $ eI1
            offender2 = offendingInfoTree . snd $ eI2
            feat1 = getFeature1 eI1
            (excerpt,_) = getFeature11And17 combinedEnv localEnv importEnv eI1
        -- in error $ show feat1 ++ "\t" ++ show (sameGen eI1 eI2)
        -- If it's an infix application and 
        -- the excerpt is not an expression 
        in if feat1 == Just 3 && excerpt /= 1 then
                AddParIT (fromJust $ siblingAtAsIT (-1) offender1) offender2
                (generateHeader symPar strRm locBody pairedYes likelyNo "repPaired" 1)
                `Seq`
                puller offender2 td2 ""
           else if sameGen eI1 eI2 then
                -- error $ show (numRgtSiblings offender1)
                AddParIT (fromJust $ siblingAtAsIT (numRgtSiblings offender1) offender1) (fromJust . parent $ offender2)
                (generateHeader symPar strRm locBody pairedYes likelyNo "repPaired" 1)
                `Seq`
                puller (fromJust . parent $ offender2) td2 ""
           else
                -- error $ show offender2
                -- (error . show . fromJust . parent)  offender2
                -- error $ show reportChild
                if reportChild 
                then puller offender2 td2
                     (generateHeader symPar strRm locBody pairedYes likelyNo "repPaired" 1)
                else puller (fromJust . parent $ offender2) td2
                     (generateHeader symPar strRm locBody pairedYes likelyNo "repPaired" 1)
    else 
        None

repTopTypMinusOne combinedEnv localEnv importEnv errInfo
    -- if the reported location is the RHS
    -- Example: Code-39-17
    | msgTyp == Just 12 = 
        Hint "Remove a parameter"
             (generateHeader symParam strRm locBody pairedNo likelyNo "repTopTypMinusOne" 1)
        
    -- If the function is point-free and the topLevel diff is -1
    -- Example: Database/group119 25||27||30||52
    | paraStyle == Just 4 && rgtMost parActualSrc = 
        Hint ("Add a parameter to point-free function, and add" 
               ++ " the parameter after *" ++ showITJust parActualSrc ++"*")
             (generateHeader symParam strAdd locBody pairedNo likelyNo "repTopTypMinusOne" 1)
    | otherwise = None
    where (excerpt,funcOp) = getFeature11And17 combinedEnv localEnv importEnv errInfo
          nodeType = getFeature10 errInfo
          msgTyp = getFeature1 errInfo
          paraStyle = getFeature16 errInfo
          parActualSrc = (fromJust . parent . offendingInfoTree . snd) errInfo
          feature2 = getFeature2 errInfo
        
repSigLessLikely combinedEnv errInfo =
    let tpss = getFeatures3And4 errInfo
        (act,exp) = tpScToTp combinedEnv tpss
        TypeDiff brackets funcDiff topDiff lkFuncs topBra= getFeatures5To9 errInfo combinedEnv
        (lastIdx, lastDiff) = last lkFuncs
        shortLen = min ((length . spines) exp) ((length . spines) act)
        newTopDiff = if lastIdx == shortLen && topDiff ==0 
                     then lastDiff
                     else topDiff
        newLkFuncs = if lastIdx == shortLen
                     then init lkFuncs
                     else lkFuncs
        newFuncDiff = if null funcDiff then newLkFuncs else funcDiff
    in if null lkFuncs then None
       else case (newTopDiff, newFuncDiff) of
        (1, (i,1):_) -> repAddParDist1 errInfo (act,exp) i
                        (generateHeader symPar strAdd locBody pairedNo likelyYes "repSigLessLikely" 1)
        (2, (i,1):_) -> repAddPar2 errInfo (act,exp) i
                        (generateHeader symPar strAdd locBody pairedNo likelyYes "repSigLessLikely" 2)
        (2, (i,2):_) -> repAddParDist2 errInfo (act,exp) i
                        (generateHeader symPar strAdd locBody pairedNo likelyYes "repSigLessLikely" 2)
        
        (-1,(i,-1):_) -> repPullArg errInfo i (getFeature1 errInfo) 1
                        (generateHeader symPar strRm locBody pairedNo likelyYes "repSigLessLikely" 1)
        (-2,(i,-2):_) -> repPullArg errInfo i (getFeature1 errInfo) 2
                        (generateHeader symPar strRm locBody pairedNo likelyYes "repSigLessLikely" 2)

        _               -> None


repSigLessCertain topLevelTypes subs combinedEnv localEnv importEnv errInfo feat isMulti
    | (not . isNone) useLikelyFunc = useLikelyFunc
    -- | (not . isNone) reGroupRR = reGroupRR
    | (not . isNone) bracSug = bracSug
    | otherwise = 
        case (topDiff, funcs, lkFuncs) of
                 -- Suggest remove an argument in general worked badly
                 (1,[],[])  -> Hint "Remove an argument"        
                               (generateHeader symArg strRm locBody pairedNo likelyNo "repSigLessCertain" 1)
                               
                 (-1,[],[]) -> repTopTypMinusOne combinedEnv localEnv importEnv errInfo        
                 _          -> Hint ("No suggestion: \n" ++ show (TypeDiff brackets funcs topDiff lkFuncs topBra)) ""
    where useLikelyFunc = repSigLessLikely combinedEnv errInfo
          bracSug = repBrac subs combinedEnv localEnv importEnv errInfo feat isMulti
        -- tpss = getFeatures3And4 errInfo
        -- (act,exp) = tpScToTp combinedEnv tpss
          TypeDiff brackets funcs topDiff lkFuncs topBra = getFeatures5To9 errInfo combinedEnv
          -- reGroupRR = repRegroup subs combinedEnv (offendingInfoTree. snd $ errInfo)
       
repExp topLevelTypes subs combinedEnv localEnv importEnv _ [] _ _= return []
repExp topLevelTypes subs combinedEnv localEnv importEnv idx (eri1:eris)  (feat1:feats) isMulti
    | (not . null) eris && shareDollarSimple eri1 (head eris) = do
        let rmDollar = Hint ("Remove $ in " ++ firstComPredIT eri1 (head eris               )) (generateHeader symDollar strRm locBody pairedNo               likelyNo "repExp" 1)
            dollarLine = lineFromErrInfo eri1
            num = length . takeWhile ((== dollarLine) . lineFromErrInfo) $ eris
        rests2 <- repExp topLevelTypes subs combinedEnv localEnv importEnv (idx+num+1) (drop num eris) feats isMulti
        return ( zip [idx..idx+num] (repeat rmDollar) ++ rests2 )

                
    | (not.isNone) repS = do
        -- if debug then putStrLn $ "Suggestions----------\n" ++ interpret1 repS ++ "\n" else putStr ""
        rests <- repExp topLevelTypes subs combinedEnv localEnv importEnv (idx+1) eris feats isMulti
        return ((idx,repS):rests)
        
    | (not . null) eris && (not.isNone) repP = do
        -- if debug then putStrLn $ "Suggestions----------\n" ++ interpret1 repP ++ "\n" else putStr ""
        rests2 <- repExp topLevelTypes subs combinedEnv localEnv importEnv (idx+2) (tail eris) (tail feats) isMulti
        return ((idx,repP) : (idx+1,repP) : rests2)
        
    | otherwise = do
            -- if debug then putStrLn $ interpret1 repI else putStr ""
            rest <- repExp topLevelTypes subs combinedEnv localEnv importEnv (idx+1) eris feats isMulti
            return ((idx,repI):rest)
        
    
    where repS = repSig topLevelTypes subs combinedEnv localEnv importEnv eri1
          
          repP = repPaired topLevelTypes subs combinedEnv localEnv importEnv eri1 (head eris)
          
          repI = repSigLessCertain topLevelTypes subs combinedEnv localEnv importEnv eri1 feat1 isMulti   

    
