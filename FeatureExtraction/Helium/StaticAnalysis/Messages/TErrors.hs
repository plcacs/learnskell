module Helium.StaticAnalysis.Messages.TErrors where

import Helium.StaticAnalysis.Messages.Messages
import Top.Types

import Data.List       (union, partition)
import Helium.Syntax.UHA_Syntax 
import Helium.Syntax.UHA_Range  
import Helium.StaticAnalysis.Miscellaneous.UHA_Source
-- import Helium.StaticAnalysis.Messages.TypeErrors
import Helium.Main.Args (Option(..))
import Top.Ordering.Tree
import Helium.StaticAnalysis.Miscellaneous.DoublyLinkedTree
import Helium.StaticAnalysis.Miscellaneous.TypeConstraints
import Top.Constraint.Information
import Top.Implementation.Overloading
import Top.Interface.Basic (ErrorLabel)
import Top.Interface.Substitution (unificationErrorLabel)
import Top.Interface.TypeInference
import Helium.Utils.Utils 
import Data.Maybe
import Data.Function


type TypeErrors = [(TypeError,ConstraintInfo)]

data TypeError  = TypeError
                     [Range]                                -- range(s) of the error
                     [MessageLine]                          -- oneliner messages
                     [(Bool, MessageBlock, MessageBlock)]   -- Hugs-like table
                     [TypeErrorHint]                        -- extra hints
     
type TypeErrorHint  = (String, MessageBlock)

data ConstraintInfo =
   CInfo_ { location      :: String
          , sources       :: (UHA_Source, Maybe UHA_Source{- term -})
          , localInfo     :: InfoTree
          , properties    :: Properties
          , errorMessage  :: Maybe TypeError
          , otherSource :: Maybe UHA_Source
          , offenderTree :: Maybe InfoTree
          , usesOtherSource :: Bool
          }
          
type InfoTrees = [InfoTree]
type InfoTree = DoublyLinkedTree LocalInfo
                            
data LocalInfo = 
     LocalInfo { self           :: UHA_Source  
               , assignedType   :: Maybe Tp
               , monos          :: Tps
               }
               deriving Show


type Properties = [Property]
data Property   
   = FolkloreConstraint
   | ConstraintPhaseNumber Int
   | HasTrustFactor Float
   | FuntionBindingEdge Int{-number of patterns-}
   | InstantiatedTypeScheme TpScheme
   | SkolemizedTypeScheme (Tps, TpScheme)
   | IsUserConstraint Int{-user-constraint-group unique number-} Int{-constraint number within group-}
   | WithHint (String, MessageBlock)
   | ReductionErrorInfo Predicate
   | FromBindingGroup 
   | IsImported Name 
   | ApplicationEdge Bool{-is binary-} [LocalInfo]{-info about terms-}
   | ExplicitTypedBinding -- superfluous?
   | ExplicitTypedDefinition Tps{- monos-} Name{- function name -}
   | Unifier Int{-type variable-} (String{-location-}, LocalInfo, String{-description-})
   | EscapedSkolems [Int]
   | PredicateArisingFrom (Predicate, ConstraintInfo)
   | TypeSignatureLocation Range
   | TypePair (Tp, Tp)

emptyConstraintInfo :: ConstraintInfo
emptyConstraintInfo =
   CInfo_ { location   = "Typing Strategy"
          , sources    = (UHA_Decls [], Nothing)
          , localInfo  = root (LocalInfo (UHA_Decls []) Nothing []) []
          , properties = []
          , errorMessage = Nothing
          , otherSource = Nothing
          , offenderTree = Nothing
          , usesOtherSource = False
          }
