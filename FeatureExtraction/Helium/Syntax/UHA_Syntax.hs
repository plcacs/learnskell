

-- UUAGC 0.9.42.2 (Helium/Syntax/UHA_Syntax.ag)
module Helium.Syntax.UHA_Syntax where

import Helium.Utils.Utils(internalError)
import Data.Maybe(isJust)

-- Alternative -------------------------------------------------
data Alternative = Alternative_Hole (Range) (Integer)
                 | Alternative_Feedback (Range) (String) (Alternative)
                 | Alternative_Alternative (Range) (Pattern) (RightHandSide)
                 | Alternative_Empty (Range)
                 deriving (Eq, Show)
                 
-- Alternatives ------------------------------------------------
type Alternatives = [Alternative]
-- AnnotatedType -----------------------------------------------
data AnnotatedType = AnnotatedType_AnnotatedType (Range) (Bool) (Type) deriving (Eq, Show)
-- AnnotatedTypes ----------------------------------------------
type AnnotatedTypes = [AnnotatedType]
-- Body --------------------------------------------------------
data Body = Body_Hole (Range) (Integer)
          | Body_Body (Range) (ImportDeclarations) (Declarations)
-- Constructor -------------------------------------------------
data Constructor = Constructor_Constructor (Range) (Name) (AnnotatedTypes)
                 | Constructor_Infix (Range) (AnnotatedType) (Name) (AnnotatedType)
                 | Constructor_Record (Range) (Name) (FieldDeclarations)
                 deriving (Eq, Show)
                 
-- Constructors ------------------------------------------------
type Constructors = [Constructor]
-- ContextItem -------------------------------------------------
data ContextItem = ContextItem_ContextItem (Range) (Name) (Types) deriving (Eq, Show)
-- ContextItems ------------------------------------------------
type ContextItems = [ContextItem]
-- Declaration -------------------------------------------------
data Declaration = Declaration_Hole (Range) (Integer)
                 | Declaration_Type (Range) (SimpleType) (Type)
                 | Declaration_Data (Range) (ContextItems) (SimpleType) (Constructors) (Names)
                 | Declaration_Newtype (Range) (ContextItems) (SimpleType) (Constructor) (Names)
                 | Declaration_Class (Range) (ContextItems) (SimpleType) (MaybeDeclarations)
                 | Declaration_Instance (Range) (ContextItems) (Name) (Types) (MaybeDeclarations)
                 | Declaration_Default (Range) (Types)
                 | Declaration_FunctionBindings (Range) (FunctionBindings)
                 | Declaration_PatternBinding (Range) (Pattern) (RightHandSide)
                 | Declaration_TypeSignature (Range) (Names) (Type)
                 | Declaration_Fixity (Range) (Fixity) (MaybeInt) (Names)
                 | Declaration_Empty (Range)
                 deriving (Eq, Show)
                 
-- Declarations ------------------------------------------------
type Declarations = [Declaration]
-- Export ------------------------------------------------------
data Export = Export_Variable (Range) (Name)
            | Export_TypeOrClass (Range) (Name) (MaybeNames)
            | Export_TypeOrClassComplete (Range) (Name)
            | Export_Module (Range) (Name)
-- Exports -----------------------------------------------------
type Exports = [Export]
-- Expression --------------------------------------------------
data Expression = Expression_Hole (Range) (Integer)
                | Expression_Feedback (Range) (String) (Expression)
                | Expression_MustUse (Range) (Expression)
                | Expression_Literal (Range) (Literal)
                | Expression_Variable (Range) (Name)
                | Expression_Constructor (Range) (Name)
                | Expression_Parenthesized (Range) (Expression)
                | Expression_NormalApplication (Range) (Expression) (Expressions)
                | Expression_InfixApplication (Range) (MaybeExpression) (Expression) (MaybeExpression)
                | Expression_If (Range) (Expression) (Expression) (Expression)
                | Expression_Lambda (Range) (Patterns) (Expression)
                | Expression_Case (Range) (Expression) (Alternatives)
                | Expression_Let (Range) (Declarations) (Expression)
                | Expression_Do (Range) (Statements)
                | Expression_List (Range) (Expressions)
                | Expression_Tuple (Range) (Expressions)
                | Expression_Comprehension (Range) (Expression) (Qualifiers)
                | Expression_Typed (Range) (Expression) (Type)
                | Expression_RecordConstruction (Range) (Name) (RecordExpressionBindings)
                | Expression_RecordUpdate (Range) (Expression) (RecordExpressionBindings)
                | Expression_Enum (Range) (Expression) (MaybeExpression) (MaybeExpression)
                | Expression_Negate (Range) (Expression)
                | Expression_NegateFloat (Range) (Expression)
                deriving (Eq, Show)
                
-- Expressions -------------------------------------------------
type Expressions = [Expression]
-- FieldDeclaration --------------------------------------------
data FieldDeclaration = FieldDeclaration_FieldDeclaration (Range) (Names) (AnnotatedType)
                        deriving (Eq, Show)
                        
-- FieldDeclarations -------------------------------------------
type FieldDeclarations = [FieldDeclaration]
-- Fixity ------------------------------------------------------
data Fixity = Fixity_Infixl (Range)
            | Fixity_Infixr (Range)
            | Fixity_Infix (Range)
            deriving (Eq, Show)
            
-- FunctionBinding ---------------------------------------------
data FunctionBinding = FunctionBinding_Hole (Range) (Integer)
                     | FunctionBinding_Feedback (Range) (String) (FunctionBinding)
                     | FunctionBinding_FunctionBinding (Range) (LeftHandSide) (RightHandSide)
                     deriving (Eq, Show)
-- FunctionBindings --------------------------------------------
type FunctionBindings = [FunctionBinding]
-- GuardedExpression -------------------------------------------
data GuardedExpression = GuardedExpression_GuardedExpression (Range) (Expression) (Expression)
                         deriving (Eq, Show)
                         
-- GuardedExpressions ------------------------------------------
type GuardedExpressions = [GuardedExpression]
-- Import ------------------------------------------------------
data Import = Import_Variable (Range) (Name)
            | Import_TypeOrClass (Range) (Name) (MaybeNames)
            | Import_TypeOrClassComplete (Range) (Name)
-- ImportDeclaration -------------------------------------------
data ImportDeclaration = ImportDeclaration_Import (Range) (Bool) (Name) (MaybeName) (MaybeImportSpecification)
                       | ImportDeclaration_Empty (Range)
-- ImportDeclarations ------------------------------------------
type ImportDeclarations = [ImportDeclaration]
-- ImportSpecification -----------------------------------------
data ImportSpecification = ImportSpecification_Import (Range) (Bool) (Imports)
-- Imports -----------------------------------------------------
type Imports = [Import]
-- LeftHandSide ------------------------------------------------
data LeftHandSide = LeftHandSide_Function (Range) (Name) (Patterns)
                  | LeftHandSide_Infix (Range) (Pattern) (Name) (Pattern)
                  | LeftHandSide_Parenthesized (Range) (LeftHandSide) (Patterns)
                  deriving (Eq,Show)
-- Literal -----------------------------------------------------
data Literal = Literal_Int (Range) (String)
             | Literal_Char (Range) (String)
             | Literal_Float (Range) (String)
             | Literal_String (Range) (String)
             deriving (Eq, Show)
-- MaybeDeclarations -------------------------------------------
data MaybeDeclarations = MaybeDeclarations_Nothing
                       | MaybeDeclarations_Just (Declarations)
                       deriving (Eq, Show)
                       
-- MaybeExports ------------------------------------------------
data MaybeExports = MaybeExports_Nothing
                  | MaybeExports_Just (Exports)
-- MaybeExpression ---------------------------------------------
data MaybeExpression = MaybeExpression_Nothing
                     | MaybeExpression_Just (Expression)
                     deriving (Eq, Show)
                     
-- MaybeImportSpecification ------------------------------------
data MaybeImportSpecification = MaybeImportSpecification_Nothing
                              | MaybeImportSpecification_Just (ImportSpecification)
-- MaybeInt ----------------------------------------------------
data MaybeInt = MaybeInt_Nothing
              | MaybeInt_Just (Int)
              deriving (Eq, Show)
              
-- MaybeName ---------------------------------------------------
data MaybeName = MaybeName_Nothing
               | MaybeName_Just (Name)
-- MaybeNames --------------------------------------------------
data MaybeNames = MaybeNames_Nothing
                | MaybeNames_Just (Names)
-- Module ------------------------------------------------------
data Module = Module_Module (Range) (MaybeName) (MaybeExports) (Body)
-- Name --------------------------------------------------------
data Name = Name_Identifier (Range) (Strings) (String)
          | Name_Operator (Range) (Strings) (String)
          | Name_Special (Range) (Strings) (String)

instance Eq Name where
   n1 == n2 = getNameName n1 == getNameName n2

instance Ord Name where
   n1 <= n2 = getNameName n1 <= getNameName n2

instance Show Name where 
    show = getNameName  
          
getNameName :: Name -> String -- !!!Name
getNameName (Name_Identifier _ _ name) = name
getNameName (Name_Operator   _ _ name) = name
getNameName (Name_Special    _ _ name) = name

          
-- Names -------------------------------------------------------
type Names = [Name]
-- Pattern -----------------------------------------------------
data Pattern = Pattern_Hole (Range) (Integer)
             | Pattern_Literal (Range) (Literal)
             | Pattern_Variable (Range) (Name)
             | Pattern_Constructor (Range) (Name) (Patterns)
             | Pattern_Parenthesized (Range) (Pattern)
             | Pattern_InfixConstructor (Range) (Pattern) (Name) (Pattern)
             | Pattern_List (Range) (Patterns)
             | Pattern_Tuple (Range) (Patterns)
             | Pattern_Record (Range) (Name) (RecordPatternBindings)
             | Pattern_Negate (Range) (Literal)
             | Pattern_As (Range) (Name) (Pattern)
             | Pattern_Wildcard (Range)
             | Pattern_Irrefutable (Range) (Pattern)
             | Pattern_Successor (Range) (Name) (Literal)
             | Pattern_NegateFloat (Range) (Literal)
             deriving (Eq, Show)
             
-- Patterns ----------------------------------------------------
type Patterns = [Pattern]
-- Position ----------------------------------------------------
data Position = Position_Position (String) (Int) (Int)
              | Position_Unknown
              deriving (Show)
              
instance Eq Position where
    Position_Position m1 l1 c1 == Position_Position m2 l2 c2 =
        m1 == m2 && l1 == l2 && c1 == c2
    Position_Unknown    == Position_Unknown    = True
    Position_Unknown    == Position_Position{} = False
    Position_Position{} == Position_Unknown    = False

-- Qualifier ---------------------------------------------------
data Qualifier = Qualifier_Guard (Range) (Expression)
               | Qualifier_Let (Range) (Declarations)
               | Qualifier_Generator (Range) (Pattern) (Expression)
               | Qualifier_Empty (Range)
               deriving (Eq, Show)
-- Qualifiers --------------------------------------------------
type Qualifiers = [Qualifier]
-- Range -------------------------------------------------------
data Range = Range_Range (Position) (Position)

instance Eq Range where
    Range_Range start1 stop1 == Range_Range start2 stop2 =
        start1 == start2 && stop1 == stop2

instance Show Range where
    show = showRange

-- !!!! In the special case that the range corresponds to the import range,
-- the module name of the second position should be printed
showRange :: Range -> String
showRange range@(Range_Range startPos endPos)
    | isImportRange range =
        moduleFromPosition endPos
    | otherwise =
        showPosition startPos

isImportRange :: Range -> Bool
isImportRange = isJust . modulesFromImportRange

modulesFromImportRange :: Range -> Maybe (String, String)
modulesFromImportRange
    (Range_Range
        (Position_Position importedIn   0 0)
        (Position_Position importedFrom 0 0)
    ) =
        Just (importedIn, importedFrom)
modulesFromImportRange _ = Nothing

                                        
moduleFromPosition :: Position -> String
moduleFromPosition pos =
    case pos of
        Position_Position moduleName _ _ -> 
            moduleName
        _ -> 
            internalError "UHA_Range" "moduleFromPosition" "unknown position"

showPosition :: Position -> String
showPosition (Position_Position _ line column) =
    "(" ++ show line ++ "," ++ show column ++ ")"
showPosition _ =
    "<unknown position>"
            
-- End of misuse functions
        
-- RecordExpressionBinding -------------------------------------
data RecordExpressionBinding = RecordExpressionBinding_RecordExpressionBinding (Range) (Name) (Expression)
                               deriving (Eq, Show)
                               
-- RecordExpressionBindings ------------------------------------
type RecordExpressionBindings = [RecordExpressionBinding]
-- RecordPatternBinding ----------------------------------------
data RecordPatternBinding = RecordPatternBinding_RecordPatternBinding (Range) (Name) (Pattern)
                            deriving (Eq, Show)
                            
-- RecordPatternBindings ---------------------------------------
type RecordPatternBindings = [RecordPatternBinding]
-- RightHandSide -----------------------------------------------
data RightHandSide = RightHandSide_Expression (Range) (Expression) (MaybeDeclarations)
                   | RightHandSide_Guarded (Range) (GuardedExpressions) (MaybeDeclarations)
                   deriving (Eq, Show)
                   
-- SimpleType --------------------------------------------------
data SimpleType = SimpleType_SimpleType (Range) (Name) (Names) deriving (Eq, Show)
-- Statement ---------------------------------------------------
data Statement = Statement_Expression (Range) (Expression)
               | Statement_Let (Range) (Declarations)
               | Statement_Generator (Range) (Pattern) (Expression)
               | Statement_Empty (Range)
               deriving (Eq, Show)
               
-- Statements --------------------------------------------------
type Statements = [Statement]
-- Strings -----------------------------------------------------
type Strings = [(String)]
-- Type --------------------------------------------------------
data Type = Type_Application (Range) (Bool) (Type) (Types)
          | Type_Variable (Range) (Name)
          | Type_Constructor (Range) (Name)
          | Type_Qualified (Range) (ContextItems) (Type)
          | Type_Forall (Range) (Names) (Type)
          | Type_Exists (Range) (Names) (Type)
          | Type_Parenthesized (Range) (Type)
          deriving (Eq, Show)
-- Types -------------------------------------------------------
type Types = [Type]