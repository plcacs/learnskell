{-| Module      :  PhaseTypeInferencer
    License     :  GPL

    Maintainer  :  helium@cs.uu.nl
    Stability   :  experimental
    Portability :  portable
-}

module Helium.Main.PhaseTypeInferencer (phaseTypeInferencer) where

import Helium.Main.CompileUtils
import Helium.StaticAnalysis.Messages.Warnings(Warning)
import Helium.StaticAnalysis.Inferencers.TypeInferencing(typeInferencing)
import Helium.ModuleSystem.DictionaryEnvironment (DictionaryEnvironment)
--import UHA_Syntax
import Helium.StaticAnalysis.Messages.TypeErrors
import Helium.StaticAnalysis.Messages.TErrors
import Helium.StaticAnalysis.Messages.Information (showInformation)
import System.FilePath.Posix
import Top.Types.Substitution

import Data.Map as M hiding (filter)

phaseTypeInferencer :: 
    String -> String -> Module -> ImportEnvironment -> ImportEnvironment -> [Option] -> 
    -- Phase (TypeError,ConstraintInfo) (DictionaryEnvironment, ImportEnvironment, TypeEnvironment, [Warning])
    IO (Either ([(TypeError,ConstraintInfo)],TypeEnvironment,FixpointSubstitution) (DictionaryEnvironment, ImportEnvironment, TypeEnvironment, [Warning]))
phaseTypeInferencer basedir fullName module_ localEnv completeEnv options = do
    enterNewPhase "Type inferencing" options

    -- 'W' and 'M' are predefined type inference algorithms
    let newOptions = (if AlgorithmW `elem` options
                        then filter (/= NoSpreading) . ([TreeWalkInorderTopLastPost, SolverGreedy]++) 
                        else id)
                   . (if AlgorithmM `elem` options
                        then filter (/= NoSpreading) . ([TreeWalkInorderTopFirstPre, SolverGreedy]++)  
                        else id)
                   $ options
                   
        (debugIO, dictionaryEnv, toplevelTypes, typeErrors, warnings, substitutions) =
            typeInferencing newOptions completeEnv module_
      
        -- add the top-level types (including the inferred types)
        finalEnv = addToTypeEnvironment toplevelTypes completeEnv
    
    -- putStrLn "\nTop-level type environment"
    -- putStrLn . show $ toplevelTypes        
    -- putStrLn "==========================="
    -- putStrLn "Full type environment"
    -- putStrLn . show $ finalEnv     
    -- putStrLn "Substitution"
    -- putStrLn "==========================="
    -- putStrLn . concatMap (\item -> show item ++ "\n") . M.assocs $ substitutions        
    
    when (DumpTypeDebug `elem` options) debugIO      
     
    -- display name information
    showInformation True options finalEnv
    
    case typeErrors of 
       
       _:_ ->
          do when (DumpInformationForAllModules `elem` options) $
                putStr (show completeEnv)
             return (Left (typeErrors,toplevelTypes,substitutions))
          
       [] -> 
          do -- Dump information
             when (DumpInformationForAllModules `elem` options) $ 
                print finalEnv
             when (HFullQualification `elem` options) $
                writeFile (combinePathAndFile basedir (dropExtension $ takeFileName fullName) ++ ".fqn") 
                          (holmesShowImpEnv module_ finalEnv)
             when (  DumpInformationForThisModule `elem` options 
                  && DumpInformationForAllModules `notElem` options
                  ) 
                  $ print (addToTypeEnvironment toplevelTypes localEnv)
                  
             return (Right (dictionaryEnv, finalEnv, toplevelTypes, warnings))