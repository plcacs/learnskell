if [ ! -e $1 ]; then
	echo "$1 does not exist"
	exit 1
fi

if [ $# -ne 2 ]; then
	echo "not enough argument"
	exit 1
fi

n=0
type1=1
type2=0
type3=0
type4=0

helium -w $1 > tmp_res
while read line;
do
	if [ "`grep "(*): " <<< $line`" ]; then
		n=$((n+1))
		continue
	fi

	if [ $n -ne $2 ]; then
		continue
	fi

	if [[ "`egrep "([1-9][a-z]{2}|left|right) (argument|operand)[[:space:]]{1,}:" <<< $line`" ]]; then
		type3=1
	fi

	if [[ "`egrep "(term|constructor|operator)[[:space:]]{1,}:" <<< $line`" ]]; then
		type2=1
	fi

	if [[ "`egrep "(inferred|declared) type[[:space:]]{1,}:" <<< $line`" ]]; then
		type4=1
	fi
done < tmp_res

if [ $2 -gt $n ]; then
	echo "wrong msg index"
	exit 1
fi

if [ $type4 -eq 1 ]; then
	echo "4"
	exit
fi

if [ $type3 -eq 1 ]; then
	echo "3"
	exit
fi

if [ $type2 -eq 1 ]; then
	echo "2"
	exit
fi

echo $type1
